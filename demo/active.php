<?php
  $arr = file("jobs.txt");
  $i = 0;
  $active = max(2,intval(count($arr)/3));
  foreach ($arr as $x) {
    $y = explode(" ", $x);
    if ($y[0] != "JOB" || count($y) != 6) continue;
    $i++;
    if ($i == $active) {
      $numnodes = $y[1];
      $cputype  = $y[2];
      $memnode  = $y[3];
      $disknode = $y[4];
      $disktype = $y[5];
      break;
    }      
  }
  $datam = file_get_contents("datam.txt");
  $datar = file_get_contents("datar.txt");
  $datat = file_get_contents("datat.txt");
?>

<!doctype html> 
<html lang="en"> 
<head> 
<style>
.ui-progressbar{
  position: relative;
}
.progress-labelm {
  position: absolute;
  left: 50%;
  top: 5px;
}
.progress-labelr {
  position: absolute;
  left: 50%;
  top: 5px;
}
</style>
<script> 
var indexm = 0;
var indexr = 0;
var indext = 0;
var arrm = [<?php echo $datam; ?>];
var arrr = [<?php echo $datar; ?>];
var arrt = [<?php echo $datat; ?>];
$(function() {
  var pbar = $( "#progressbar-m" ), pbel = $( ".progress-labelm" );
  pbar.progressbar({
    value: false,
    change: function() { pbel.text( pbar.progressbar( "value" ) + "%" );  },
    complete: function() { pbel.text( "Complete!" ); }
  });
  function progress() {
    var val = pbar.progressbar( "value" );
    indexm++;
    pbar.progressbar( "value", arrm[indexm] );
    if ( val < 99 ) { setTimeout( progress, 500 ); }
  }
  setTimeout( progress, 500 );
});
$(function() {
  var pbar = $( "#progressbar-r" ), pbel = $( ".progress-labelr" );
  pbar.progressbar({
    value: false,
    change: function() { pbel.text( pbar.progressbar( "value" ) + "%" );  },
    complete: function() { pbel.text( "Complete!" ); }
  });
  function progress() {
    var val = pbar.progressbar( "value" );
    indexr++;
    pbar.progressbar( "value", arrr[indexr] );
    if ( val < 99 ) { setTimeout( progress, 500 ); }
  }
  setTimeout( progress, 500 );
});
function startTime() {
  document.getElementById('mrclock').innerHTML = arrt[indext];
  indext++;
  if (indext < arrt.length) {
     setTimeout(function(){startTime()}, 500);
  }
}
$( document ).ready(function() {
  startTime();
});
</script>
</head> 
<body> 
<table bgcolor="lightgrey"><tr>
<td><img src="active_icon.jpg"></td>
<td><h3>Monitor Active Job</h3></td>
</tr></table>
<table>
<tr><td>Job ID:</td><td><b><?php echo $active; ?></b></td></tr>
<tr><td>DataNodes:</td><td><?php echo $numnodes; ?></td></tr>
<tr><td>CPU:</td><td>Dual E5-<?php echo $cputype; ?></td></tr>
<tr><td>Memory:</td><td><?php echo $memnode; ?>GB</td></tr>
<tr><td>Disks:</td><td><?php echo $disknode; ?>x <?php echo $disktype; ?>RPM</td></tr>
<tr><td>Time Elapsed:</td><td><b><div id="mrclock">00:00</div></b></td></tr>
</table>
<br>
Map:
<div id="progressbar-m"><div class="progress-labelm">Waiting...</div></div>
Reduce:
<div id="progressbar-r"><div class="progress-labelr">Waiting...</div></div>
</body> 
</html>
