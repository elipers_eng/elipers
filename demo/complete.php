<?php
  $arr = file("jobs.txt");
  $i = 0;
  $active = max(2,intval(count($arr)/3));
  foreach ($arr as $x) {
    $y = explode(" ", $x);
    if ($y[0] != "JOB" || count($y) != 6) continue;
    $i++;
    if ($i == $active) break;
    $arr2[$i] = "<option value=\"" . $i . "\">" . "Job ID: " . $i ." DONE (DataNodes=" . $y[1] .", CPU=E5-" . $y[2] ."...)" . "</option>";
  }
  $arrval = file("val_2c_4g_4d.txt");
  $y = explode(" ", $arrval[0]);
  $arrval0[0] = $y[0];
  $arrval0[1] = $y[1];
  $arrval0[2] = $y[2];
  $arrval0[3] = "Run Time: " . $y[3] . " minutes";
  $arrval1[0] = "valstrc = '" . $y[0] . "'";
  $arrval1[1] = "valstrm = '" . $y[1] . "'";
  $arrval1[2] = "valstrd = '" . $y[2] . "'";
  $arrval1[3] = "valstrt = 'Run Time: " . $y[3] . " minutes'";
  $arrval = file("val_2c_8g_4d.txt");
  $y = explode(" ", $arrval[0]);
  $arrval2[0] = "valstrc = '" . $y[0] . "'";
  $arrval2[1] = "valstrm = '" . $y[1] . "'";
  $arrval2[2] = "valstrd = '" . $y[2] . "'";
  $arrval2[3] = "valstrt = 'Run Time: " . $y[3] . " minutes'";
  $arrval = file("val_4c_4g_4d.txt");
  $y = explode(" ", $arrval[0]);
  $arrval3[0] = "valstrc = '" . $y[0] . "'";
  $arrval3[1] = "valstrm = '" . $y[1] . "'";
  $arrval3[2] = "valstrd = '" . $y[2] . "'";
  $arrval3[3] = "valstrt = 'Run Time: " . $y[3] . " minutes'";
  $arrval = file("val_4c_8g_4d.txt");
  $y = explode(" ", $arrval[0]);
  $arrval4[0] = "valstrc = '" . $y[0] . "'";
  $arrval4[1] = "valstrm = '" . $y[1] . "'";
  $arrval4[2] = "valstrd = '" . $y[2] . "'";
  $arrval4[3] = "valstrt = 'Run Time: " . $y[3] . " minutes'";

  echo <<<_END
<!doctype html> 
<html lang="en"> 
<head> 
<script> 
$(function() { 
    $( "#accordion3" ).accordion({
      heightStyle: "content"
      }); 
}); 
$(function() {
$( "#jobs3" )
       .selectmenu({
          change: function( event, data ) {
	     if (data.item.value == 1) {
    	      imgstrc = '<p><img src="cpu_2c_4g_4d.jpg"></p>'
    	      imgstrm = '<p><img src="mem_2c_4g_4d.jpg"></p>'
    	      imgstrd = '<p><img src="disk_2c_4g_4d.jpg"></p>'
    	      imgstrcd = '<img src="cdial_2c_4g_4d.jpg" width="75%">'
    	      imgstrmd = '<img src="mdial_2c_4g_4d.jpg" width="75%">'
    	      imgstrdd = '<img src="ddial_2c_4g_4d.jpg" width="75%">'
_END;

echo PHP_EOL;
echo $arrval1[0], PHP_EOL;
echo $arrval1[1], PHP_EOL;
echo $arrval1[2], PHP_EOL;
echo $arrval1[3], PHP_EOL;

  echo <<<_END
		} else if (data.item.value == 2) {
    	      imgstrc = '<p><img src="cpu_2c_8g_4d.jpg"></p>'
    	      imgstrm = '<p><img src="mem_2c_8g_4d.jpg"></p>'
    	      imgstrd = '<p><img src="disk_2c_8g_4d.jpg"></p>' 
    	      imgstrcd = '<img src="cdial_2c_8g_4d.jpg" width="75%">'
    	      imgstrmd = '<img src="mdial_2c_8g_4d.jpg" width="75%">'
    	      imgstrdd = '<img src="ddial_2c_8g_4d.jpg" width="75%">'
_END;

echo PHP_EOL;
echo $arrval2[0], PHP_EOL;
echo $arrval2[1], PHP_EOL;
echo $arrval2[2], PHP_EOL;
echo $arrval2[3], PHP_EOL;

  echo <<<_END
		}else if (data.item.value == 3) {
    	      imgstrc = '<p><img src="cpu_4c_4g_4d.jpg"></p>'
    	      imgstrm = '<p><img src="mem_4c_4g_4d.jpg"></p>'
    	      imgstrd = '<p><img src="disk_4c_4g_4d.jpg"></p>'
    	      imgstrcd = '<img src="cdial_4c_4g_4d.jpg" width="75%">'
    	      imgstrmd = '<img src="mdial_4c_4g_4d.jpg" width="75%">'
    	      imgstrdd = '<img src="ddial_4c_4g_4d.jpg" width="75%">'
_END;

echo PHP_EOL;
echo $arrval3[0], PHP_EOL;
echo $arrval3[1], PHP_EOL;
echo $arrval3[2], PHP_EOL;
echo $arrval3[3], PHP_EOL;

  echo <<<_END
		}else if (data.item.value == 4) {
    	      imgstrc = '<p><img src="cpu_4c_8g_4d.jpg"></p>'
    	      imgstrm = '<p><img src="mem_4c_8g_4d.jpg"></p>'
    	      imgstrd = '<p><img src="disk_4c_8g_4d.jpg"></p>'
    	      imgstrcd = '<img src="cdial_4c_8g_4d.jpg" width="75%">'
    	      imgstrmd = '<img src="mdial_4c_8g_4d.jpg" width="75%">'
    	      imgstrdd = '<img src="ddial_4c_8g_4d.jpg" width="75%">'
_END;

echo PHP_EOL;
echo $arrval4[0], PHP_EOL;
echo $arrval4[1], PHP_EOL;
echo $arrval4[2], PHP_EOL;
echo $arrval4[3], PHP_EOL;

  echo <<<_END
		}else {
    	      imgstrc = '<p><img src="cpu_2c_4g_4d.jpg"></p>'
    	      imgstrm = '<p><img src="mem_2c_4g_4d.jpg"></p>'
    	      imgstrd = '<p><img src="disk_2c_4g_4d.jpg"></p>'
    	      imgstrcd = '<img src="cdial_2c_4g_4d.jpg" width="75%">'
    	      imgstrmd = '<img src="mdial_2c_4g_4d.jpg" width="75%">'
    	      imgstrdd = '<img src="ddial_2c_4g_4d.jpg" width="75%">'
_END;

echo PHP_EOL;
echo $arrval1[0], PHP_EOL;
echo $arrval1[1], PHP_EOL;
echo $arrval1[2], PHP_EOL;
echo $arrval1[3], PHP_EOL;

  echo <<<_END
		}
 	    document.getElementById('cpu').innerHTML = imgstrc;
 	    document.getElementById('memory').innerHTML = imgstrm;
 	    document.getElementById('disk').innerHTML = imgstrd;
 	    document.getElementById('cdial').innerHTML = imgstrcd;
 	    document.getElementById('mdial').innerHTML = imgstrmd;
 	    document.getElementById('ddial').innerHTML = imgstrdd;
 	    document.getElementById('cval').innerHTML = valstrc;
 	    document.getElementById('mval').innerHTML = valstrm;
 	    document.getElementById('dval').innerHTML = valstrd;
 	    document.getElementById('tval').innerHTML = valstrt;
          }
        })
      .selectmenu( "menuWidget" )
      .addClass( "overflow" );
});
</script> 
<style>
fieldset { border: 0; }
label { display: block; margin: 30px 0 0 0; }
.overflow { height: 100px; }
</style>
</head> 
<body> 
<table bgcolor="lightgrey"><tr>
<td><img src="complete_icon.jpg"></td>
<td><h3>Inspect Completed Jobs</h3></td>
</tr></table>
<div class="demo">
<form action="#">
<fieldset>
<label for="jobs3">Select a completed job</label>
<select name="jobs3" id="jobs3">
_END;

foreach ($arr2 as $x) {
  echo $x;
}

echo <<<_END
</select>
</fieldset>
</form>
</div>
<div id="accordion3"> 
<h3>Summary</h3>
<div id="summary"> 
<table>
<tr>
<td>CPU Utilization:</td>
<td>Memory Utilization:</td>
<td>Disk Utilization:</td>
</tr>
<tr>
<td><div id="cval"> 
_END;

echo $arrval0[0];

  echo <<<_END
</div></td>
<td><div id="mval"> 
_END;

echo $arrval0[1];

  echo <<<_END
</div></td>
<td><div id="dval"> 
_END;

echo $arrval0[2];

  echo <<<_END
</div></td>
</tr>
<tr>
<td><div id="cdial"> 
<img src="cdial_2c_4g_4d.jpg" width="75%">
</div></td>
<td><div id="mdial"> 
<img src="mdial_2c_4g_4d.jpg" width="75%">
</div></td>
<td><div id="ddial"> 
<img src="ddial_2c_4g_4d.jpg" width="75%">
</div></td>
</tr>
<td><div id="tval"> 
_END;

echo $arrval0[3];

  echo <<<_END
</div></td>
<tr>
</tr>
</table>
</div> 

<h3>CPU Utilization</h3>
<div id="cpu"> 
<p><img src="cpu_2c_4g_4d.jpg"></p>
</div> 

<h3>Memory Utilization</h3>
<div id="memory"> 
<p><img src="mem_2c_4g_4d.jpg"></p>
</div> 

<h3>Disk Utilization</h3>
<div id="disk"> 
<p><img src="disk_2c_4g_4d.jpg"></p>
</div> 

</div> 
</body> 
</html>
_END;

?>
