<?php
  $arr = file("jobs.txt");
  $i = 0;
  $active = max(2,intval(count($arr)/3));
  foreach ($arr as $x) {
    $y = explode(" ", $x);
    if ($y[0] != "JOB" || count($y) != 6) continue;
    $i++;
    if ($i == $active) break;
    $arr2[$i] = "<li>" . "Job ID: " . $i ." DONE (DataNodes=" . $y[1] .", CPU=E5-" . $y[2] ."...)" . "</li>";
  }

  echo <<<_END
<!doctype html>
<html lang="en">
<head>
<script>
$(function() {
array = [];
$( "#accordion4" ).accordion();
$( "#accordion4 li" ).draggable({
  appendTo: "body",
  helper: "clone"
});
$( "#compare ul" ).droppable({
  activeClass: "ui-state-default",
  hoverClass: "ui-state-hover",
  accept: ":not(.ui-sortable-helper)",
  drop: function( event, ui ) {
    $( this ).find( ".placeholder" ).remove();
    $( "<li></li>" ).text( ui.draggable.text() ).appendTo( this );
    array.push(ui.draggable.text());
  }
}).sortable({
  items: "li:not(.placeholder)",
  sort: function() {
    // gets added unintentionally by droppable interacting with sortable
    // using connectWithSortable fixes this, but doesn't allow you to customize active/hoverClass options
    $( this ).removeClass( "ui-state-default" );
  }
});
$( "input[type=submit]" )
  .button()
  .click( function( event ) {
    event.preventDefault();
    cpu = 0;
    memory = 0;
    disk = 0;
    jobs = 0;
    for (i=0; i<array.length; i++) {
      if (array[i].search("CPU Utilization") != -1)
        cpu = 1;
      if (array[i].search("Memory Utilization") != -1)
        memory = 1;
      if (array[i].search("Disk Utilization") != -1)
        disk = 1;
      if (array[i].search("Job ID") != -1)
        jobs++;
    }
    if (jobs == 0)
      imgstr = '<p>No Job selected.</p>'
    else if (jobs > 2)
      imgstr = '<p>Too many Jobs selected.</p>'
    else if ( jobs + cpu + memory + disk != 3 )
      imgstr = '<p>Incorrect number of Items selected</p>'
    else {
      if (cpu == 1 && memory == 1 && jobs == 1)
        imgstr = '<p><img src="cpu_vs_mem_4c_8g.jpg"></p>'
      else if (memory == 1 && disk == 1 && jobs == 1)
        imgstr = '<p><img src="mem_vs_disk_4c_8g.jpg"></p>'
      else if (disk == 1 && cpu == 1 && jobs == 1)
        imgstr = '<p><img src="cpu_vs_disk_4c_8g.jpg"></p>'
      else if (cpu == 1 && jobs == 2)
        imgstr = '<p><img src="cpu_2c_4g_vs_4c_8g.jpg"></p>'
      else if (memory == 1 && jobs == 2)
        imgstr = '<p><img src="mem_2c_4g_vs_4c_8g.jpg"></p>'
      else if (disk == 1 && jobs == 2)
        imgstr = '<p><img src="disk_2c_4g_vs_4c_8g.jpg"></p>'
      else
        imgstr = '<p>Unsupported configration selected</p>'
    }
    document.getElementById('graph').innerHTML = imgstr;
  });
});
</script>
</head>
<body>
<table bgcolor="lightgrey"><tr>
<td><img src="analyze_icon.jpg"></td>
<td><h3>Analyze Completed Jobs</h3></td>
</tr></table>
<table>
<div id="items">
<h3>Selectable Items</h3>
<div id="accordion4">
<h3><a href="#">Completed Jobs</a></h3>
<div>
<ul>
_END;

foreach ($arr2 as $x) {
  echo $x;
}

echo <<<_END
</ul>
</div>
<h3><a href="#">Metrics</a></h3>
<div>
<ul>
<li>CPU Utilization</li>
<li>Memory Utilization</li>
<li>Disk Utilization</li>
</ul>
</div>
</div>
</div>
<div id="compare">
<h3>Compare Items</h3>
<div class="ui-widget-content">
<ul>
<li class="placeholder">Drop Jobs or Metrics here to compare</li>
</ul>
</div>
<div id="submit">
<input type="submit" value="Compare">
</div>
<h3>Result</h3>
<div id="graph">
<p></p>
</div>
</div>
</body>
</html>
_END;

?>
