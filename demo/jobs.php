<?php
  $arr = file("jobs.txt");
  $i = 0;
  $active = max(2,intval(count($arr)/3));
  foreach ($arr as $x) {
    $y = explode(" ", $x);
    if ($y[0] != "JOB" || count($y) != 6) continue;
    $i++;
    $state = ($i == $active) ? "ACTIVE" : (($i < $active) ? "DONE" : "QUEUED");
    $uistate = ($i > $active) ? "ui-state-default" : "ui-state-default ui-state-disabled";
    if ($i == $active)
      $uicon = "ui-icon ui-icon-clock";
    else
      $uicon = ($i < $active) ? "ui-icon ui-icon-flag" : "ui-icon ui-icon-document";
    $b1 = ($i == $active) ? "<b>" : "";
    $b2 = ($i == $active) ? "</b>" : "";
    $arr2[$i] = "<li class=\"" . $uistate . "\"><span class=\"" . $uicon . "\"></span>" . $b1 ."Job ID: " . $i ." " . $state ." (DataNodes=" . $y[1] .", CPU=E5-" . $y[2] .", MEM=" . $y[3] ."GB...)" . $b2 ."</li>";
  }

  echo <<<_END
<!doctype html> 
<html lang="en"> 
<head> 
<style>
#sortable1 { list-style-type: none; margin: 0; padding: 0; zoom: 1 }
#sortable1 li { margin: 0 5px 5px 5px; padding: 3px; width: 50% }
#sortable1 li span { position: absolute; margin-left: -2em; }
</style>
<script> 
$(function() { 
  $( "#sortable1" ).sortable({
    items: "li:not(.ui-state-disabled)"
  });
  $( "#sortable1 li" ).disableSelection();
}); 
</script> 
</head> 
<body> 
<table bgcolor="lightgrey"><tr>
<td><img src="jobs_icon.jpg"></td>
<td><h3>Manage Submitted Job</h3></td>
</tr></table>
<ul id="sortable1">
_END;

foreach ($arr2 as $x) {
  echo $x;
}

echo <<<_END
</ul>
</body> 
</html>
_END;

?>
