import tornado.ioloop
import tornado.web
import tornado.websocket
import tornado.template

import numpy as np

in_file = open("ticker_days.tsv")
daysl = []
for line in in_file:
    daysl.append(line.rstrip().split('\t'))
for i in range(len(daysl)):
    print daysl[i]

in_file = open("ticker_sym.txt")
sym= []    #list of symbols
symi = {}  #dictionary of symbol/indices
x = 3
for line in in_file:
    y = line.rstrip()
    sym.append(y)
    symi[y] = [x, x+1, x+2]
    x += 3
print sym
print symi

syml = {}  #dictionary of symbol lists (temp)
symc = {}  #dictionary of symbol lists length (temp)
for s in sym:
    syml[s] = []
    symc[s] = 0

in_file = open("ticker.tsv")
for line in in_file:
    data = line.rstrip().split('\t')
    syml[data[3]].append(data)

for s in sym:
    sortl = sorted(syml[s], key=lambda x: 10000*int(x[0])+100*int(x[1])+int(x[2]))
    syml[s] = sortl
    symc[s] = len(sortl)

cscol = syml['CSCO']
for i in range(len(cscol)):
    print cscol[i]

orcll = syml['ORCL']
for i in range(len(orcll)):
    print orcll[i]

l = []
while (len(daysl) != 0 and sum(symc.values())!= 0):
    daysval = 10000*int(daysl[0][0]) + 100*int(daysl[0][1]) + int(daysl[0][2])
    row = daysl[0]
    for s in sym:
        if (daysval == 10000*int(syml[s][0][0]) + 100*int(syml[s][0][1]) + int(syml[s][0][2])):
            row += [s, syml[s][0][4], syml[s][0][5]]
            syml[s].pop(0)
        else:
            row += [s, '', '']
        symc[s] -= 1
    l.append(row)
    daysl.pop(0)
        
cscov = [int(x[symi['CSCO'][1]]) for x in l]
orclv = [int(x[symi['ORCL'][1]]) for x in l]
cscop = [float(x[symi['CSCO'][2]]) for x in l]
orclp = [float(x[symi['ORCL'][2]]) for x in l]

class MainHandler(tornado.web.RequestHandler):
  def get(self):
    loader = tornado.template.Loader(".")
    self.write(loader.load("index.html").generate())

class WSHandler(tornado.websocket.WebSocketHandler):
  def check_origin(self, origin):
    return True

  def open(self):
    print 'connection opened...'

  def on_message(self, message):
    print 'received:', message
    m = message.split(' ');
    if (m[0] == 'PRICE') :
      sym = m[1]
      sym_price = [float(x[symi[sym][2]]) for x in l] 
      string = ' '.join(str(e) for e in sym_price)
    elif (m[0] == 'NORM'):
      sym = m[1]
      sym_price = [float(x[symi[sym][2]]) for x in l] 
      string = ' '.join(str(e) for e in sym_price)
      sym_mean = np.mean(sym_price)
      sym_std  = np.std(sym_price)
      sym_norm = map(lambda x: (x - sym_mean)/sym_std, sym_price)
      string = ' '.join(str(e) for e in sym_norm)
    elif (m[0] == 'STAT'):
      sym = m[1]
      sym_price = [float(x[symi[sym][2]]) for x in l] 
      sym_min  = np.min(sym_price)
      sym_max  = np.max(sym_price)
      sym_mean = np.mean(sym_price)
      sym_std  = np.std(sym_price)
      string = str(sym_max) + ' ' + str(sym_min) + ' ' + str(sym_mean) + ' ' + str(sym_std)
    elif (m[0] == 'CORR'):
      sym1 = m[1]
      sym2 = m[2]
      sym1_price = [float(x[symi[sym1][2]]) for x in l] 
      sym2_price = [float(x[symi[sym2][2]]) for x in l] 
      sym1_sym2_corr = np.corrcoef(sym1_price, sym2_price)[0][1]
      string = str(sym1_sym2_corr)
    else:
      string = ""
    self.write_message(string)

  def on_close(self):
    print 'connection closed...'

application = tornado.web.Application([
  (r'/', WSHandler),
])

if __name__ == "__main__":
  print 'Ready...'
  application.listen(8080)
  tornado.ioloop.IOLoop.instance().start()
 
