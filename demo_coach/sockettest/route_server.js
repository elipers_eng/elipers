#!/usr/bin/env node

var fileconnection = null;
var barconnection = null;
var gqueue = [];

var fps = 50;
var tick = Math.floor(1000/fps);

var WebSocketServer = require('websocket').server;
var http = require('http');

var server = http.createServer(function(request, response) {
    console.log((new Date()) + ' Received request for ' + request.url);
    response.writeHead(404);
    response.end();
});

server.listen(8080, function() {
    console.log((new Date()) + ' Server is listening on port 8080');
});

wsServer = new WebSocketServer({
    httpServer: server,
    // You should not use autoAcceptConnections for production
    // applications, as it defeats all standard cross-origin protection
    // facilities built into the protocol and the browser.  You should
    // *always* verify the connection's origin and decide whether or not
    // to accept it.
    autoAcceptConnections: false
});

function originIsAllowed(origin) {
    // put logic here to detect whether the specified origin is allowed.
    return true;
}

function delayforward() {
    if (gqueue.length > 0) {
        var data = gqueue[0];
	gqueue.shift();
        barconnection.sendUTF(data);
	if (gqueue.length > 0) {
	    setTimeout( delayforward, tick );
	} else {
	    console.log((new Date()) + ' Forward last Message: ' + data);
	}
    }
}

wsServer.on('request', function(request) {
    if (!originIsAllowed(request.origin)) {
        // Make sure we only accept requests from an allowed origin
        request.reject();
        console.log((new Date()) + ' Connection from origin ' + request.origin + ' rejected.');
        return;
    }

    var connection = request.accept('echo-protocol', request.origin);
    if (fileconnection == null) {
        fileconnection = connection;
	console.log((new Date()) + ' File Connection accepted.');
    } else {
	barconnection = connection;
	console.log((new Date()) + ' Bar Connection accepted.');
    }
    connection.on('message', function(message) {
	gqueue.push(message.utf8Data);
	if (gqueue.length == 1) {
	    console.log((new Date()) + ' Received first Message: ' + message.utf8Data);
	    setTimeout( delayforward, tick );
	}
    });
    connection.on('close', function(reasonCode, description) {
	    if (connection == barconnection) {
                barconnection = null;
		console.log((new Date()) + ' Bar Peer ' + connection.remoteAddress + ' disconnected.');
	    } else {
                fileconnection = null;
		console.log((new Date()) + ' File Peer ' + connection.remoteAddress + ' disconnected.');
	    }
    });
});
