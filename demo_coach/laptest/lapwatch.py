from pyspark.sql.session import SparkSession

sparkSession = SparkSession.builder.master("local").appName("simple").enableHiveSupport().getOrCreate()

import time  
import sys
from datetime import datetime

t = "Time"
d = "Dist"
vg = "V_GPS"
vf = "Fr_Speed"
vr = "Rr_Speed"


from watchdog.observers import Observer  
from watchdog.events import PatternMatchingEventHandler  

class MyHandler(PatternMatchingEventHandler):
    patterns = ["*"]
    def process(self, event):
        print event.src_path, event.event_type
    def on_modified(self, event):
        self.process(event)
    def on_created(self, event):
        self.process(event)
        df = sparkSession.read.option("header","true").option("inferSchema","true").csv(event.src_path)
        df.select(t, d, vg, vf, vr).describe().show()

observer = Observer()
x = observer.schedule(MyHandler(), path='/tmp/laps')
observer.start()

try:
    while True:
        print datetime.now().strftime("%Y-%m-%d %H:%M:%S")
#        sys.stdout.write('.')
        time.sleep(1)
except KeyboardInterrupt:
    observer.stop()

observer.join()
