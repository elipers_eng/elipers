from pyspark import SparkContext
from pyspark.sql.session import SparkSession

from datetime import timedelta
from datetime import datetime

sc = SparkContext()
spark = SparkSession.builder.master("local").appName("simple").enableHiveSupport().getOrCreate()

from pyspark.streaming import StreamingContext
ssc = StreamingContext(sc, 1)

from pyspark.sql import Row
rd1 = ssc.textFileStream("/tmp/laps")
rd2 = rd1.map(lambda l: l.split(","))

def rdd_to_rowrdd(p) :
  return Row(\
    TStime=datetime.strptime(p[0], '%Y-%m-%d %H:%M:%S'),\
    Time=float(p[1]),\
    Dist=int(p[2]),\
    HHMM=p[3],\
    MMDD=p[4],\
    SSHH=p[5],\
    V_GPS=float(p[6]),\
    Fr_Speed=float(p[7]),\
    Rr_Speed=float(p[8]),\
    APS_angle=float(p[9]),\
    TPS_angle=float(p[10]),\
    RPM=float(p[11]),\
    Susp_F=float(p[12]),\
    Susp_R=float(p[13]),\
    Banking_GPS=float(p[14]),\
    Course=float(p[15]),\
    Latitude=float(p[16]),\
    Longitude=float(p[17]),\
    ValidSat=float(p[18]),\
    T_Water=float(p[19]),\
    T_Air=float(p[20]),\
    Gear=float(p[21]),\
    P_Oil=float(p[22]),\
    Sample_number=int(p[23]),\
    Date=p[24],\
    Hour=p[25],\
    Temperature=float(p[26]),\
    Status=p[27],\
    x1=p[28],\
    x2=p[29])

def do_rdd(rdd):
    print datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    if rdd.count() > 0:
      rd3 = rdd.map(rdd_to_rowrdd)
      df = spark.createDataFrame(rd3)
      df.select("Time", "Dist", "V_GPS", "Fr_speed", "Rr_speed").describe().show()

rd2.foreachRDD(do_rdd)

ssc.start()
ssc.awaitTermination()
