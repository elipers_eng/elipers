import sys

if len(sys.argv) != 2:
   print("Usage: modelgenbreath <filename>")
   exit(-1)

from pyspark import SparkContext

sc = SparkContext("local", "modelgenbreath")

data = sc.textFile("/tmp/modelbreath.txt")
from pyspark.mllib.regression import LabeledPoint
from pyspark.mllib.tree import DecisionTree
def parsePoint(line): values = [float(x) for x in line.split(' ')]; return LabeledPoint(values[0], values[1:])

parsedData = data.map(parsePoint)
model = DecisionTree.trainClassifier(parsedData, numClasses=63, categoricalFeaturesInfo={}, impurity='gini')

with open(sys.argv[1]) as f:
  for line in f:
    l = line.split() 
    x = model.predict(l)
    print("Prediction breathing rate is %i" % x)
