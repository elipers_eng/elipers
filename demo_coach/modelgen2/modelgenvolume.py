import sys

if len(sys.argv) != 2:
   print("Usage: modelgenvolume <filename>")
   exit(-1)

from pyspark import SparkContext

sc = SparkContext("local", "modelgenvolume")

data = sc.textFile("/tmp/modelvolume.txt")
from pyspark.mllib.regression import LabeledPoint
from pyspark.mllib.tree import DecisionTree
def parsePoint(line): values = [float(x) for x in line.split(' ')]; return LabeledPoint(values[0], values[1:])

parsedData = data.map(parsePoint)
model = DecisionTree.trainClassifier(parsedData, numClasses=100, categoricalFeaturesInfo={}, impurity='gini')

with open(sys.argv[1]) as f:
  for line in f:
    l = line.split() 
    x = model.predict(l)
    print("Prediction volume rate is %i" % x)
