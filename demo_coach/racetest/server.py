import tornado.ioloop
import tornado.web
import tornado.websocket
import tornado.template

import numpy as np

in_file = open("race2.csv")
rows = []
for line in in_file:
    rows.append(line.rstrip().split(','))

num_rows = len(rows)
num_steps = 50

percent_start = 0
percent_end = 100

row_start = percent_start * num_rows / 100
row_end = percent_end * num_rows / 100
row_skip = (row_end - row_start) / num_steps

val_start = row_start
val_skip = row_skip
val_count = num_steps
val_end = val_start + val_skip * val_count

#for i in range(len(rows)):
#    print rows[i]

columns = zip(*rows)

#for i in range(len(columns)):
#    print columns[i]

in_file = open("header.txt")
fields = in_file.readline().rstrip().split(',') #list of fields

v_front = columns[fields.index('V_FRONT')]
v_rear = columns[fields.index('V_REAR')]

v_front2 = [float(x) for x in v_front]
v_rear2 = [float(x) for x in v_rear]

print v_front2
        
class MainHandler(tornado.web.RequestHandler):
  def get(self):
    loader = tornado.template.Loader(".")
    self.write(loader.load("index.html").generate())

class WSHandler(tornado.websocket.WebSocketHandler):
  def check_origin(self, origin):
    return True

  def open(self):
    print 'connection opened...'

  def on_message(self, message):
    print 'received:', message
    m = message.split(' ');
    if (m[0] == 'VALUE') :
      field = m[1]
      rstart = int(m[2]) * num_rows / 100
      rend = int(m[3]) * num_rows / 100
      rskip = (rend - rstart) / num_steps
      if (rskip == 0):
        rskip = 1
      rrange = rskip * num_steps
      field_val = [float(x) for x in columns[fields.index(field)]][rstart:][:rrange] 
      field_ret = field_val[::rskip]
      string = ' '.join(str(e) for e in field_ret)
    elif (m[0] == 'NORM'):
      field = m[1]
      rstart = int(m[2]) * num_rows / 100
      rend = int(m[3]) * num_rows / 100
      rskip = (rend - rstart) / num_steps
      if (rskip == 0):
        rskip = 1
      rrange = rskip * num_steps
      field_val = [float(x) for x in columns[fields.index(field)]][rstart:][:rrange] 
      field_mean = np.mean(field_val)
      field_std  = np.std(field_val)
      field_norm = map(lambda x: (x - field_mean)/field_std, field_val)
      field_ret = field_norm[::rskip]
      string = ' '.join(str(e) for e in field_ret)
      print string
    elif (m[0] == 'STAT'):
      field = m[1]
      rstart = int(m[2]) * num_rows / 100
      rend = int(m[3]) * num_rows / 100
      rskip = (rend - rstart) / num_steps
      if (rskip == 0):
        rskip = 1
      rrange = rskip * num_steps
      field_val = [float(x) for x in columns[fields.index(field)]][rstart:][:rrange] 
      field_min  = np.min(field_val)
      field_max  = np.max(field_val)
      field_mean = np.mean(field_val)
      field_std  = np.std(field_val)
      string = str(field_max) + ' ' + str(field_min) + ' ' + str(field_mean) + ' ' + str(field_std)
    elif (m[0] == 'CORR'):
      field1 = m[1]
      field2 = m[2]
      rstart = int(m[3]) * num_rows / 100
      rend = int(m[4]) * num_rows / 100
      rskip = (rend - rstart) / num_steps
      if (rskip == 0):
        rskip = 1
      rrange = rskip * num_steps
      field1_val = [float(x) for x in columns[fields.index(field1)]][rstart:][:rrange] 
      field2_val = [float(x) for x in columns[fields.index(field2)]][rstart:][:rrange] 
      field1_field2_corr = np.corrcoef(field1_val, field2_val)[0][1]
      string = str(field1_field2_corr)
      print 'start: ', rstart, ':', rskip, ':', num_steps
    else:
      string = ""
    self.write_message(string)

  def on_close(self):
    print 'connection closed...'

application = tornado.web.Application([
  (r'/', WSHandler),
])

if __name__ == "__main__":
  print 'Ready...'
  application.listen(8080)
  tornado.ioloop.IOLoop.instance().start()
 
