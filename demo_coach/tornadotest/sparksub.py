from pyspark.sql.session import SparkSession

import numpy as np

sparkSession = SparkSession.builder.master("local").appName("simple").enableHiveSupport().getOrCreate()

df = sparkSession.read.option("header","true").option("inferSchema","true").csv("foobar3")

#
# select intersted fields
#
ts = "TSTime"
t = "Time"
d = "Dist"
#alt = "Altitude"
bk = "Banking_GPS"
th = "TPS_angle"
vg = "V_GPS"
lat = "Latitude"
lon = "Longitude"
rp = "RPM"
sf = "Susp_F"
sr = "Susp_R"
vf = "Fr_Speed"
vr = "Rr_Speed"
#bf = "Brake_F"
#br = "Brake_R"
tm = "Temperature"

dfk = df.select(ts, t, d, lat, lon, vg, vf, vr, sf, sr, bk, th, rp, tm)
dfk.persist()

df1 = dfk.filter(df[t]%1 == 0)
it1 = df1.select(t).rdd.map(lambda r: r[0]).collect()
id1 = df1.select(d).rdd.map(lambda r: r[0]).collect()

from pyspark.sql.functions import lead
from pyspark.sql import Window


win = Window.orderBy(d).rowsBetween(1,1)
diff = lead(df[d]).over(win) - df[d]

df2 = dfk.filter(df[d]%50 == 0).withColumn('diff', diff).filter("diff != 0")
it2 = df2.select(t).rdd.map(lambda r: r[0]).collect()
id2 = df2.select(d).rdd.map(lambda r: r[0]).collect()
