import tornado.ioloop
import tornado.web
import tornado.websocket
import tornado.template

str2df  = { 'df1':df1, 'df2':df2 } 
str2id  = { 'df1':id1, 'df2':id2 } 
str2it  = { 'df1':it1, 'df2':it2 } 
str2col = { 'ts':ts, 't':t, 'd':d, 'lat':lat, 'lon':lon, 'vg':vg, 'vf':vf, 'vr':vr, 'sf':sf, 'sr':sr, 'bk':bk, 'th':th, 'rp':rp, 'tm':tm }

class MainHandler(tornado.web.RequestHandler):
  def get(self):
    loader = tornado.template.Loader(".")
    self.write(loader.load("index.html").generate())

class WSHandler(tornado.websocket.WebSocketHandler):
  def check_origin(self, origin):
    return True
  def open(self):
    print 'connection opened...'
  def on_message(self, message):
    print 'received:', message
    m = message.split(' ');
    l = len(m)
    if (m[0] == 'DATA') :
      frm = m[1]
      if frm in str2df :
        col = m[2]
        if col in str2col :
          s = str2df[frm].select(str2col[col]).rdd.map(lambda r: r[0]).collect()
          x = 0
          y = 1
          if (l > 3) :
            x = int(m[3])
          if (l > 4) :
            y = int(m[4])
          a1 = s[x:][:50*y]
          a2 = a1[::y]
          string = ' '.join(str(e) for e in a2)
        else :
          string = "Unrecognized Field"
      else :
        string = "Unrecognized Frame"
    elif (m[0] == 'COUNT') :
      frm = m[1]
      if frm in str2df :
        s = str2df[frm].count()
        string = str(s)
      else :
        string = "Unrecognized Frame"
    elif (m[0] == 'ROW') :
      frm = m[1]
      if frm in str2df :
        x = 0
        if (l > 2) :
          x = int(m[2])
        if (l > 3) :
          x += 49 * int(m[3])
        s = str2df[frm].filter(df[t] == str2it[frm][x]).rdd.take(1)
        string = str(s)
    elif (m[0] == 'POS') :
      frm = m[1]
      s1 = str2df[frm].select(str2col['lat']).rdd.map(lambda r: r[0]).collect()
      s2 = str2df[frm].select(str2col['lon']).rdd.map(lambda r: r[0]).collect()
      x = 0
      y = 1
      if (l > 2) :
        x = int(m[2])
      if (l > 3) :
        y = int(m[3])
      a1 = s1[x:][:50*y]
      a2 = a1[::y]
      a3 = s2[x:][:50*y]
      a4 = a3[::y]
      string = str(a2[0]) + ' ' + str(a4[0]) + ' ' + str(a2[9]) + ' ' + str(a4[9]) + ' ' + str(a2[19]) + ' ' + str(a4[19]) + ' ' + str(a2[29]) + ' ' + str(a4[29])  + ' ' + str(a2[39]) + ' ' + str(a4[39]) + ' ' + str(a2[49]) + ' ' + str(a4[49])

    elif (m[0] == 'POS2') :
      frm = m[1]
      if frm in str2df :
        x = 0
        y = 49
        if (l > 2) :
          x = int(m[2])
          y = x + 49
        if (l > 3) :
          y = x + 49 * int(m[3])
        s1 = str2df[frm].filter(df[t] == str2it[frm][x]).rdd.take(1)
        s2 = str2df[frm].filter(df[t] == str2it[frm][y]).rdd.take(1)
        string = str(s1[0][lat]) + ' ' + str(s1[0][lon]) + ' ' + str(s2[0][lat]) + ' ' + str(s2[0][lon])
      else :
        string = "Unrecognized Frame"
    elif (m[0] == 'STOP') :
      tornado.ioloop.IOLoop.instance().stop()
      string = "Server Stopped"
    else:
      string = "Unrecognized Command"
    self.write_message(string)
  def on_close(self):
    print 'connection closed...'

application = tornado.web.Application([ (r'/', WSHandler) ])
application.listen(8080)
