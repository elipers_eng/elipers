import tornado.ioloop
import tornado.web
import tornado.websocket
import tornado.template

import csv
import time
import math
import threading
import re

import numpy as np
from collections import Counter
from itertools import groupby
from itertools import accumulate
from scipy import signal
from scipy.interpolate import interp1d

fname = ["acc_10hz.csv","acc_rbr_10hz.csv"]

lwinh = 300
lwin  = 2 * lwinh + 1
lwinl = lwinh
lwinr = lwinh + 1

swinh = 50
swin  = 2 * swinh + 1
swinl = swinh
swinr = swinh + 1

def flist_lwin(ll):
    return(ll)
    
def flist_swin(ll):
    return(ll[lwinl-swinl:len(flist)-lwinr+swinr])
    

flist = []
header_z = []

to_get = []
to_put = []

def file_hdr(fn):
    with open(fn) as f:
        hd = [r for r in csv.reader(f)][0]
    return(hd)

def file_body(fn):
    with open(fn) as f:
        body = [r for r in csv.reader(f)]
    body.pop(0)    
    return(body)

def load_files(fnl):
    global flist
    global header_z
    print(fnl)
    hdl = [file_hdr(fn) for fn in fnl]
    print(hdl)
    if len(hdl) > 1:
        eq = [hdl[0] == x for x in hdl[1:]]
        if not all(eq):
            return False
    header_z = hdl[0]
    bdl = [file_body(fn) for fn in fnl]
    print([len(x) for x in bdl])
    flist = sum(bdl, [])
    print(len(flist))
    return True


def load_file(n):
    global flist
    global header_z
    with open(fname[n]) as f:
        flist = [r for r in csv.reader(f)]
        header_z = flist[0]
    flist.pop(0)

# PYTHON
#load_file(0)
print("Python Ready")
# PYTHON

# SPARK
from pyspark import SparkContext
from pyspark.sql.session import SparkSession
from pyspark.sql.types import *
from pyspark.mllib.clustering import KMeans, KMeansModel
sc = SparkContext()
spark = SparkSession.builder.master("local").appName("simple").enableHiveSupport().getOrCreate()
#df = spark.read.option("header","true").option("inferSchema","true").csv(fname[fnum])
print("Spark Ready")
# SPARK

num_clus = 2
clusters = []
cluster_sord = [0] * num_clus
cluster_map = []
cluster_error = []
cluster_errs = []
cluster_range = []
cluster_range_smooth = []
df = []

#take_clus = 0
lspan = []
tspanl = []
take_oidx = []
left_oidx = []

def check_column(msg):
    if msg.isdigit():
        j = int(msg)
        if j >= len(header_z):
            j = -1
    else :
        if msg in header_z:
            j = header_z.index(msg)
        else:
            j = -1
    return j        

def mid_avg_ratio(sig,i,w):
    s1 = sum(sig[i-20:i])
    s2 = sum(sig[i+1:i+20+1])
    av = (s1+s2)/(2*w)
    r = sig[i]/av
    return r

def strround2(frlst):
    return [str(round(i,4)) for i in frlst]

def sparkflt(frlst):
    return [np.float(i) for i in frlst]

def filter_range(frlst, rng):
    return sum([frlst[x[0]:x[0]+x[1]] for x in rng], [])

def check_join(l):
    for i in range(0, len(l)-1):
        if l[i][0] + l[i][1] != l[i+1][0]:
            return(False)
    return(True)

def which_bucket(x, lacc):
    lm = [x >= lacc[i] and x < lacc[i+1] for i in range(0, len(lacc)-1)]
    return lm.index(True)

def split_range(rng, oldspanl):
    lacc = [0] + list(accumulate([x[1] for x in oldspanl]))
    buc = which_bucket(rng[0], lacc)
    idx = rng[0] - lacc[buc]
    togo = rng[1]
    l = []
    while togo > 0:
        cut = min(oldspanl[buc][1]-idx, togo)
        l.append([buc, idx, cut])    
        togo -= cut
        idx += cut
        if idx == oldspanl[buc][1]:
            buc += 1
            idx = 0
    return(l)

def convert_range(bnewspan, oldspanl):
    bucket = bnewspan[0]
    offset = bnewspan[1]
    len = bnewspan[2]
    return([oldspanl[bucket][0]+offset,len])

def zidx_to_oidx(zidx, lspan):
    oidx = []
    for i in zidx: 
        bspan = split_range(i, lspan)
        ospan = [convert_range(r,lspan) for r in bspan]
        oidx += ospan
    return(oidx)

def clamp(x, xc):
    if abs(x) > xc:
        if x > 0:
            return xc
        else:
            return -xc
    return x

def clamp2(x, xc):
    if abs(x) > xc:
        if x > 0:
            return 2
        else:
            return -2
    if x == 0:
        return 0
    if x > 0:
        return 1
    if x < 0:
        return -1

def chunks(l,n):
    for i in range(0,len(l),n):
        yield l[i:i+n]

        

def ratechunk(p, y, z):
    if p > 0.4:
        if (y > z):
            return(8)
        else:
            return(9)
    else:
        return(0)

def expandchunk(ll, res, fac):
    if res == 0:
        return(sum([[x]*fac for x in ll], []))
    else:
        return(sum([[x]*fac for x in ll[:-1]], []) + [ll[-1]] * res)

def scrunge(ll, fac):
    ll_split = [ll[i:i+fac] for i in range(0, len(ll),fac)]
    return([max(x,key=x.count) for x in ll_split])

def threaded_function(arg):
#    while True:
        # nothing
    time.sleep(10)
            

class MainHandler(tornado.web.RequestHandler):
    def get(self):
        loader = tornado.template.Loader(".")
        self.write(loader.load("index.html").generate())

class WSHandler(tornado.websocket.WebSocketHandler):
    def check_origin(self, origin):
        return True

    def open(self):
        print('connection opened...')

    def on_message(self, message):
        global num_clus
        global clusters
        global cluster_sord
        global cluster_map
        global cluster_error
        global cluster_errs
        global cluster_range
        global cluster_range_smooth
        global df
        global to_put
        global to_get
#        global take_clus
        global take_oidx
        global left_oidx
        global lspan
        global tspanl
        print('received: ', message)
        msg = message.split(' ')
        rstr = 'NACK'
        if len(msg) > 0:
            cmd = msg[0]
            msg.pop(0)
            if cmd == 'FILE' :
                if len(msg) == 0:
                    fidx = list(range(0,len(fname)))
                    fjoin = [str(x[0])+':'+x[1] for x in zip(fidx, fname)]
                    rstr = ','.join(fjoin)
                elif len(msg) > 0 :
                    if all([i.isdigit() for i in msg]):
                        ff = [fname[int(i)] for i in msg]
                        retval = load_files(ff)
                        if retval:
                            rstr = 'ACK'
                        else :
                            rstr = 'NACK'
#                    if msg[0].isdigit() :
#                        i = int(msg[0])
#                        if i < len(fname) :
#                            load_file(int(i))
#                            rstr = 'ACK'
#                        else :
#                            rstr = 'NACK'
            elif cmd == 'PUT' :
                if to_put != []:
                    if left_oidx == []:
                        print(len(to_put[1]))
                        to_get.append(to_put)
                    else:
                        print(left_oidx)
                        header_g = to_put[0]
                        data_g = to_put[1]
                        data_fg = filter_range(data_g, left_oidx)
                        print(len(data_g))
                        print(len(data_fg))
                        to_get.append([header_g, data_fg])
                    to_put = []
                    print([x[0] for x in to_get])
                    rstr = 'ACK'
            elif cmd == 'PUTTEST' :
                if to_put != []:
                    if left_oidx == []:
#                        to_get.append(to_put)
                        print([len(x[1]) for x in to_get])
                    else:
                        sid = int(msg[0])
                        print(left_oidx)
                        header_g = to_put[0]
                        data_g = to_put[1]
                        r1 = left_oidx[sid]
                        r2 = data_g[r1[0]:r1[0]+r1[1]]
                        print(len(r2))
                    r3 = [clamp2(v, 0.02) for v in r2]
                    clamped = r3
#                    chunkclamp = list(chunks(clamped, 100))
                    chunkclamp = [clamped[i:i+100] for i in range(0, len(clamped),100)]
                    num_chunks = len(chunkclamp)
                    unclamphi = [x.count(1) for x in chunkclamp]
                    unclamplo = [x.count(-1) for x in chunkclamp]
                    unclamppct = [(x.count(-1)+x.count(0)+x.count(1))/len(x) for x in chunkclamp]
                    ratehole = [ratechunk(unclamppct[i], unclamphi[i], unclamplo[i]) for i in range(0, num_chunks)]
                    ratenohole = [0] * num_chunks
                    rlast = 8
                    for i in range(0,num_chunks):
                        if ratehole[i] == 0:
                            ratenohole[i] = rlast
                        else:
                            ratenohole[i] = ratehole[i]
                            rlast = ratehole[i]
                    chunk_residue = len(clamped)%100
                    rate_expand = expandchunk(ratenohole, chunk_residue, 100)
                    r4 = expandchunk(unclamphi, chunk_residue, 100)
                    r5 = expandchunk(unclamplo, chunk_residue, 100)
                    r6 = expandchunk(unclamppct, chunk_residue, 100)
                    r7 = expandchunk(ratehole, chunk_residue, 100)
                    r8 = expandchunk(ratenohole, chunk_residue, 100)
                    char_map = ''.join([str(i) for i in r8])
                    p = []
                    for i in [8, 9]:
                        rex = re.compile(str(i) + '+')
                        iter = rex.finditer(char_map)
                        for m in iter:
                            spn = m.span()
                            p.append([i, spn[0], spn[1]-spn[0]])
                    r9 = sorted(p, key=lambda x: x[1])
                    if msg[1] == 'a' :
                        rlst = strround2(r2)
                    elif msg[1] == 'b':
                        rlst = strround2(r3)
                    elif msg[1] == 'c':
                        rlst = [str(i) for i in r4]
                    elif msg[1] == 'd':
                        rlst = [str(i) for i in r5]
                    elif msg[1] == 'e':
                        rlst = [str(i) for i in r6]
                    elif msg[1] == 'f':
                        rlst = [str(i) for i in r7]
                    elif msg[1] == 'g':
                        rlst = [str(i) for i in r8]
                    elif msg[1] == 'h':
                        rlst = [':'.join([str(i) for i in c]) for c in r9]
                    rstr = ','.join(rlst)
            elif cmd == 'PUTTEST2' :
                if to_put != []:
                    if left_oidx == []:
                        header_g = to_put[0]
                        data_g = to_put[1]
                        print(len(data_g))
                        rstart = 0
                        rlen = len(data_g)
                    else:
                        sid = int(msg[0])
                        print(left_oidx)
                        header_g = to_put[0]
                        data_g = to_put[1]
                        rstart = left_oidx[sid][0]
                        rlen = left_oidx[sid][1]
                    rdata_g = data_g[rstart:rstart+rlen]
                    print(len(rdata_g))
                    clamped = [clamp2(v, 0.02) for v in rdata_g]
                    chunkclamp = [clamped[i:i+100] for i in range(0, len(clamped),100)]
                    num_chunks = len(chunkclamp)
                    unclamphi = [x.count(1) for x in chunkclamp]
                    unclamplo = [x.count(-1) for x in chunkclamp]
                    unclamppct = [(x.count(-1)+x.count(0)+x.count(1))/len(x) for x in chunkclamp]
                    ratehole = [ratechunk(unclamppct[i], unclamphi[i], unclamplo[i]) for i in range(0, num_chunks)]
                    ratenohole = [0] * num_chunks
                    rlast = 8
                    for i in range(0,num_chunks):
                        if ratehole[i] == 0:
                            ratenohole[i] = rlast
                        else:
                            ratenohole[i] = ratehole[i]
                            rlast = ratehole[i]
                    chunk_residue = len(clamped)%100
                    rate_expand = expandchunk(ratenohole, chunk_residue, 100)
                    char_map = ''.join([str(i) for i in rate_expand])
                    p = []
                    for i in [8, 9]:
                        rex = re.compile(str(i) + '+')
                        iter = rex.finditer(char_map)
                        for m in iter:
                            spn = m.span()
                            p.append([i, spn[0], spn[1]-spn[0]])
                    rate_range = sorted(p, key=lambda x: x[1])
                    rate_range_start = [[x[0] , x[1]+rstart, x[2]] for x in rate_range]
                    rlst = [':'.join([str(i) for i in c]) for c in rate_range_start]
                    rstr = ','.join(rlst)
            elif cmd == 'PUTLOOP' :
                if to_put != []:
                    print(lspan)
                    header_g = to_put[0]
                    data_g = to_put[1]
                    if left_oidx == []:
                        print(len(data_g))
                        rstart = 0
                        rlen = len(data_g)
                        wlist = [[0, len(data_g)]]
                    else:
                        print(left_oidx)
                        sid = int(msg[0])
                        rstart = left_oidx[sid][0]
                        rlen = left_oidx[sid][1]
                        wlist = left_oidx
                    print(wlist)
                    rate_range_col = []
                    for rng in wlist:
                        rstart = rng[0]
                        rlen = rng[1]
                        rdata_g = data_g[rstart:rstart+rlen]
                        print(len(rdata_g))
                        clamped = [clamp2(v, 0.02) for v in rdata_g]
                        chunkclamp = [clamped[i:i+100] for i in range(0, len(clamped),100)]
                        num_chunks = len(chunkclamp)
                        unclamphi = [x.count(1) for x in chunkclamp]
                        unclamplo = [x.count(-1) for x in chunkclamp]
                        unclamppct = [(x.count(-1)+x.count(0)+x.count(1))/len(x) for x in chunkclamp]
                        ratehole = [ratechunk(unclamppct[i], unclamphi[i], unclamplo[i]) for i in range(0, num_chunks)]
                        ratenohole = [0] * num_chunks
                        rlast = 8
                        for i in range(0,num_chunks):
                            if ratehole[i] == 0:
                                ratenohole[i] = rlast
                            else:
                                ratenohole[i] = ratehole[i]
                                rlast = ratehole[i]
                        chunk_residue = len(clamped)%100
                        rate_expand = expandchunk(ratenohole, chunk_residue, 100)
                        char_map = ''.join([str(i) for i in rate_expand])
                        p = []
                        for i in [8, 9]:
                            rex = re.compile(str(i) + '+')
                            iter = rex.finditer(char_map)
                            for m in iter:
                                spn = m.span()
                                p.append([i, spn[0], spn[1]-spn[0]])
                        rate_range = sorted(p, key=lambda x: x[1])
                        rate_range_start = [[x[0] , x[1]+rstart, x[2]] for x in rate_range]
                        rate_range_col.append(rate_range_start)
                    rr0 = sum(rate_range_col, [])
                    rr1 = [y[1:] for y in list(filter(lambda x: x[0] == 8, rr0))]
                    rr2 = [y[1:] for y in list(filter(lambda x: x[0] == 9, rr0))]
                    print(rr1)
                    print(rr2)
                    tspanl.append(rr1)
                    lspan = rr2
                    rlst = [':'.join([str(i) for i in c]) for c in rate_range_col[sid]]
                    rstr = ','.join(rlst)
            elif cmd == 'GETH' :
                if len(to_get) > 0:
                    header_g = [x[0] for x in to_get]
                    rstr = ','.join(header_g)
            elif cmd == 'GETR' :
                if len(to_get) > 0:
                    if len(msg) == 0:
                        col = -1
                    else:
                        if msg[0].isdigit() :
                            col = int(msg[0])
                        else:
                            col = len(to_get)
                    if col < len(to_get):
                        rlst = strround2(to_get[col][1])
                        rstr = ','.join(rlst)
            elif cmd == 'CLR' :
                to_get = []
                num_clus = 2
                clusters = []
                cluster_sord = [0] * num_clus
                cluster_map = []
                cluster_error = []
                cluster_errs = []
                cluster_range = []
                cluster_range_smooth = []
                df = []
                take_oidx = []
                left_oidx = []
                lspan = []
                tspanl = []
                rstr = 'ACK'
            elif cmd == 'CLRCLUS' :
                if take_oidx != [] and left_oidx != []:
                    to_get = []
                    num_clus = 2
                    clusters = []
                    cluster_sord = [0] * num_clus
                    cluster_map = []
                    cluster_error = []
                    cluster_errs = []
                    cluster_range = []
                    cluster_range_smooth = []
                    df = []
                    rstr = 'ACK'
            elif cmd == 'TAKE' :
                if cluster_range != []:
                    take_clus = []
                    if len(msg) > 0 :
                        if msg[0].isdigit():
                            clus = int(msg[0])
                            if clus < num_clus: 
                                take_clus = [clus]
                        else:
                            if cluster_errs != []:
                                if msg[0] == 'MINERR':
                                    p = [x[2] for x in cluster_errs]
                                    take_clus = [p.index(min(p))]
                                elif msg[0] == 'MINCNT':
                                    p = [x[1] for x in cluster_errs]
                                    take_clus = [p.index(min(p))]
                                elif msg[0] == 'MAXERR':
                                    p = [x[2] for x in cluster_errs]
                                    take_clus = [p.index(max(p))]
                                elif msg[0] == 'MAXCNT':
                                    p = [x[1] for x in cluster_errs]
                                    take_clus = [p.index(max(p))]
                                elif msg[0] == 'NOTMINERR':
                                    p = [x[2] for x in cluster_errs]
                                    take_clus = list(range(0, num_clus))
                                    take_clus.pop(p.index(min(p)))
                                elif msg[0] == 'NOTMAXERR':
                                    p = [x[2] for x in cluster_errs]
                                    take_clus = list(range(0, num_clus))
                                    take_clus.pop(p.index(max(p)))
                        print(take_clus)
                        if take_clus != []:
                            take_zidxx = [x for x in cluster_range if x[0] in take_clus]
                            print(take_zidxx)
                            take_zidx = [x[1:] for x in take_zidxx]
                            left_zidxx = [x for x in cluster_range if not x[0] in take_clus]
                            print(left_zidxx)
                            left_zidx = [x[1:] for x in left_zidxx]
                            take_oidx = zidx_to_oidx(take_zidx, lspan)
                            left_oidx = zidx_to_oidx(left_zidx, lspan)
                            print(take_oidx)
                            print(left_oidx)
                            tspanl.append(take_oidx)
                            lspan = left_oidx
                            rstr = 'ACK'
            elif cmd == 'GSPAN' :
                if lspan != []:
                    tlspanl = tspanl + [lspan]
                    for x in tlspanl:
                        print(x)
                    if check_join(sorted(sum(tlspanl, []))):
                        tlspansl = sum([[[i]+y for y in tlspanl[i]] for i in range(0, len(tlspanl))], [])
                        tlspansl_sort = sorted(tlspansl, key=lambda x: x[1])
                        if len(msg) > 0 :
                            subcmd = msg[0]
                            if subcmd == 'SHOW' :
                                lmap = sum([[r[0]] * r[2] for r in tlspansl_sort], [])
                                rlst = [str(x) for x in lmap]
                                rstr = ','.join(rlst)
                            elif subcmd == 'COUNT' :
                                tcl = [sum([x[1] for x in y]) for y in tlspanl]
                                rlst = [str(x) for x in tcl]
                                rstr = ','.join(rlst)
                            elif subcmd == 'RANGE' :
                                rlst = [':'.join([str(i) for i in r]) for r in tlspansl_sort]
                                rstr = ','.join(rlst)
            elif cmd == 'DATAFRAME' :
                if len(to_get) > 0:
                    header_g = [x[0] for x in to_get]
                    print(header_g)
                    data_g = [x[1] for x in to_get]
                    data_gt = list(map(list, zip(*data_g)))
                    rdd = sc.parallelize(data_gt)
                    schema = StructType([StructField(x, FloatType(), True) for x in header_g])
                    df = spark.createDataFrame(rdd, schema)
                    df.describe().show()
                    rstr = 'ACK'
            elif cmd == 'CLUSTER' :
                if len(to_get) > 0:
                    if len(msg) > 0 :
                        subcmd = msg[0]
                        if subcmd == 'TRY' and len(msg) > 1 :
                            nc = int(msg[1])
                            if nc > 1 :
                                num_clus = nc
                                data_g = [x[1] for x in to_get]
                                data_gt = list(map(list, zip(*data_g)))
                                parsedData = sc.parallelize(data_gt)
                                cx = []
                                cc = 0
                                for i in range(0, 50):
                                    lclusters = KMeans.train(parsedData, num_clus, maxIterations=10, initializationMode="random")
                                    lcluster_dist = [np.linalg.norm(x) for x in lclusters.centers]
                                    lcluster_sord = [i[0] for i in sorted(enumerate(lcluster_dist), key=lambda x:x[1])]
                                    lcluster_sdist = [lcluster_dist[lcluster_sord[i]] for i in range(0, num_clus)]
                                    print(lcluster_sdist)
                                    if cx == lcluster_sdist :
                                        cc += 1
                                    else :
                                        cx = lcluster_sdist
                                        cc = 1
                                    if cc == 3:
                                        break
                                print(cx, cc)
                                if cc == 3 :
                                    clusters = lclusters
                                    cluster_sord = lcluster_sord
                                    cnt = parsedData.count()
                                    pd = parsedData.take(cnt)
                                    result = [clusters.predict(x) for x in pd]
                                    cluster_smap = [cluster_sord.index(j) for j in range(0, num_clus)]
                                    cluster_map = [cluster_smap[j] for j in result]
                                    cluster_range = []
                                    cluster_range_smooth = []
                                    errora = [np.array(pd[i]) - clusters.centers[result[i]] for i in range(0,cnt)]
                                    cluster_error = [np.linalg.norm(x) for x in errora]
                                    if lspan == []:
                                        lspan = [[0, cnt]]
                                    rstr = 'ACK'
                                else :
                                    clusters = []
                                    cluster_sord = [0] * num_clus
                                    rstr = 'NACK'
                        if subcmd == 'FORCE' and len(msg) > 1 :
                            nc = int(msg[1])
                            if nc > 1 :
                                num_clus = nc
                                data_g = [x[1] for x in to_get]
                                data_gt = list(map(list, zip(*data_g)))
                                parsedData = sc.parallelize(data_gt)
                                lclusters = KMeans.train(parsedData, num_clus, maxIterations=10, initializationMode="random")
                                lcluster_dist = [np.linalg.norm(x) for x in lclusters.centers]
                                lcluster_sord = [i[0] for i in sorted(enumerate(lcluster_dist), key=lambda x:x[1])]
                                lcluster_sdist = [lcluster_dist[lcluster_sord[i]] for i in range(0, num_clus)]
                                print(lcluster_sdist)
                                clusters = lclusters
                                cluster_sord = lcluster_sord
                                cnt = parsedData.count()
                                pd = parsedData.take(cnt)
                                result = [clusters.predict(x) for x in pd]
                                cluster_smap = [cluster_sord.index(j) for j in range(0, num_clus)]
                                cluster_map = [cluster_smap[j] for j in result]
                                cluster_range = []
                                cluster_range_smooth = []
                                errora = [np.array(pd[i]) - clusters.centers[result[i]] for i in range(0,cnt)]
                                cluster_error = [np.linalg.norm(x) for x in errora]
                                if lspan == []:
                                    lspan = [[0, cnt]]
                                rstr = 'ACK'
                        elif subcmd == 'CENTER' :
                            if clusters != []:
                                print(clusters.centers)
                                cluster_scenters = [clusters.centers[cluster_sord[i]] for i in range(0, num_clus)]
                                print(cluster_scenters)
                                rlst = [':'.join([str(i) for i in c]) for c in cluster_scenters]
                                rstr = ','.join(rlst)
                        elif subcmd == 'DIST' :
                            if clusters != []:
                                cluster_dist = [np.linalg.norm(x) for x in clusters.centers]
                                print(cluster_dist)
                                cluster_sdist = [cluster_dist[cluster_sord[i]] for i in range(0, num_clus)]
                                print(cluster_sdist)
                                rlst = [str(x) for x in cluster_sdist]
                                rstr = ','.join(rlst)
            elif cmd == 'MAP' :
                if cluster_map != []:
                    if cluster_range == []:
                        char_map = ''.join([str(i) for i in cluster_map])
                        p = []
                        for i in range(0, num_clus):
                            rex = re.compile(str(i) + '+')
                            iter = rex.finditer(char_map)
                            for m in iter:
                                spn = m.span()
                                p.append([i, spn[0], spn[1]-spn[0]])
                        psort = sorted(p, key=lambda x: x[1])
                        cluster_range = psort
                    if len(msg) > 0 :
                        subcmd = msg[0]
                        if subcmd == 'SHOW' :
                            rlst = [str(x) for x in cluster_map]
                            rstr = ','.join(rlst)
                        elif subcmd == 'COUNT' :
                            cluster_count = [cluster_map.count(i) for i in range(0, num_clus)]
                            rlst = [str(x) for x in cluster_count]
                            rstr = ','.join(rlst)
                        elif subcmd == 'RANGE' :
                            rlst = [':'.join([str(i) for i in c]) for c in cluster_range]
                            rstr = ','.join(rlst)
                        elif subcmd == 'ERROR' :
                            rlst = strround2(cluster_error)
                            rstr = ','.join(rlst)
                        elif subcmd == 'ERRS' :
                            if cluster_errs == []:
                                for c in range(0, num_clus):
                                    n = cluster_map.count(c)
                                    s = sum([cluster_error[i] if cluster_map[i] == c else 0 for i in range(0, len(cluster_map))])
                                    cluster_errs.append([c, n, s/n])
                            rlst = [':'.join([str(i) for i in c]) for c in cluster_errs]
                            rstr = ','.join(rlst)
                        elif subcmd == 'ERRA' :
                            n = len(cluster_error)
                            s = sum(cluster_error)
                            rlst = ['A', str(n), str(s/n)]
                            rstr = ','.join(rlst)
            elif cmd == 'HEADER' :
                rstr = ','.join(header_z)
            elif cmd == 'ROW' :
                if len(msg) == 0:
                    rstr = str(len(flist))
                elif len(msg) > 0 :
                    if msg[0].isdigit():
                        i = int(msg[0])
                        if i < len(flist) :
                            rlst = flist[i]
                            rstr = ','.join(rlst)
            elif cmd == 'COLUMN' :
                if len(msg) == 0:
                    rstr = str(len(header_z))
                elif len(msg) > 0 :
                    j = check_column(msg[0])
                    if j != -1 :
                        rlst = [x[j] for x in flist]
                        rstr = ','.join(rlst)
            elif cmd == 'SIGNAL' :
                if len(msg) > 0 :
                    j = check_column(msg[0])
                    if j != -1 :
                        sig = [float(x[j]) for x in flist]
                        sigt = sig[lwinl:len(sig)-lwinr]
                        print(len(sigt))
                        rlst = strround2(sigt)
                        rstr = ','.join(rlst)
                        to_put = ['_'.join(msg+ [cmd]), sparkflt(sigt)]
            elif cmd == 'SMOOTH' :
                if len(msg) > 0 :
                    j = check_column(msg[0])
                    if j != -1 :
                        sig = [float(x[j]) for x in flist]
                        sigt = flist_swin(sig)
                        csiga= np.convolve(sigt, np.ones(swin),mode='valid')[0:-1]/swin
                        csigl = list(csiga)
                        print(len(csigl))
                        rlst = strround2(csigl)
                        rstr = ','.join(rlst)
                        to_put = ['_'.join(msg+ [cmd]), sparkflt(csigl)]
            elif cmd == 'SMOOTH2' :
                if len(msg) > 0 :
                    j = check_column(msg[0])
                    if j != -1 :
                        sig = [float(x[j]) for x in flist]
                        sigt = flist_lwin(sig)
                        csiga= np.convolve(sigt, np.ones(lwin),mode='valid')[0:-1]/lwin
                        csigl = list(csiga)
                        print(len(csigl))
                        rlst = strround2(csigl)
                        rstr = ','.join(rlst)
                        to_put = ['_'.join(msg+ [cmd]), sparkflt(csigl)]
            elif cmd == 'DIFF' :
                if len(msg) > 1 :
                    j = check_column(msg[0])
                    k = check_column(msg[1])
                    if j != -1 and k != 1 :
                        sigj = [float(x[j]) for x in flist]
                        sigjt = flist_swin(sigj)
                        csigja= np.convolve(sigjt, np.ones(swin),mode='valid')[0:-1]/swin
                        csigjl = list(csigja)
                        sigk = [float(x[k]) for x in flist]
                        sigkt = flist_swin(sigk)
                        csigka= np.convolve(sigkt, np.ones(swin),mode='valid')[0:-1]/swin
                        csigkl = list(csigka)
                        dsigl = [csigjl[q] - csigkl[q] for q in range(0, len(list(csigjl)))]
                        rlst = strround2(dsigl)
                        rstr = ','.join(rlst)
                        to_put = ['_'.join(msg+ [cmd]), sparkflt(dsigl)]
            elif cmd == 'STDDEV' :
                if len(msg) > 0 :
                    j = check_column(msg[0])
                    if j != -1 :
                        sig = [float(x[j]) for x in flist]
                        sigt = flist_swin(sig)
                        numseg = len(sig) - lwin
                        avg_ay_col = []
                        for i in range(0,numseg):
                            sig_split = np.array(sigt[i:i+swin])
                            a = np.std(sig_split)
                            avg_ay_col.append(a)
                        print(len(avg_ay_col))
                        rlst = strround2(avg_ay_col)
                        rstr = ','.join(rlst)
                        to_put = ['_'.join(msg+ [cmd]), sparkflt(avg_ay_col)]
            elif cmd == 'STDDEV2' :
                if len(msg) > 0 :
                    j = check_column(msg[0])
                    if j != -1 :
                        sig = [float(x[j]) for x in flist]
                        sigt = flist_lwin(sig)
                        numseg = len(sig) - lwin
                        avg_ay_col = []
                        for i in range(0,numseg):
                            sig_split = np.array(sigt[i:i+lwin])
                            a = np.std(sig_split)
                            avg_ay_col.append(a)
                        print(len(avg_ay_col))
                        rlst = strround2(avg_ay_col)
                        rstr = ','.join(rlst)
                        to_put = ['_'.join(msg+ [cmd]), sparkflt(avg_ay_col)]
            elif cmd == 'UBOUND' :
                if len(msg) > 0 :
                    j = check_column(msg[0])
                    if j != -1 :
                        sig = [float(x[j]) for x in flist]
                        sigt = flist_swin(sig)
                        numseg = len(sig) - lwin
                        avg_ay_col = []
                        for i in range(0,numseg):
                            sig_split = np.array(sigt[i:i+swin])
                            a = signal.find_peaks_cwt(sig_split, [3])
                            a_x = [i + p for p in list(a)]
                            a_y = [sigt[j] for j in a_x]
                            avg_ay = sum(a_y)/len(a)
                            avg_ay_col.append(avg_ay)
                        print(len(avg_ay_col))
                        rlst = strround2(avg_ay_col)
                        rstr = ','.join(rlst)
                        to_put = ['_'.join(msg+ [cmd]), sparkflt(avg_ay_col)]
            elif cmd == 'LBOUND' :
                if len(msg) > 0 :
                    j = check_column(msg[0])
                    if j != -1 :
                        sig = [float(x[j]) for x in flist]
                        sigt = flist_swin(sig)
                        numseg = len(sig) - lwin
                        avg_ay_col = []
                        for i in range(0,numseg):
                            sig_split = np.array(sigt[i:i+swin])
                            a = signal.find_peaks_cwt([-i for i in sig_split], [3])
                            a_x = [i + p for p in list(a)]
                            a_y = [sigt[j] for j in a_x]
                            avg_ay = sum(a_y)/len(a)
                            avg_ay_col.append(avg_ay)
                        print(len(avg_ay_col))
                        rlst = strround2(avg_ay_col)
                        rstr = ','.join(rlst)
                        to_put = ['_'.join(msg+ [cmd]), sparkflt(avg_ay_col)]
            elif cmd == 'DBOUND' :
                if len(msg) > 0 :
                    j = check_column(msg[0])
                    if j != -1 :
                        sig = [float(x[j]) for x in flist]
                        sigt = flist_swin(sig)
                        numseg = len(sig) - lwin
                        avg_ay_col_u = []
                        for i in range(0,numseg):
                            sig_split = np.array(sigt[i:i+swin])
                            a = signal.find_peaks_cwt(sig_split, [3])
                            a_x = [i + p for p in list(a)]
                            a_y = [sigt[j] for j in a_x]
                            avg_ay = sum(a_y)/len(a)
                            avg_ay_col_u.append(avg_ay)
                        avg_ay_col_l = []
                        for i in range(0,numseg):
                            sig_split = np.array(sigt[i:i+swin])
                            a = signal.find_peaks_cwt([-i for i in sig_split], [3])
                            a_x = [i + p for p in list(a)]
                            a_y = [sigt[j] for j in a_x]
                            avg_ay = sum(a_y)/len(a)
                            avg_ay_col_l.append(avg_ay)
                        avg_ay_col = [avg_ay_col_u[i]-avg_ay_col_l[i] for i in range(0, numseg)]
                        print(len(avg_ay_col))
                        rlst = strround2(avg_ay_col)
                        rstr = ','.join(rlst)
                        to_put = ['_'.join(msg+ [cmd]), sparkflt(avg_ay_col)]
            elif cmd == 'SLOPE' :
                if len(msg) > 0 :
                    j = check_column(msg[0])
                    if j != -1 :
                        sig = [float(x[j]) for x in flist]
                        sigt = flist_swin(sig)
                        csig = np.convolve(sigt, np.ones(swin),mode='valid')[0:-1]/swin
                        smth = signal.savgol_filter(csig, swin, 1)
                        grad = np.gradient(smth)
                        gradl = list(grad)
                        rlst = strround2(gradl)
                        rstr = ','.join(rlst)
                        to_put = ['_'.join(msg+ [cmd]), sparkflt(gradl)]
            elif cmd == 'DPULSE' :
                if len(msg) > 0 :
                    j = check_column(msg[0])
                    if j != -1 :
                        sig = [float(x[j]) for x in flist]
                        bx = signal.find_peaks_cwt([-i for i in sig], [3])
#                        print(bx) 
                        bxf = [x for x in bx if x > 20 and x + 20 < len(sig)]
#                        print(bxf) 
                        bb = [i for i in bxf if mid_avg_ratio(sig, i, 20) > 1.2]
                        print(bb) 
#                        print([mid_avg_ratio(sig, i, 20) for i in bxf])
                        ebb = [t for t in enumerate(bb)]
                        debb = [(bb[i], ebb[i][1] - ebb[i-1][1]) for i in range(1, len(ebb))]
                        fbb = [i[0] for i in debb if i[1] > 2*20+1]
                        xbb = [bb[0]] + fbb
                        print(xbb)
#                        bbb = [':'.join([str(j) for j in sig[i-wdw:i+wdw]]) for i in xbb]
                        bbb = [sig[i-2*20:i+2*20+1] for i in xbb]
                        rlst = [':'.join([str(j) for j in i]) for i in bbb]
                        rstr = ','.join(rlst)
            elif cmd == 'HELP' :
                rlst = ['FILE','PUT','GETH/R','CLR','HEADER','ROW','COLUMN','SMOOTH','UBOUND','LBOUND','SLOPE','DPULSE','CLUSTER-TRY-CENTER-DIST','MAP-SHOW-COUNT-RANGE']
                rstr = ','.join(rlst)
            self.write_message(rstr)


    def on_close(self):
        print('connection closed...')
        
application = tornado.web.Application([
    (r'/', WSHandler),
])

if __name__ == "__main__":
    thread = threading.Thread(target=threaded_function, args=(10,))
    thread.start()
    print('Ready...')
    application.listen(8080)
    tornado.ioloop.IOLoop.instance().start()
    
