#!/bin/bash
set -x
python3 client2.py 'TAKE MAXERR'
python3 client2.py 'GSPAN COUNT'
python3 client2.py 'CLRCLUS'
python3 client2.py 'STDDEV yaccb' > /dev/null
python3 client2.py 'PUT'
python3 client2.py 'STDDEV zaccb' > /dev/null
python3 client2.py 'PUT'
python3 client2.py 'CLUSTER TRY 2'
python3 client2.py 'CLUSTER DIST'
python3 client2.py 'CLUSTER CENTER'
python3 client2.py 'MAP RANGE'
python3 client2.py 'MAP ERRS'
python3 client2.py 'MAP ERRA'
python3 client2.py 'GETH' | awk '{printf "%s,%s,%s\n", $0,"STATE","ERROR"}' > hhh
python3 client2.py 'GETR 0' > xxx
python3 client2.py 'GETR 1' >> xxx
python3 client2.py 'MAP SHOW' >> xxx
python3 client2.py 'MAP ERROR' >> xxx
csvtool transpose xxx > yyy
cat hhh yyy > xxx
