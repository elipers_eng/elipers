#!/bin/bash
set -x
#python3 client2.py 'FILE 0'
python3 client2.py 'FILE 1'
python3 client2.py 'CLR'
python3 client2.py 'SMOOTH yacc' > /dev/null
python3 client2.py 'PUT'
python3 client2.py 'SMOOTH zacc' > /dev/null
python3 client2.py 'PUT'
python3 client2.py 'CLUSTER TRY 2'
python3 client2.py 'MAP RANGE'
python3 client2.py 'MAP ERRS'
#
python3 client2.py 'TAKE MAXERR'
python3 client2.py 'GSPAN COUNT'
python3 client2.py 'CLRCLUS'
python3 client2.py 'STDDEV yaccb' > /dev/null
python3 client2.py 'PUT'
python3 client2.py 'STDDEV zaccb' > /dev/null
python3 client2.py 'PUT'
python3 client2.py 'CLUSTER TRY 2'
python3 client2.py 'MAP RANGE'
python3 client2.py 'MAP ERRS'
#
python3 client2.py 'TAKE MINERR'
python3 client2.py 'GSPAN COUNT'
python3 client2.py 'CLRCLUS'
python3 client2.py 'STDDEV2 xaccb' > /dev/null
python3 client2.py 'PUT'
python3 client2.py 'STDDEV2 yaccb' > /dev/null
python3 client2.py 'PUT'
python3 client2.py 'CLUSTER TRY 2'
python3 client2.py 'MAP RANGE'
python3 client2.py 'MAP ERRS'
#
python3 client2.py 'TAKE MAXERR'
python3 client2.py 'GSPAN COUNT'
python3 client2.py 'CLRCLUS'
python3 client2.py 'DIFF yrotb zrotb' > /dev/null
python3 client2.py 'PUTLOOP 0' > yyy
#
python3 client2.py 'GSPAN SHOW' > zzz
