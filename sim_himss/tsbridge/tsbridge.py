#!/usr/bin/env python

import logging
import logging.handlers
import sys
import time
import random

sys_logger = logging.getLogger(__name__)
sys_logger.setLevel(logging.INFO)
sys_formatter = logging.Formatter("%(asctime)s - %(name)s - %(levelname)s - %(message)s")
sys_handler = logging.FileHandler("/var/log/tsbridge/tsbridge_sys.log")
sys_handler.setFormatter(sys_formatter)
sys_logger.addHandler(sys_handler)

class MyLogger(object):
    def __init__(self, logger, level):
        self.logger = logger
        self.level = level

    def write(self, message):
        if message.rstrip() != "":
           self.logger.log(self.level, message.rstrip())

sys.stdout = MyLogger(sys_logger, logging.INFO)
sys.stderr = MyLogger(sys_logger, logging.ERROR)

tsb_logger = logging.getLogger("TSBridgeLog")
tsb_logger.setLevel(logging.INFO)
tsb_formatter = logging.Formatter("%(asctime)s,%(message)s","%Y-%m-%d %H:%M:%S")
tsb_handler = logging.handlers.RotatingFileHandler("/var/log/tsbridge/tsbridge.log", maxBytes=10000, backupCount=8)
tsb_handler.setFormatter(tsb_formatter)
tsb_logger.addHandler(tsb_handler)


flist = ['/home/totem/foobar_lap3.csv', '/home/totem/foobar_lap4.csv', '/home/totem/foobar_lap7.csv', '/home/totem/foobar_lap8.csv', '/home/totem/foobar_lap11.csv', '/home/totem/foobar_lap14.csv']
num_files = len(flist)

filename = ''
lines = []
numlines = 0
lindex = 1
t_start = 0
d_start = 0

def new_lap():
    global filename
    global lines
    global numlines
    global lindex
    global t_start
    global d_start
    filename = flist[int(random.uniform(0,num_files))]
    with open(filename) as f:
        lines = f.read().splitlines()
    numlines = len(lines)
    lindex = 1
    l1 = lines[1].split(',')
    t_start = float(l1[1])
    d_start = int(l1[2])

new_lap()
    
while True:
    while True:
        l = lines[lindex].split(',')
        t = float(l[1]) - t_start
        res = int(t*10000)%10000
        if res == 0 or res == 9999 :
            break
        lindex += 1
        if lindex == numlines :
            new_lap()
    d = int(l[2]) - d_start
    vg  = l[6]
    lat = l[16]
    lon = l[17]
    tsb_logger.info("%s,%s,%s,%s,%s" % (str(t),str(d),vg,lat,lon))
    lindex += 1
    if lindex == numlines :
        new_lap()
    time.sleep(1)
