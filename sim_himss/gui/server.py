import tornado.ioloop
import tornado.web
import tornado.websocket
import tornado.template

import csv
import threading
from time import sleep

f = open("harvest.csv")
rows = csv.reader(f)
lines = [r for r in rows]
line_num = 0
line_max = len(lines)
stream_th = None
stream_ev = None

f = open("harvest_header.csv")
rows = csv.reader(f)
header = [r for r in rows][0]

def line_field(i, f):
    if f[0] == 'ALL':
        return lines[i]
    chk = [x in header for x in f]
    if all(x == True for x in chk) == False:
        return []
    idx = [header.index(x) for x in f]
    ln = lines[i]
    val = [ln[x] for x in idx]
    return val

class MainHandler(tornado.web.RequestHandler):
  def get(self):
    loader = tornado.template.Loader(".")
    self.write(loader.load("index.html").generate())

class WSHandler(tornado.websocket.WebSocketHandler):

  def tf(self, ev, msg):
    for i in xrange(line_max):
      if ev.isSet():
        return
      val = line_field(i, msg)
      stro = 'DATA ' + ' '.join(val)
      self.write_message(stro)
      sleep(1)

  def check_origin(self, origin):
    return True

  def open(self):
    print 'connection opened...'

  def on_message(self, message):
    global stream_th
    global stream_ev
    global line_num
    global line_max
    print 'received:', message

    msg = message.split(' ')
    if len(msg) > 0:
        cmd = msg[0]
        msg.pop(0)
        if cmd == 'READ' :
            if len(msg) > 0 :
                val = line_field(line_num, msg)
                string = 'DATA ' + ' '.join(val)
                if len(val) > 0:
                    line_num += 1 
                    if line_num == line_max :
                        line_num = 0
            else:
                string = 'NACK'
        elif cmd == 'START' :
            stream_ev = threading.Event()
            stream_th = threading.Thread(target=self.tf, args=(stream_ev,msg,))
            stream_th.start()
            string = 'ACK'
        elif cmd == 'STOP' :
            stream_ev.set()
            stream_th.join()
            string = 'ACK'
    else:
        string = 'NACK'

    self.write_message(string)
    print 'send:', string

  def on_close(self):
    print 'connection closed...'

application = tornado.web.Application([
  (r'/', WSHandler),
])

if __name__ == "__main__":
  print 'Ready...'
  application.listen(8080)
  tornado.ioloop.IOLoop.instance().start()
 
