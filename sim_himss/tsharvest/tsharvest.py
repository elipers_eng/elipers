#!/usr/bin/env python

import logging
import logging.handlers
import sys
import time
import random
import paramiko
import csv

sys_logger = logging.getLogger(__name__)
sys_logger.setLevel(logging.INFO)
sys_formatter = logging.Formatter("%(asctime)s - %(name)s - %(levelname)s - %(message)s")
sys_handler = logging.FileHandler("/var/log/tsharvest/tsharvest_sys.log")
sys_handler.setFormatter(sys_formatter)
sys_logger.addHandler(sys_handler)

class MyLogger(object):
    def __init__(self, logger, level):
        self.logger = logger
        self.level = level

    def write(self, message):
        if message.rstrip() != "":
           self.logger.log(self.level, message.rstrip())

sys.stdout = MyLogger(sys_logger, logging.INFO)
sys.stderr = MyLogger(sys_logger, logging.ERROR)

tsb_logger = logging.getLogger("TSHarvestLog")
tsb_logger.setLevel(logging.INFO)
tsb_formatter = logging.Formatter("%(asctime)s,%(message)s","%Y-%m-%d %H:%M:%S")
tsb_handler = logging.handlers.RotatingFileHandler("/var/log/tsharvest/tsharvest.log", maxBytes=10000, backupCount=8)
tsb_handler.setFormatter(tsb_formatter)
tsb_logger.addHandler(tsb_handler)


tb = ['tsbridge01', 'tsbridge02', 'tsbridge03']
nb = len(tb)
fname0 = ['/var/log/tsharvest/' + x + '/tsbridge.log' for x in tb]
fname1 = ['/var/log/tsharvest/' + x + '/tsbridge.log.1' for x in tb]

ssh = paramiko.SSHClient()
ssh.load_system_host_keys()
ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())

while True:
    for i in xrange(0,nb) :
        ssh.connect(tb[i], username="totem")
        sftp = ssh.open_sftp()
        sftp.get('/var/log/tsbridge/tsbridge.log', fname0[i])
        sftp.get('/var/log/tsbridge/tsbridge.log.1', fname1[i])
        sftp.close()
        ssh.close()

    ftstime = [''] * nb
    frest = [''] * nb

    for i in xrange(0,nb) :
        f0 = open(fname0[i])
        f1 = open(fname1[i])
        lines = [r for r in csv.reader(f1)] + [r for r in csv.reader(f0)]
        ftstime[i] = [x[0] for x in lines]
        frest[i] = [','.join(x[1:]) for x in lines]
        f0.close()
        f1.close()
        
    fnumlines = [len(x) for x in ftstime]
    shortest_i  = fnumlines.index(min(fnumlines))
    first_tstime = ftstime[shortest_i][0]

    in_list = [first_tstime in x for x in ftstime] 
    if all(in_list) :
        idx = [x.index(first_tstime) for x in ftstime] 
        rlen = [len(x) - x.index(first_tstime) for x in ftstime] 
        min_rlen = min(rlen)
        t = ftstime[0][idx[0]:][:min_rlen]
        for i in xrange(0,nb) :
            r = frest[i][idx[i]:][:min_rlen]
            q = zip(t, r)
            t = [','.join(x) for x in q]

        with open("/var/log/tsharvest/tsbridgeall/next_index", 'r') as f:
            x = f.readline().strip()
        with open("/var/log/tsharvest/tsbridgeall/all" + x + ".log", 'w') as f:
            for i in xrange(0, len(t)) :
                f.write(t[i] + '\n')
        nxt = int(x) + 1
        if nxt == 10 :
            nxt = 0
        with open("/var/log/tsharvest/tsbridgeall/next_index", 'w') as f:
            f.write(str(nxt) + '\n')
    else:
        idx = [0] * nb
        rlen = [0] * nb
        nxt = 99 

    tsb_logger.info("%s,%s,%s,%s,%s,%s,%s,%s,%s,%s" % (fnumlines[0], fnumlines[1], fnumlines[2], idx[0], idx[1], idx[2], rlen[0], rlen[1], rlen[2],nxt))
    time.sleep(15)
