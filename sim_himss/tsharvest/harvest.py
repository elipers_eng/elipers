import paramiko
import csv

tb = ['tsbridge01', 'tsbridge02', 'tsbridge03']
nb = len(tb)
fname0 = ['/var/log/tsharvest/' + x + '/tsbridge.log' for x in tb]
fname1 = ['/var/log/tsharvest/' + x + '/tsbridge.log.1' for x in tb]

ssh = paramiko.SSHClient()
ssh.load_system_host_keys()
ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())

for i in xrange(0,nb) :
    ssh.connect(tb[i], username="totem")
    sftp = ssh.open_sftp()
    sftp.get('/var/log/tsbridge/tsbridge.log', fname0[i])
    sftp.get('/var/log/tsbridge/tsbridge.log.1', fname1[i])
    sftp.close()
    ssh.close()

ftstime = [''] * nb
frest = [''] * nb

for i in xrange(0,nb) :
    f0 = open(fname0[i])
    f1 = open(fname1[i])
    lines = [r for r in csv.reader(f1)] + [r for r in csv.reader(f0)]
    ftstime[i] = [x[0] for x in lines]
    frest[i] = [','.join(x[1:]) for x in lines]
    f0.close()
    f1.close()
    
fnumlines = [len(x) for x in ftstime]
shortest_i  = fnumlines.index(min(fnumlines))
first_tstime = ftstime[shortest_i][0]

idx = [x.index(first_tstime) for x in ftstime] 
rlen = [len(x) - x.index(first_tstime) for x in ftstime] 
min_rlen = min(rlen)


t = ftstime[0][idx[0]:][:min_rlen]
for i in xrange(0,nb) :
    r = frest[i][idx[i]:][:min_rlen]
    q = zip(t, r)
    t = [','.join(x) for x in q]

for i in xrange(0, len(t)) :
    print t[i]
