import csv
import time

nf = 10
flist = [None] * nf

for i in xrange(0, nf):
    with open("/var/log/tsharvest/tsbridgeall/all" + str(i) + ".log") as f:
        flist[i] = [r for r in csv.reader(f)]

        
lasttime = [x[-1][0] for x in flist]
lasttime_t = [time.strptime(x, "%Y-%m-%d %H:%M:%S") for x in lasttime]

min_idx = lasttime_t.index(min(lasttime_t))

oflist = flist[min_idx:] + flist[:min_idx]
otlist = [[q[0] for q in p] for p in oflist]
endtime = [x[-1][0] for x in oflist]

#print "oflist"
#for i in xrange(0, nf):
#    print oflist[i][0][0], "to", oflist[i][-1][0]


otidx = [0] * nf    
for i in xrange(0, nf-1):
    otidx[i+1] = otlist[i+1].index(endtime[i]) + 1
    
#print "otlist"
#for i in xrange(0, nf):
#    print otlist[i][otidx[i]], "to",  otlist[i][-1], "(", len(otlist[i][otidx[i]:]) , ")"

mflist = []
for i in xrange(0, nf):
    mflist = mflist + oflist[i][otidx[i]:]

for i in xrange(0, len(mflist)):
    print ','.join(mflist[i])
   

    
