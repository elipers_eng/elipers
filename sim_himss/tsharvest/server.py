import tornado.ioloop
import tornado.web
import tornado.websocket
import tornado.template

import csv
import time

with open("harvest_header.csv") as f:
    rows = csv.reader(f)
    header = [r for r in rows][0]

nf = 10
flist = [None] * nf

mflist = []
line_num = 0
line_max = 0

def line_field(i, f):
    if f[0] == 'ALL':
        return mflist[i]
    chk = [x in header for x in f]
    if all(x == True for x in chk) == False:
        return []
    idx = [header.index(x) for x in f]
    ln = mflist[i]
    val = [ln[x] for x in idx]
    return val

class MainHandler(tornado.web.RequestHandler):
    def get(self):
        loader = tornado.template.Loader(".")
        self.write(loader.load("index.html").generate())
        
class WSHandler(tornado.websocket.WebSocketHandler):
    def check_origin(self, origin):
        return True

    def open(self):
        print 'connection opened...'

    def on_message(self, message):
        global line_num
        global line_max
        print 'received:', message

        msg = message.split(' ')
        if len(msg) > 0:
            cmd = msg[0]
            msg.pop(0)
            if cmd == 'READ' :
                if len(msg) > 0 :
                    val = line_field(line_num, msg)
                    string = 'DATA ' + ' '.join(val)
                    if len(val) > 0:
                        line_num += 1 
                        if line_num == line_max :
                            line_num = 0
                    else:
                        string = 'NACK'
            elif cmd == 'START' :
                string = 'ACK'
            elif cmd == 'STOP' :
                string = 'ACK'
            else:
                string = 'NACK'

        self.write_message(string)
        print 'send: ', string

    def on_close(self):
        print 'connection closed...'

for i in xrange(0, nf):
    with open("/var/log/tsharvest/tsbridgeall/all" + str(i) + ".log") as f:
        flist[i] = [r for r in csv.reader(f)]

lasttime = [x[-1][0] for x in flist]
lasttime_t = [time.strptime(x, "%Y-%m-%d %H:%M:%S") for x in lasttime]
min_idx = lasttime_t.index(min(lasttime_t))

oflist = flist[min_idx:] + flist[:min_idx]
otlist = [[q[0] for q in p] for p in oflist]
endtime = [x[-1] for x in otlist]

otidx = [0] * nf    
for i in xrange(0, nf-1):
    otidx[i+1] = otlist[i+1].index(endtime[i]) + 1

for i in xrange(0, nf):
    mflist += oflist[i][otidx[i]:]

line_num = 0
line_max = len(mflist)

application = tornado.web.Application([
    (r'/', WSHandler),
])

if __name__ == "__main__":
    print 'Ready...'
    print line_max
    application.listen(8080)
    tornado.ioloop.IOLoop.instance().start()
          

