from datetime import datetime
from datetime import timedelta
debug = 0

tempg = '%d/%m/%y %H:%M:%S.%f'
templ = '%S.%f'

#f = open("test")
#f = open("test2")
f = open("Shez_FP_1_Jersey_2.csv")

if debug == 1 :
  print ">> PREAMBLE <<"

line = f.readline().strip()
while True:
  if line == '' :
    break
  s = line.split(",")
#  print len(s), s
  if s[0] == '"Date"' :
    start_date = s[1]
  elif s[0] == '"Time"' :
    start_time = s[1]
  elif s[0] == '"Sample Rate"' :
    rate = s[1]

  line = f.readline().strip()

t = start_date.replace('"','') + ' ' + start_time.replace('"','') + '.0'
start = datetime.strptime(t, tempg)
step = float(1)/int(rate.replace('"',''))

if debug == 1 :
 print start
 print step

if debug == 1 :
 print ">> INDEX <<"

header = ""

line = f.readline().strip()
while True:
  if line == '' :
    break
  if (header == "") :
    header = line.strip(',')
  line = f.readline().strip()

tsheader = '"TStime",'
print tsheader + header

if debug == 1 :
  print ">> BODY <<"
#  f.close()
#  quit()

linehold=""
timehold=start;

line = f.readline().strip()
while True:
  if line == '' :
    print str(timehold) + ',' + linehold
    break
  s = line.replace('"','').split(",")
  sec = float(s[0])
  if (linehold != "") :
    if (sec != 0) :
      print str(timehold) + ',' + linehold
    else :
      timehold_s = float(timehold.microsecond)/1000000
      if timehold_s%step == 0 :
        start = timehold
      else :
        start = timehold + timedelta(seconds=(step - timehold_s%step))
  linehold = ','.join(s)
  timehold = start + timedelta(seconds=sec)
  line = f.readline().strip()

if debug == 1 :
  print str(timehold)

f.close()
