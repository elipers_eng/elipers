from datetime import datetime
from datetime import timedelta

f1 = open("../bodycaptest/Shez_3_1.csv")
f2 = open("../bodycaptest/Shez_3_2.csv")
#f1 = open('x')
#f2 = open('y')

testmode=0
trim_begin = 1
trim_end = 1

temp = '%Y-%m-%d %H:%M:%S'

#
# Header
#
l1 = f1.readline().strip()
l2 = f2.readline().strip()

h1 = l1.split(',')
h2 = l2.split(',')

if testmode == 1:
  print h1[0]
  print h2[0]

if h1[0] != '"TStime"' or h2[0] != '"TStime"' :
  print "Non TStime formatted"
  quit()

print (',').join(h1 + h2[1:])

if testmode == 1 :
  last1 = "BLANK"
  last2 = "BLANK"
else :
  last1 = ','.join(["" for x in h1[1:]])
  last2 = ','.join(["" for x in h2[1:]])

blank1 = 1
blank2 = 1

#
# Body
#

l1 = f1.readline()
l2 = f2.readline()

s1 = l1.strip().split(',')
s2 = l2.strip().split(',')

b1 = datetime.strptime(s1[0], temp)
b2 = datetime.strptime(s2[0], temp)
if testmode == 1:
  print b1
  print b2

while True:
  if l1 == '' and l2 == '' :
    break
  elif l1 == '' :
    if trim_end == 1 :
      break
    s2 = l2.strip().split(',')
    r2 = ','.join(s2[1:])
    print s2[0] + ',' + last1 + ',' + r2
    last2 = r2
    blank2 = 0
    l2 = f2.readline()
  elif l2 == '' :
    if trim_end == 1 :
      break
    s1 = l1.strip().split(',')
    r1 = ','.join(s1[1:])
    print s1[0] + ',' + r1 + ',' + last2
    last1 = r1
    blank1 = 0
    l1 = f1.readline()
  else :
    s1 = l1.strip().split(',')
    r1 = ','.join(s1[1:])
    s2 = l2.strip().split(',')
    r2 = ','.join(s2[1:])
    if s1[0] < s2[0]:
      if trim_begin == 0 or blank2 == 0 :
        print s1[0] + ',' + r1 + ',' + last2
      last1 = r1
      blank1 = 0
      l1 = f1.readline()
    elif s2[0] < s1[0]:
      if trim_begin ==0 or blank1 == 0 :
        print s2[0] + ',' + last1 + ',' + r2
      last2 = r2
      blank2 = 0
      l2 = f2.readline()
    else :
      print s1[0] + ',' + r1 + ',' + r2
      last1 = r1
      last2 = r2
      blank1 = 0
      blank2 = 0
      l1 = f1.readline()
      l2 = f2.readline()

if testmode == 1:
  print last1
  print last2

f1.close()
f2.close()
