from datetime import datetime
from datetime import timedelta
debug = 0

templ = '%Y.%m.%d %H.%M.%S'

f = open("X_R1-RABID-27.csv")
#f = open("test2")

if debug == 1 :
  print ">> PREAMBLE <<"

if debug == 1 :
  print ">> INDEX <<"

header = ""

line = f.readline().strip()
s = line.split(",")
hhmmi = s.index('"HHMM"')
mmddi = s.index('"MMDD"')
sshhi = s.index('"SSHH"')

#print hhmmi, mmddi, sshhi

header = line
tsheader = '"TStime",'
print tsheader + header

line = f.readline().strip()

if debug == 1 :
  print ">> BODY <<"
#  f.close()
#  quit()


line = f.readline().strip()
while True:
  if line == '' :
    break
  s = line.replace('"','').split(",")
  time = datetime.strptime('2016.' + s[mmddi] + ' ' + s[hhmmi] + '.' + s[sshhi].split('.')[0], templ)
#  print str(time), s[0]
  print str(time) + ',' + ','.join(s)

  line = f.readline().strip()

if debug == 1 :
 print ">> POSTAMBLE <<"

f.close()
