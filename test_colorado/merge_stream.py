from __future__ import print_function
import csv
import math
import sys
import itertools

s0 = 'LOG_SN'
s1 = '_Join_'
s3 = '.csv'

imu = [40095, 40099]

tstr = 'timeOfWeek'
#tstr = 'time'

sns = ['ins1', 'ins2', 'preintegratedImu_tow', 'barometer_tow']
sns_fld = [['lla[0]','lla[1]','lla[2]','theta[0]', 'theta[1]', 'theta[2]'],
           ['lla[0]','lla[1]','lla[2]','uvw[0]', 'uvw[1]', 'uvw[2]'],
           ['theta1[0]','theta1[1]','theta1[2]','theta2[0]','theta2[1]','theta2[2]','vel1[0]','vel1[1]','vel1[2]','vel2[0]','vel2[1]','vel2[2]'],
           ['bar','barTemp']]

imu_sns = [str(q[0])+'/'+s0+str(q[0])+s1+q[1]+s3 for q in itertools.product(imu, sns)]
imu_sns2 = [str(q[0])+'_'+q[1] for q in itertools.product(imu, sns)]

tll = []
for fname in imu_sns:
    with open(fname) as f:
        reader = csv.reader(f)
        flist = list(reader)
    hdr = flist[0]
    print(hdr, file=sys.stderr)
    tidx = hdr.index(tstr)    
    flist.pop(0)
    tlist = [round(float(q[tidx]),4) for q in flist]
    tll.append(tlist)

timestep = min([round((q[-1]-q[0])/len(q),3) for q in tll])
startt = max([q[0] for q in tll])
starttq = math.ceil(startt/timestep) * timestep

endt = min([q[-1] for q in tll])
endtq = math.floor(endt/timestep) * timestep

starttf = [[p <= starttq for p in q] for q in tll]
startidx = [q.index(False) - 1 for q in starttf]

numstream = len(tll)

# this test has to be all [True, False]
for i in range(0,numstream):
    print(starttf[i][startidx[i]:startidx[i]+2], file=sys.stderr)

endtf = [[p >= endtq for p in q] for q in tll]
endidx = [q.index(True)+1 for q in endtf]

# this test has to be all [False, True]
for i in range(0,numstream):
    print(endtf[i][endidx[i]-2:endidx[i]], file=sys.stderr)

tlloverlap = [q[0][q[1]:q[2]] for q in zip(tll, startidx, endidx)]

timescale = [round(starttq + i*timestep,3) for i in range(0, int((endtq - starttq)/timestep)+2)]

    
for j in range(0,numstream):
    k = 0
    for i in range(0, len(timescale)):
        if timescale[i] <= tlloverlap[j][k]:
            k += 0
#            print('GOOD1', timescale[i],tlloverlap[j][k], i, k)
        elif timescale[i] < tlloverlap[j][k+1]:
            k += 0
#            print('GOOD2', timescale[i],tlloverlap[j][k], i, k)
        else:
            k += 1
#            print('BUMP', timescale[i],tlloverlap[j][k], i, k)
    
vcol = []
for j in range(0, numstream):
    k = 0
    col = []
    for i in range(0, len(timescale)):
        if timescale[i] <= tlloverlap[j][k]:
            k += 0
        elif timescale[i] < tlloverlap[j][k+1]:
            k += 0
        else:
            k += 1
        col.append(k)
    vcol.append(col)

snsxx = sns_fld * numstream
snsxxl = [[tstr] + timescale]
for j in range(0, numstream):
    print('FILE', imu_sns[j], file=sys.stderr)
    with open(imu_sns[j]) as f:
        reader = csv.reader(f)
        flist = list(reader)
    hdr = flist[0]
    flist.pop(0)
    for field in snsxx[j]:
        fieldname = field + '_' + str(imu_sns2[j])
        print('FIELD', fieldname, file=sys.stderr)
        fieldidx = hdr.index(field)
        colx = [float(q[fieldidx]) for q in flist[startidx[j]:endidx[j]]]
        colxstretch = [colx[i] for i in vcol[j]]
        print('LENGTH', len(colx), 'TO', len(colxstretch), file=sys.stderr)
        snsxxl.append([fieldname] + colxstretch)

for i in range(0, len(timescale)+1):
    print(','.join([str(p) for p in [q[i] for q in snsxxl]]))
