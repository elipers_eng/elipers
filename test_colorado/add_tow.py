from __future__ import print_function
import csv
import math
import sys

s0 = 'LOG_SN'
s1 = '_Join_'
s3 = '.csv'

imu = [40095]
#imu = [40099]
sns0 = 'gps1Pos'
sns1 = 'barometer'
#sns1 = 'preintegratedImu'

imu_sns0 = str(imu[0])+'/'+s0+str(imu[0])+s1+sns0+s3
imu_sns1 = str(imu[0])+'/'+s0+str(imu[0])+s1+sns1+s3

with open(imu_sns0) as f:
    reader = csv.reader(f)
    glist = list(reader)
    ghdr = glist[0]
    glist.pop(0)
gtowidx = ghdr.index('timeOfWeekMs')
goffidx = ghdr.index('towOffset')

gxlist = [[float(q[gtowidx])/1000, float(q[goffidx])] for q in glist]
gxlist2 = [round(q[0]-q[1],4) for q in gxlist]

with open(imu_sns1) as f:
    reader = csv.reader(f)
    plist = list(reader)
    phdr = plist[0]
    plist.pop(0)
ptimeidx = phdr.index('time')
    
pxlist = [round(float(q[ptimeidx]),4) for q in plist]

curroff = gxlist[0][1]
j = 0
towlist = ['timeOfWeek']
for i in range(0, len(plist)):
#for i in range(0, 1000):
#    print(gxlist2[j], pxlist[i], curroff, pxlist[i] + curroff, gxlist[j][0], gxlist[j][1])
    towlist.append(round(pxlist[i] + curroff,4))
    if gxlist2[j] < pxlist[i]:
        if j+1 < len(gxlist):
            j += 1
            curroff = gxlist[j][1]
    else:
        j += 0

x = [phdr] + plist
z = [q[0] +[q[1]]for q in zip(x, towlist)]

imu_snso = str(imu[0])+'/'+s0+str(imu[0])+s1+sns1+'_tow'+s3
with open(imu_snso, 'wt') as f:
    writer = csv.writer(f)
    writer.writerows(z)
    
