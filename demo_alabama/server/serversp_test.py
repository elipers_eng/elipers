import tornado.ioloop
import tornado.web
import tornado.websocket
import tornado.template

import csv
import time
import threading
import numpy
import random

from operator import itemgetter
from itertools import groupby
from numpy import array
from time import gmtime, strftime


with open("tllvzep_noheader.csv") as f:
    flist = [r for r in csv.reader(f)]

with open("tllvzep_header.csv") as f:
    hlist = [r for r in csv.reader(f)]

header = hlist[0]
header[3:3] = ['Sec', 'Lap']

print "HAHA"
        
with open("latlng_part.csv") as f:
    plist = [r for r in csv.reader(f)]

sec_part = [int(y) for y in plist[0]]

with open("latlng.csv") as f:
    llist = [r for r in csv.reader(f)]

llist2 = [[float(y) for y in x] for x in llist]

ya = []
for i in xrange(0, len(sec_part)):
    ya.append(sum(sec_part[0:i]))

llsec = []
for i in xrange(0, len(sec_part)):
    llsec.append(llist2[ya[i]:ya[i]+sec_part[i]])
                
llsec_center = [x[len(x)/2] for x in llsec]

#num_sec = 100
#len_sec = 11
#llsec_old = []
#for i in xrange(0, num_sec):
#    llsec_old.append(llist2[i*len_sec:(i+1)*len_sec])
#llsec_center_old = [x[len_sec/2] for x in llsec]

def ll2sec(ll):
    ld = [(ll[0]-p[0])**2 + (ll[1]-p[1])**2 for p in llsec_center]
    return ld.index(min(ld))

#for i in xrange(0, num_sec):
#    print i, ll2sec(llsec[i][len_sec-1])

lap_num = 1
lap_max = 30
cnt_num = 0
cnt_max = 10

line_cnt = 0
stream_th = None
stream_ev = None

lasttime = flist[-1][0]
lasttime_t = time.strptime(lasttime, "%Y-%m-%d %H:%M:%S")

mflist = []

start_poll = True

max_vector_len = 100
lat_vector = []
lng_vector = []
sec_vector = []
lap_vector = []
dat_vector = []

mflist_max = 100
mflist_new = False

def threaded_function(arg):
    global mflist
    global mflist_new
    global start_poll
    global lap_num
    global cnt_num
    while True:
        if start_poll == True:
            line_x = random.randint(0, len(flist)-1)
            ll = flist[line_x]
#            filltime = strftime("%Y-%m-%d %H:%M:%S", gmtime())             
#            ll[0] = filltime
#            foobar = [str(y) for y in [ll2sec([float(x) for x in ll[1:3]]), lap_num]]
#            print len(ll), ll[0:7]
#            ll[3:3] = foobar
            filltime = strftime("%Y-%m-%d %H:%M:%S", gmtime())             
            latlon = ll[1:3]
            rest = ll[3:]
            foobar = [str(y) for y in [ll2sec([float(x) for x in ll[1:3]]), lap_num]]
            ll_new = [filltime] + latlon +  foobar + rest
#            print len(ll), len(ll_new)
            cnt_num += 1
            if cnt_num == cnt_max:
                lap_num = 1 + lap_num%lap_max
                cnt_num = 0
#            mflist.append(ll)
            mflist.append(ll_new)
            mflist_new = True
            if len(mflist) == mflist_max+1:
                mflist.pop(0)
#            print "FILL 1:", len(mflist), filltime
        else:
            print "no poll"
        time.sleep(1)

def line_field(i, f):
    if f[0] == 'ALL':
        return mflist[i]
    chk = [x in header for x in f]
    if all(x == True for x in chk) == False:
        return []
    idx = [header.index(x) for x in f]
    ln = mflist[i]
    val = [ln[x] for x in idx]
    return val

class MainHandler(tornado.web.RequestHandler):
    def get(self):
        loader = tornado.template.Loader(".")
        self.write(loader.load("index.html").generate())
        
class WSHandler(tornado.websocket.WebSocketHandler):
    def tf(self, ev, msg):
        global line_cnt
        global mflist_new
        global lat_vector
        global lng_vector
        global sec_vector
        global lap_vector
        global dat_vector
        while True:
            if ev.isSet():
                return
            if mflist_new:
                if (msg[0] != 'ALL') :
                    line_x = len(mflist)-1
                    emptytime = mflist[line_x][0]
                    if len(dat_vector) > 0:
                        val = line_field(line_x, ['Latitude', 'Longitude', 'Sec', 'Lap', msg[0]])
                        if len(val) > 0:
                            line_cnt += 1 
                            lat_vector.append(val[0])
                            lng_vector.append(val[1])
                            sec_vector.append(val[2])
                            lap_vector.append(val[3])
                            dat_vector.append(float(val[4]))
                            if len(lat_vector) == max_vector_len+1:
                                lat_vector.pop(0)
                                lng_vector.pop(0)
                                sec_vector.pop(0)
                                lap_vector.pop(0)
                                dat_vector.pop(0)
                        print "EMPTY 1:", line_x, '/', len(mflist), emptytime
                    else:
                        toload = min(len(mflist), max_vector_len)
                        line_x = len(mflist) - toload
                        for i in xrange(0, toload):
                            val = line_field(line_x+i, ['Latitude', 'Longitude', 'Sec', 'Lap', msg[0]])
                            line_cnt += 1 
                            lat_vector.append(val[0])
                            lng_vector.append(val[1])
                            sec_vector.append(val[2])
                            lap_vector.append(val[3])
                            dat_vector.append(float(val[4]))
                        print "EMPTY n:", toload, emptytime
                    if len(lat_vector) > 1: 
                        dat_min = min(dat_vector)
                        dat_rng = max(dat_vector) - min(dat_vector)
                        if dat_rng > 0:
                            dat_nvector = [2*((x - dat_min)/dat_rng) - 1 for x in dat_vector]
                            dat_sort = sorted(dat_nvector)
                            if len(lat_vector) > 2:
                                print min(dat_vector), '[', dat_sort[0], dat_sort[1], dat_sort[-2], dat_sort[-1], ']', max(dat_vector)
                        else:
                            dat_nvector = [0] * len(dat_vector)
                    else:
                        dat_nvector = [0]
#                    print dat_nvector[0], '...', dat_nvector[-1], '(', len(dat_vector), ')'
                    nrm_vector = [str(x) for x in dat_nvector]
                    llm = zip(lat_vector, lng_vector, sec_vector, lap_vector, nrm_vector)
                    string = 'VECTOR ' + ' '.join(' '.join(p) for p in llm)  
#                    print string
                    self.write_message(string)

                    time.sleep(0.5)

                    val = line_field(line_x, ['ALL'])
                    string = 'DATA ' + ' '.join(val)
#                    print string
                    self.write_message(string)
                else:
                    print "EMPTY 0:"
                mflist_new = False;  
            else:
                print "EMPTY 0:"
            time.sleep(0.5)

    def td(self, ev, msg):
        global line_cnt
        global mflist_new
        while True:
            if ev.isSet():
                return
            if mflist_new:
                line_x = len(mflist)-1
                emptytime = mflist[line_x][0]
                val = line_field(line_x, msg)
                if (msg[0] == 'ALL') :
                    string = 'DATA ' + ' '.join(val)
                else:
                    string = 'DATA ' + ' '.join(val)
                if len(val) > 0:
                    line_cnt += 1 
                    print "EMPTY 1:", line_x, '/', len(mflist), emptytime
                self.write_message(string)
                mflist_new = False;  
            else:
                print "EMPTY 0:"
            time.sleep(1)

    def tv(self, ev, msg):
        global line_cnt
        global mflist_new
        global lat_vector
        global lng_vector
        global sec_vector
        global lap_vector
        global dat_vector
        while True:
            if ev.isSet():
                return
            if mflist_new:
                if len(dat_vector) > 0:
                    line_x = len(mflist)-1
                    emptytime = mflist[line_x][0]
                    if (msg[0] != 'ALL') :
                        val = line_field(line_x, ['Latitude', 'Longitude', 'Sec', 'Lap', msg[0]])
                        if len(val) > 0:
                            line_cnt += 1 
                            lat_vector.append(val[0])
                            lng_vector.append(val[1])
                            sec_vector.append(val[2])
                            lap_vector.append(val[3])
                            dat_vector.append(float(val[4]))
                            if len(lat_vector) == max_vector_len+1:
                                lat_vector.pop(0)
                                lng_vector.pop(0)
                                sec_vector.pop(0)
                                lap_vector.pop(0)
                                dat_vector.pop(0)
                    print "EMPTY 1:", line_x, '/', len(mflist), emptytime
                else:
                    toload = min(len(mflist), max_vector_len)
                    if (msg[0] != 'ALL') :
                        line_x = len(mflist) - toload
                        for i in xrange(0, toload):
                            val = line_field(line_x+i, ['Latitude', 'Longitude', 'Sec', 'Lap', msg[0]])
                            line_cnt += 1 
                            lat_vector.append(val[0])
                            lng_vector.append(val[1])
                            sec_vector.append(val[2])
                            lap_vector.append(val[3])
                            dat_vector.append(float(val[4]))
                    print "EMPTY ", toload
                if len(lat_vector) > 1: 
                    dat_min = min(dat_vector)
                    dat_rng = max(dat_vector) - min(dat_vector)
                    if dat_rng > 0:
                        dat_nvector = [2*((x - dat_min)/dat_rng) - 1 for x in dat_vector]
                        dat_sort = sorted(dat_nvector)
                        if len(lat_vector) > 2:
                            print min(dat_vector), '[', dat_sort[0], dat_sort[1], dat_sort[-2], dat_sort[-1], ']', max(dat_vector)
                    else:
                        dat_nvector = [0] * len(dat_vector)
                else:
                    dat_nvector = [0]
#                print dat_nvector[0], '...', dat_nvector[-1], '(', len(dat_vector), ')'
                nrm_vector = [str(x) for x in dat_nvector]
#                print dat_vector
#                print nrm_vector
                llm = zip(lat_vector, lng_vector, sec_vector, lap_vector, nrm_vector)
                string = 'VECTOR ' + ' '.join(' '.join(p) for p in llm)  
#                print string
                self.write_message(string)
                mflist_new = False;  
            else:
                print "EMPTY 0:"
            time.sleep(1)

    def check_origin(self, origin):
        return True

    def open(self):
        print 'connection opened...'

    def on_message(self, message):
        global line_cnt
        global stream_th
        global stream_ev
        global lat_vector
        global lng_vector
        global sec_vector
        global lap_vector
        global dat_vector
        line_max = len(mflist)
        print 'received:', message

        msg = message.split(' ')
        if len(msg) > 0:
            cmd = msg[0]
            msg.pop(0)
            if cmd == 'SET' :
                slop = 5
                if line_max - line_num < slop:
                    string = 'NACK'
                else:
                    line_num = line_max-slop
                    string = 'ACK'
            elif cmd == 'READ' :
                if len(msg) > 0 :
                    line_x = len(mflist)-1
                    emptytime = mflist[line_x][0]
                    val = line_field(line_x, msg)
                    if (msg[0] == 'ALL') :
                        string = 'DATA ' + ' '.join(val)
                    else:
                        string = 'DATA ' + ' '.join(val)
                    if len(val) > 0:
                        line_cnt += 1 
                        print "EMPTY 1:", line_x, '/', len(mflist), emptytime
                    else:
                        string = 'NACK'
                else:
                    string = 'NACK'
            elif cmd == 'VECTOR' :
                if len(msg) > 0 :
                    line_x = len(mflist)-1
                    emptytime = mflist[line_x][0]
                    if (msg[0] != 'ALL') :
                        val = line_field(line_x, ['Latitude', 'Longitude', 'Sec', 'Lap', msg[0]])
                        if len(val) > 0:
                            line_cnt += 1 
                            lat_vector.append(val[0])
                            lng_vector.append(val[1])
                            sec_vector.append(val[2])
                            lap_vector.append(val[3])
                            dat_vector.append(float(val[4]))
                            if len(lat_vector) == max_vector_len+1:
                                lat_vector.pop(0)
                                lng_vector.pop(0)
                                sec_vector.pop(0)
                                lap_vector.pop(0)
                                dat_vector.pop(0)
                    print "EMPTY 1:", line_x, '/', len(mflist), emptytime
                    if len(lat_vector) > 1: 
                        dat_min = min(dat_vector)
                        dat_rng = max(dat_vector) - min(dat_vector)
                        if dat_rng > 0:
                            dat_nvector = [2*((x - dat_min)/dat_rng) - 1 for x in dat_vector]
                            dat_sort = sorted(dat_nvector)
                            if len(lat_vector) > 2:
                                print min(dat_vector), '[', dat_sort[0], dat_sort[1], dat_sort[-2], dat_sort[-1], ']', max(dat_vector)
                        else:
                            dat_nvector = [0] * len(dat_vector)
                    else:
                        dat_nvector = [0]
#                    print dat_nvector[0], '...', dat_nvector[-1], '(', len(dat_vector), ')'
                    nrm_vector = [str(x) for x in dat_nvector]
#                    print dat_vector
#                    print nrm_vector
                    llm = zip(lat_vector, lng_vector, sec_vector, lap_vector, nrm_vector)
                    string = 'VECTOR ' + ' '.join(' '.join(p) for p in llm)  
#                    print string
                else:
                    string = 'NACK'
            elif cmd == 'START' :
                if len(msg) > 0 :
                    stream_ev = threading.Event()
                    stream_th = threading.Thread(target=self.tf, args=(stream_ev,msg,))
                    stream_th.start()
                    string = 'ACK'
                else:
                    string = 'NACK'
            elif cmd == 'STARTD' :
                if len(msg) > 0 :
                    stream_ev = threading.Event()
                    stream_th = threading.Thread(target=self.td, args=(stream_ev,msg,))
                    stream_th.start()
                    string = 'ACK'
                else:
                    string = 'NACK'
            elif cmd == 'STARTV' :
                if len(msg) > 0 :
                    stream_ev = threading.Event()
                    stream_th = threading.Thread(target=self.tv, args=(stream_ev,msg,))
                    stream_th.start()
                    string = 'ACK'
                else:
                    string = 'NACK'
            elif cmd == 'STOP' :
                stream_ev.set()
                stream_th.join()
                stream_ev = None
                stream_th = None
                string = 'ACK'
            elif cmd == 'CLEAR' :
                lat_vector = []
                lng_vector = []
                sec_vector = []
                lap_vector = []
                dat_vector = []
                string = 'ACK'
            elif cmd == 'LAPV' :
                dat_min = min(dat_vector)
                dat_rng = max(dat_vector) - min(dat_vector)
                dat_nvector = [2*((x - dat_min)/dat_rng) - 1 for x in dat_vector]
                if len(msg) > 0:
                    lapx = msg[0]
                    if lapx == 'ALL':
                        startidx = 0
                        endidx = len(lap_vector)
                    else :
                        if lapx in lap_vector:
                            startidx = lap_vector.index(lapx)
                            lapy = str(int(lapx) + 1)
                            if lapy in lap_vector:
                                endidx = lap_vector.index(lapy)
                            else:
                                endidx = len(lap_vector)
                        else :
                            startidx = -1
                    if startidx != -1:
                        print startidx, endidx
                        lat_sub = lat_vector[startidx:endidx]
                        lng_sub = lng_vector[startidx:endidx]
                        sec_sub = sec_vector[startidx:endidx]
                        lap_sub = lap_vector[startidx:endidx]
                        nrm_vector = [str(x) for x in dat_nvector]
                        nrm_sub = nrm_vector[startidx:endidx]
                        print lap_sub
                        print nrm_sub
                        llm = zip(lat_sub, lng_sub, sec_sub, lap_sub, nrm_sub)
                        string = 'VECTOR ' + ' '.join(' '.join(p) for p in llm)  
                    else:
                        string = 'NACK'
                else:
                    string = 'NACK'
            else:
                string = 'NACK'

        self.write_message(string)
        print 'send: ', string

    def on_close(self):
        global stream_th
        global stream_ev
        if stream_th != None:
            print 'stopping stream'
            stream_ev.set()
            stream_th.join()
        print 'connection closed...'

application = tornado.web.Application([
    (r'/', WSHandler),
])

if __name__ == "__main__":
    print len(mflist)
    thread = threading.Thread(target=threaded_function, args=(10,))
    thread.start()
    print 'Ready...'
    application.listen(8080)
    tornado.ioloop.IOLoop.instance().start()
