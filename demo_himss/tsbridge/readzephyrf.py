import bluetooth
import time

lifesign_msg = '\x02\x23\x00\x00\x03'
getserial_msg = '\x02\x0b\x00\x00\x03'
setgenon_msg = '\x02\x14\x01\x01\x5e\x03'
setgenoff_msg = '\x02\x14\x01\x00\x00\x03'
setbrton_msg = '\x02\x15\x01\x01\x5e\x03'
setbrtoff_msg = '\x02\x15\x01\x00\x00\x03'
setecgon_msg = '\x02\x16\x01\x01\x5e\x03'
setecgoff_msg = '\x02\x16\x01\x00\x00\x03'
setaccon_msg = '\x02\x1e\x01\x01\x5e\x03'
setaccoff_msg = '\x02\x1e\x01\x00\x00\x03'
setsumon_msg = '\x02\xbd\x02\x01\x00\xc4\x03'
setsumoff_msg = '\x02\xbd\x02\x00\x00\x00\x03'

ser = bluetooth.BluetoothSocket(bluetooth.RFCOMM)
ser.connect(('A0:E6:F8:4A:8F:36', 1))
turnon = 0

def byte2unsigned(x):
    return x

def word2unsigned(x,y):
    return x*256 + y

def byte2signed(x):
    if x > 127 :
        return (256-x) * (-1)
    else :
        return x

def word2signed(x,y):
    if x > 127 :
        return (65536 - (x*256+y)) * (-1)
    else :
        return x*256 + y

def tsunsigned(x, s):
    if x == 65535 :
        return 'nan'
    if s == 1 :
        return str(x)
    return str(float(x)/s)
        
def tsunsignedb(x, s):
    if x == 255 :
        return 'nan'
    if s == 1 :
        return str(x)
    return str(float(x)/s)
        
def tssigned(x, s):
    if x == -32768 :
        return 'nan'
    if s == 1 :
        return str(x)
    return str(float(x)/s)
    
def tssignedb(x, s):
    if x == -128 :
        return 'nan'
    if s == 1 :
        return str(x)
    return str(float(x)/s)
    
def five_to_four(x) :
    y0 = (x[0]) + ((x[1] & 0x03) << 8)
    y1 = ((x[1] >> 2) & 0x3f) + ((x[2] & 0x0f) << 6)
    y2 = ((x[2] >> 4) & 0x0f) + ((x[3] & 0x3f) << 4)
    y3 = ((x[3] >> 6) & 0x03) + (x[4] << 2)
    return [y0, y1, y2, y3]

def four_to_three(x) :
    y0 = (x[0]) + ((x[1] & 0x03) << 8)
    y1 = ((x[1] >> 2) & 0x3f) + ((x[2] & 0x0f) << 6)
    y2 = ((x[2] >> 4) & 0x0f) + ((x[3] & 0x3f) << 4)
    return [y0, y1, y2]

def three_to_two(x) :
    y0 = (x[0]) + ((x[1] & 0x03) << 8)
    y1 = ((x[1] >> 2) & 0x3f) + ((x[2] & 0x0f) << 6)
    return [y0, y1]

while True:
    t = time.strftime("%Y-%m-%d %H:%M:%S") 
    x = ser.recv(1024)
    y = [hex(ord(i)) for i in x]
    y2 = [ord(i) for i in x]
    if y[1] == '0x23' :
        z = "RCV LIFESIGN"
        if turnon == 0 :
            ser.send(setgenoff_msg)
            ser.send(setbrtoff_msg)
            ser.send(setecgoff_msg)
            ser.send(setaccoff_msg)
            ser.send(setsumoff_msg)
            print t, ": SEND SETTURNOFF"
            turnon = 1
        elif turnon == 1 : 
            ser.send(setgenon_msg)
#            ser.send(setbrton_msg)
#            ser.send(setecgon_msg)
#            ser.send(setaccon_msg)
#            ser.send(setsumon_msg)
            print t, ": SEND SETTURNON"
            turnon = 2
        else :
            ser.send(lifesign_msg)
            print t, ": SEND LIFESIGN"
    elif y[1] == '0x14' :
        z = "RCV GENACK"
    elif y[1] == '0x15' :
        z = "RCV BRTACK"
    elif y[1] == '0x16' :
        z = "RCV ECGACK"
    elif y[1] == '0x1e' :
        z = "RCV ACCACK"
    elif y[1] == '0xbd' :
        z = "RCV SUMACK"
    else :
        if y[1] == '0x20' :
            z = "RCV GEN"
            heart = tsunsigned(word2unsigned(y2[13],y2[12]),1)
            resp  = tsunsigned(word2unsigned(y2[15],y2[14]),10)
            sktp  = tssigned(word2signed(y2[17],y2[16]),10)
            pos   = tssigned(word2signed(y2[19],y2[18]),1)
            vmu   = tsunsigned(word2unsigned(y2[21],y2[20]),100)
            pacc  = tsunsigned(word2unsigned(y2[23],y2[22]),100)
            volt  = tsunsigned(word2unsigned(y2[25],y2[24]),1000)
            brta  = tsunsigned(word2unsigned(y2[27],y2[26]),1)
            ecga  = tsunsigned(word2unsigned(y2[29],y2[28]),1000000)
            ecgn  = tsunsigned(word2unsigned(y2[31],y2[30]),1000000)
            acvm  = tssigned(word2signed(y2[33],y2[32]),100)
            acvp  = tssigned(word2signed(y2[35],y2[34]),100)
            aclm  = tssigned(word2signed(y2[37],y2[36]),100)
            aclp  = tssigned(word2signed(y2[39],y2[38]),100)
            acsm  = tssigned(word2signed(y2[41],y2[40]),100)
            acsp  = tssigned(word2signed(y2[43],y2[42]),100)
            za = 'heart='+heart+',resp='+resp+',sktp='+sktp+',pos='+pos+',vmu='+vmu+',pacc='+pacc+',volt='+volt+',brta='+brta+',ecga='+ecga+',ecgn='+ecgn+',acvm='+acvm+',acvp='+acvp+',aclm='+aclm+',aclp='+aclp+',acsm='+acsm+',acsp='+acsp
#            za = heart+','+resp+','+sktp+','+pos+','+vmu+','+pacc+','+volt+','+brta+','+ecga+','+ecgn+','+acvm+','+acvp+','+aclm+','+aclp+','+acsm+','+acsp
            zb = ','.join(y[12:43])
            print t, ": ", za
            print t, ": ", zb
        elif y[1] == '0x21' :
            z = "RCV BRT"
            c1 = five_to_four(y2[12:17])
            c2 = five_to_four(y2[17:22])
            c3 = five_to_four(y2[22:27])
            c4 = five_to_four(y2[27:32])
            c5 = three_to_two(y2[32:35])
            print t, ": ", ':'.join([str(x) for x in c1+c2+c3+c4+c5])
        elif y[1] == '0x22' :
            z = "RCV ECG"
            c1 = five_to_four(y2[12:17])
            c2 = five_to_four(y2[17:22])
            c3 = five_to_four(y2[22:27])
            c4 = five_to_four(y2[27:32])
            c5 = five_to_four(y2[32:37])
            c6 = five_to_four(y2[37:42])
            c7 = five_to_four(y2[42:47])
            c8 = five_to_four(y2[47:52])
            c9 = five_to_four(y2[52:57])
            c10 = five_to_four(y2[57:62])
            c11 = five_to_four(y2[62:67])
            c12 = five_to_four(y2[67:72])
            c13 = five_to_four(y2[72:77])
            c14 = five_to_four(y2[77:82])
            c15 = five_to_four(y2[82:87])
            c16 = four_to_three(y2[87:91])
            print t, ": ", ':'.join([str(x) for x in c1+c2+c3+c4+c5+c6+c7+c8+c9+c10+c11+c12+c13+c14+c15+c16])
        elif y[1] == '0x25' :
            z = "RCV ACC"
            c1 = five_to_four(y2[12:17])
            c2 = five_to_four(y2[17:22])
            c3 = five_to_four(y2[22:27])
            c4 = five_to_four(y2[27:32])
            c5 = five_to_four(y2[32:37])
            c6 = five_to_four(y2[37:42])
            c7 = five_to_four(y2[42:47])
            c8 = five_to_four(y2[47:52])
            c9 = five_to_four(y2[52:57])
            c10 = five_to_four(y2[57:62])
            c11 = five_to_four(y2[62:67])
            c12 = five_to_four(y2[67:72])
            c13 = five_to_four(y2[72:77])
            c14 = five_to_four(y2[77:82])
            c15 = five_to_four(y2[82:87])
            print t, ": ", ':'.join([str(x) for x in c1+c2+c3+c4+c5+c6+c7+c8+c9+c10+c11+c12+c13+c14+c15])
        elif y[1] == '0x2b' :
            z = "RCV SUM"
            heart = tsunsigned(word2unsigned(y2[14],y2[13]),1)
            resp  = tsunsigned(word2unsigned(y2[16],y2[15]),10)
            sktp  = tssigned(word2signed(y2[18],y2[17]),10)
            pos   = tssigned(word2signed(y2[20],y2[19]),1)
            vmu   = tsunsigned(word2unsigned(y2[22],y2[21]),100)
            pacc  = tsunsigned(word2unsigned(y2[24],y2[23]),100)
            volt  = tsunsigned(word2unsigned(y2[26],y2[25]),1000)
            batt  = tsunsignedb(byte2unsigned(y2[27]),1)
            brta  = tsunsigned(word2unsigned(y2[29],y2[28]),1)
            brtn  = tsunsigned(word2unsigned(y2[31],y2[30]),1)
            brtc  = tsunsignedb(byte2unsigned(y2[32]),1)
            ecga  = tsunsigned(word2unsigned(y2[34],y2[33]),1000000)
            ecgn  = tsunsigned(word2unsigned(y2[36],y2[35]),1000000)
            hrtc  = tsunsignedb(byte2unsigned(y2[37]),1)
            hrtv  = tsunsigned(word2unsigned(y2[39],y2[38]),1)
            acvm  = tssigned(word2signed(y2[46],y2[45]),100)
            acvp  = tssigned(word2signed(y2[48],y2[47]),100)
            aclm  = tssigned(word2signed(y2[50],y2[49]),100)
            aclp  = tssigned(word2signed(y2[52],y2[51]),100)
            acsm  = tssigned(word2signed(y2[54],y2[53]),100)
            acsp  = tssigned(word2signed(y2[56],y2[55]),100)
            intp  = tssigned(word2signed(y2[58],y2[57]),10)
            ectp  = tssigned(word2signed(y2[65],y2[64]),10)
            za = 'heart='+heart+',resp='+resp+',sktp='+sktp+',pos='+pos+',vmu='+vmu+',pacc='+pacc+',volt='+volt+',batt='+batt+',brta='+brta+',brtn='+brtn+',brtc='+brtc+',hrtc='+hrtc+',hrtv='+hrtv+',ecga='+ecga+',ecgn='+ecgn+',acvm='+acvm+',acvp='+acvp+',aclm='+aclm+',aclp='+aclp+',acsm='+acsm+',acsp='+acsp+',intp='+intp+',ectp='+ectp
            zb = ','.join(y[13:72])
            print t, ": ", za
            print t, ": ", zb
    print t, ": ", z

    
    
