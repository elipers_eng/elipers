import bluetooth

setgenoff_msg = '\x02\x14\x01\x00\x00\x03'
setbrtoff_msg = '\x02\x15\x01\x00\x00\x03'
setecgoff_msg = '\x02\x16\x01\x00\x00\x03'
setaccoff_msg = '\x02\x1e\x01\x00\x00\x03'
setsumoff_msg = '\x02\xbd\x02\x00\x00\x00\x03'

ser = bluetooth.BluetoothSocket(bluetooth.RFCOMM)
ser.connect(('A0:E6:F8:4A:8F:36', 1))

ser.send(setgenoff_msg)
ser.send(setbrtoff_msg)
ser.send(setecgoff_msg)
ser.send(setaccoff_msg)
ser.send(setsumoff_msg)
