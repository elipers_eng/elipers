#!/usr/bin/env python

import logging
import logging.handlers
import sys
import time
import subprocess
import threading
#import gps
#from gps import *
import serial
import bluetooth

gpsd = None #seting the global variable

class GpsPoller(threading.Thread):
    def __init__(self):
        threading.Thread.__init__(self)
        global gpsd #bring it in scope
        gpsd = gps(mode=WATCH_ENABLE) #starting the stream of info
        self.current_value = None
        self.running = True #setting the thread running to true

    def run(self):
        global gpsd
        while gpsp.running:
            gpsd.next() #this will continue to loop and grab EACH set of gpsd info to clear the buffer

class BCD:
    def __init__(self):
        self.index = 'null'
        self.batt = 'null'
        self.temp = 'null'

bcd = None #seting the global variable
ser = None #seting the global variable

class BCPoller(threading.Thread):
    def __init__(self):
        threading.Thread.__init__(self)
        global bcd #bring it in scope
        global ser #bring it in scope
        ser = serial.Serial("/dev/ttyUSB0", 115200, timeout=1)
        if(ser.isOpen() == True):
            ser.close()
        ser.open()
        ser.write(b"\x02")
        x = ser.read()
        x = ser.readline()
        ser.write(b"\x40")
        x = ser.read()
        ser.write(b"\xf5")
        ser.write(b"\x01")
        x = ser.read()
        ser.write(b"\x23")
        ser.write(b"\x03")
        x = ser.read()
        bcd = BCD()
        self.current_value = None
        self.running = True #setting the thread running to true

    def run(self):
        global bcd
        global ser
        l = ser.readline()
        while bcp.running:
            if len(l) > 0 :
                f = l.split()
                dst_type =  f[4] + f[5] + f[6]
                dst_id = f[7]
                src =  f[8]
                cmd =  f[11]
                if dst_type == "808000" and cmd == "06" :
                    bcd.index = str(int(f[14] + f[13] + f[12], 16))
                    bcd.batt  = str(int(f[16] + f[15], 16))
                    bcd.temp  = str(float(int(f[18] + f[17], 16)) / 10)
            l = ser.readline()
            
class ZEP:
    def __init__(self):
        self.first_life = 0
        self.gen_packet = []
        self.brt_packet = []
        self.ecg_packet = []
        self.acc_packet = []
        self.sum_packet = []

lifesign_msg = '\x02\x23\x00\x00\x03'
setgenon_msg = '\x02\x14\x01\x01\x5e\x03'
setbrton_msg = '\x02\x15\x01\x01\x5e\x03'
setecgon_msg = '\x02\x16\x01\x01\x5e\x03'
setaccon_msg = '\x02\x1e\x01\x01\x5e\x03'
setsumon_msg = '\x02\xbd\x02\x01\x00\xc4\x03'

zep = None #seting the global variable
rfs = None #seting the global variable
gen_tmp = []
brt_tmp = []
ecg_tmp = []
acc_tmp = []
sum_tmp = []

def byte2unsigned(x):
    return x

def word2unsigned(x,y):
    return x*256 + y

def byte2signed(x):
    if x > 127 :
        return (256-x) * (-1)
    else :
        return x

def word2signed(x,y):
    if x > 127 :
        return (65536 - (x*256+y)) * (-1)
    else :
        return x*256 + y

def tsunsigned(x, s):
    if x == 65535 :
        return 'null'
    if s == 1 :
        return str(x)
    return str(float(x)/s)
        
def tsunsignedb(x, s):
    if x == 255 :
        return 'null'
    if s == 1 :
        return str(x)
    return str(float(x)/s)
        
def tssigned(x, s):
    if x == -32768 :
        return 'null'
    if s == 1 :
        return str(x)
    return str(float(x)/s)

def tssignedb(x, s):
    if x == -128 :
        return 'null'
    if s == 1 :
        return str(x)
    return str(float(x)/s)

class ZepPoller(threading.Thread):
    def __init__(self):
        threading.Thread.__init__(self)
        global zep #bring it in scope
        global rfs #bring it in scope
        rfs = bluetooth.BluetoothSocket(bluetooth.RFCOMM)
        rfs.connect(('A0:E6:F8:4A:8F:36', 1))
        zep = ZEP()
        self.current_value = None
        self.running = True #setting the thread running to true
    def run(self):
        global zep
        global rfs
        global gen_tmp
        global brt_tmp
        global ecg_tmp
        global acc_tmp
        global sum_tmp
        while zpp.running:
            xr = rfs.recv(1024)
            yr = [hex(ord(i)) for i in xr]
            yr2 = [ord(i) for i in xr]
            if yr[1] == '0x23' :
                if zep.first_life == 0 :
                    rfs.send(setgenon_msg)
                    rfs.send(setbrton_msg)
                    rfs.send(setecgon_msg)
                    rfs.send(setaccon_msg)
                    rfs.send(setsumon_msg)
                    zep.first_life = 1
                rfs.send(lifesign_msg)
            elif yr[1] == '0x20' :
                zep.gen_packet = gen_tmp
                zep.brt_packet = brt_tmp
                zep.ecg_packet = ecg_tmp
                zep.acc_packet = acc_tmp
                zep.sum_packet = sum_tmp
                gen_tmp = [yr2]
                brt_tmp = []
                ecg_tmp = []
                acc_tmp = []
                sum_tmp = []
            elif yr[1] == '0x21' :
                brt_tmp.append(yr2)
            elif yr[1] == '0x22' :
                ecg_tmp.append(yr2)
            elif yr[1] == '0x25' :
                acc_tmp.append(yr2)
            elif yr[1] == '0x2b' :
                sum_tmp.append(yr2)

sys_logger = logging.getLogger(__name__)
sys_logger.setLevel(logging.INFO)
sys_formatter = logging.Formatter("%(asctime)s - %(name)s - %(levelname)s - %(message)s")
sys_handler = logging.FileHandler("/var/log/tsbridge/tsbridge_sys.log")
sys_handler.setFormatter(sys_formatter)
sys_logger.addHandler(sys_handler)

class MyLogger(object):
    def __init__(self, logger, level):
        self.logger = logger
        self.level = level

    def write(self, message):
        if message.rstrip() != "":
           self.logger.log(self.level, message.rstrip())

sys.stdout = MyLogger(sys_logger, logging.INFO)
sys.stderr = MyLogger(sys_logger, logging.ERROR)

tsb_logger = logging.getLogger("TSBridgeLog")
tsb_logger.setLevel(logging.INFO)
tsb_formatter = logging.Formatter("%(asctime)s,%(message)s","%Y-%m-%d %H:%M:%S")
tsb_handler = logging.handlers.RotatingFileHandler("/var/log/tsbridge/tsbridge.log", maxBytes=10000, backupCount=8)
#tsb_handler = logging.handlers.TimedRotatingFileHandler("/var/log/tsbridge/tsbridge.log", when="h", interval=6, backupCount=12)
tsb_handler.setFormatter(tsb_formatter)
tsb_logger.addHandler(tsb_handler)

#gpsp = GpsPoller()
#gpsp.start()

bcp = BCPoller()
bcp.start()

zpp = ZepPoller()
zpp.start()

while True:
#    x = subprocess.check_output(['vcgencmd','measure_temp'])
#    temp=x[x.index('=')+1:x.index('C')-1]
#    y = subprocess.check_output(['vcgencmd','measure_volts'])
#    volt=y[y.index('=')+1:y.index('V')]
    temp = 0
    volt = 0
#    lat = gpsd.fix.latitude 
#    lon = gpsd.fix.longitude 
#    ys = [x.used for x in gpsd.satellites].count(True)
    lat = 0
    lon = 0
    ys = 0
    bi = bcd.index
    bb = bcd.batt
    bt = bcd.temp
#    bi = 'null'
#    bb = 'null'
#    bt = 'null'
    zgl = len(zep.gen_packet)
    if zgl == 0 :
        heart = 'null'
        resp  = 'null'
        sktp  = 'null'
        pos   = 'null'
        vmu   = 'null'
        pacc  = 'null'
        volt  = 'null'
        brta  = 'null'
        ecga  = 'null'
        ecgn  = 'null'
        acvm  = 'null'
        acvp  = 'null'
        aclm  = 'null'
        aclp  = 'null'
        acsm  = 'null'
        acsp  = 'null'
    else :
        zgp = zep.gen_packet[0]
        heart = tsunsigned(word2unsigned(zgp[13],zgp[12]),1)
        resp  = tsunsigned(word2unsigned(zgp[15],zgp[14]),10)
        sktp  = tssigned(word2signed(zgp[17],zgp[16]),10)
        pos   = tssigned(word2signed(zgp[19],zgp[18]),1)
        vmu   = tsunsigned(word2unsigned(zgp[21],zgp[20]),100)
        pacc  = tsunsigned(word2unsigned(zgp[23],zgp[22]),100)
        volt  = tsunsigned(word2unsigned(zgp[25],zgp[24]),1000)
        brta  = tsunsigned(word2unsigned(zgp[27],zgp[26]),1)
        ecga  = tsunsigned(word2unsigned(zgp[29],zgp[28]),1000000)
        ecgn  = tsunsigned(word2unsigned(zgp[31],zgp[30]),1000000)
        acvm  = tssigned(word2signed(zgp[33],zgp[32]),100)
        acvp  = tssigned(word2signed(zgp[35],zgp[34]),100)
        aclm  = tssigned(word2signed(zgp[37],zgp[36]),100)
        aclp  = tssigned(word2signed(zgp[39],zgp[38]),100)
        acsm  = tssigned(word2signed(zgp[41],zgp[40]),100)
        acsp  = tssigned(word2signed(zgp[43],zgp[42]),100)
    zsl = len(zep.sum_packet)
    if zsl == 0 :
        batt = 'null'
        intp = 'null'
        ectp = 'null'
    else :
        zsp = zep.sum_packet[0]
        batt  = tsunsignedb(byte2unsigned(zsp[27]),1)
        intp  = tssigned(word2signed(zsp[58],zsp[57]),10)
        ectp  = tssigned(word2signed(zsp[65],zsp[64]),10)
    logstr = ("%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s" % (heart,resp,sktp,pos,vmu,pacc,volt,brta,ecga,ecgn,acvm,acvp,aclm,aclp,acsm,acsp,batt,intp,ectp,bi,bb,bt))
#    llstr  = time.strftime("%Y-%m-%d %H:%M:%S") + ',' + logstr
    tsb_logger.info(logstr)
#    x = subprocess.check_output(['/home/pi/release_1_3_1/projects/symphony_test/exe/symphony_test', '-D', '/dev/ttyUSB2', '--send_ack', llstr])
    time.sleep(1)
    



