import paramiko
import time
import datetime


ssh = paramiko.SSHClient()
ssh.load_system_host_keys()
ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())

count = 0
while True:
    ssh.connect("tsbridge01", username="pi")
    sftp = ssh.open_sftp()
    sftp.get('/var/log/tsbridge/tsbridge.log', '/tmp/tsbridge.log')
    sftp.get('/var/log/tsbridge/tsbridge.log.1', '/tmp/tsbridge.log.1')
    sftp.close()
    ssh.close()
    print str(datetime.datetime.now()), ":", count
    count += 1
    time.sleep(10)
