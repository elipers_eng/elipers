import tornado.ioloop
import tornado.web
import tornado.websocket
import tornado.template

import csv
import time
import threading

from pyspark import SparkContext

sc = SparkContext()

print "Spark started"
data = sc.textFile("/home/totem/data.txt")
from pyspark.mllib.regression import LabeledPoint
from pyspark.mllib.tree import DecisionTree
def parsePoint(line): values = [float(x) for x in line.split(' ')]; return LabeledPoint(values[0], values[1:])

parsedData = data.map(parsePoint)
parsedData.count()

model = DecisionTree.trainClassifier(parsedData, numClasses=3, categoricalFeaturesInfo={}, impurity='gini')
print "Spark ready"
#model = None

with open("lap.csv") as f:
    rows = csv.reader(f)
    location = [r for r in rows]
    
loc_pointer = 0
loc_max = len(location)

with open("harvest_header.csv") as f:
    rows = csv.reader(f)
    header = [r for r in rows][0]

nf = 6
flist = [None] * nf

mflist = []
line_num = 0
stream_th = None
stream_ev = None

for i in xrange(0, nf):
    with open("/var/log/tsharvest/tsbridgeall/all" + str(i) + ".log") as f:
        flist[i] = [r for r in csv.reader(f)]

lasttime = [x[-1][0] for x in flist]
lasttime_t = [time.strptime(x, "%Y-%m-%d %H:%M:%S") for x in lasttime]
min_idx = lasttime_t.index(min(lasttime_t))

oflist = flist[min_idx:] + flist[:min_idx]
otlist = [[q[0] for q in p] for p in oflist]
endtime = [x[-1] for x in otlist]

otidx = [0] * nf    
for i in xrange(0, nf-1):
    if endtime[i] in otlist[i+1]:
        otidx[i+1] = otlist[i+1].index(endtime[i]) + 1
    else:
        print "panic 1"

for i in xrange(0, nf):
    mflist += oflist[i][otidx[i]:]

def threaded_function(arg):
    global mflist
#    for i in xrange(0, arg):
    while True:
        for i in xrange(0, nf):
            with open("/var/log/tsharvest/tsbridgeall/all" + str(i) + ".log") as f:
                flist[i] = [r for r in csv.reader(f)]
        lasttime = [x[-1][0] for x in flist]
        lasttime_t = [time.strptime(x, "%Y-%m-%d %H:%M:%S") for x in lasttime]
        min_idx = lasttime_t.index(min(lasttime_t))
        endtime = mflist[-1][0]
        max_idx = (min_idx-1)%nf
        maxlist = flist[max_idx]
        maxtlist = [q[0] for q in maxlist]
        if endtime in maxtlist:
            maxidx = maxtlist.index(endtime) + 1
#            print endtime, maxidx, len(maxtlist)
            if maxidx != len(maxtlist) :
                mflist += maxlist[maxidx:]
            print "FILL:", len(maxtlist) - maxidx, '[', line_num, ':', len(mflist), ']'
        else:
            print "panic 2"
        time.sleep(5)

def line_field(i, f):
    if f[0] == 'ALL':
        return mflist[i]
    chk = [x in header for x in f]
    if all(x == True for x in chk) == False:
        return []
    idx = [header.index(x) for x in f]
    ln = mflist[i]
    val = [ln[x] for x in idx]
    return val

class MainHandler(tornado.web.RequestHandler):
    def get(self):
        loader = tornado.template.Loader(".")
        self.write(loader.load("index.html").generate())
        
class WSHandler(tornado.websocket.WebSocketHandler):
    def tf(self, ev, msg):
        global line_num
        global loc_pointer
        while True:
            if ev.isSet():
                return
            line_max = len(mflist)
            if line_num < line_max:
                val = line_field(line_num, msg)
                if (msg[0] == 'ALL') :
                    lat = location[loc_pointer][0]
                    lon = location[loc_pointer][1]
                    loc_pointer = (loc_pointer+1)%loc_max
                    print val[5], val[6]
                    x = val[5]
                    y = val[6]
                    if model == None :
                        z = 0
                    else:
                        z = int(model.predict([float(x), float(y)]))
                    stro = 'DATA ' + ' '.join(val) + ' ' + lat + ' ' + lon + ' ' + str(z)
                else:
                    stro = 'DATA ' + ' '.join(val)
                if len(val) > 0:
                    line_num += 1
                    print "EMPTY:", 1, '[', line_num, ':', line_max, ']'
                    self.write_message(stro)
            else:        
                print "EMPTY:", 0, '[', line_num, ':', line_max, ']'
            time.sleep(1)

    def check_origin(self, origin):
        return True

    def open(self):
        print 'connection opened...'

    def on_message(self, message):
        global line_num
        global stream_th
        global stream_ev
        line_max = len(mflist)
        print 'received:', message

        msg = message.split(' ')
        if len(msg) > 0:
            cmd = msg[0]
            msg.pop(0)
            if cmd == 'SET' :
                slop = 5
                if line_max - line_num < slop:
                    string = 'NACK'
                else:
                    line_num = line_max-slop
                    string = 'ACK'
            elif cmd == 'READ' :
                if line_num < line_max :
                    if len(msg) > 0 :
                        val = line_field(line_num, msg)
                        if (msg[0] == 'ALL') :
                            lat = location[loc_pointer][0]
                            lon = location[loc_pointer][1]
                            loc_pointer = (loc_pointer+1)%loc_max
                            x = val[5]
                            y = val[6]
                            if model == None :
                                z = 0
                            else:
                                z = int(model.predict([float(x), float(y)]))
                            string = 'DATA ' + ' '.join(val) + ' ' + lat + ' ' + lon + ' ' + str(z)
                        else:
                            string = 'DATA ' + ' '.join(val)
                        if len(val) > 0:
                            line_num += 1 
                            print "EMPTY:", 1, '[', line_num, ':', line_max, ']'
                        else:
                            string = 'NACK'
                else:
                    print "EMPTY:", 0, '[', line_num, ':', line_max, ']'
                    string = 'NODATA'
            elif cmd == 'START' :
                stream_ev = threading.Event()
                stream_th = threading.Thread(target=self.tf, args=(stream_ev,msg,))
                stream_th.start()
                string = 'ACK'
            elif cmd == 'STOP' :
                stream_ev.set()
                stream_th.join()
                stream_ev = None
                stream_th = None
                string = 'ACK'
            else:
                string = 'NACK'

        self.write_message(string)
        print 'send: ', string

    def on_close(self):
        global stream_th
        global stream_ev
        if stream_th != None:
            print 'stopping stream'
            stream_ev.set()
            stream_th.join()
        print 'connection closed...'

application = tornado.web.Application([
    (r'/', WSHandler),
])

if __name__ == "__main__":
    print len(mflist)
    thread = threading.Thread(target=threaded_function, args=(10,))
    thread.start()
    print 'Ready...'
    application.listen(8080)
    tornado.ioloop.IOLoop.instance().start()
