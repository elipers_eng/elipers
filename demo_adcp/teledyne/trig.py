from pyspark.sql.session import SparkSession

spark = SparkSession.builder.master("local").appName("simple").enableHiveSupport().getOrCreate()

df = spark.read.option("header","true").option("inferSchema","true").csv("/tmp/LTA.csv")

from pyspark.sql.functions import atan2, hypot, sin, cos, when

#
# select intersted fields
#
en = "ensemble_ID"
ts = "human_time"

nv  = "nav_avg_speed"
ntd = "nav_avg_track_true"

# east, north, up, error  
c1bev = "velocity_beam1_cell_1"
c1bnv = "velocity_beam2_cell_1"
c1buv = "velocity_beam3_cell_1"
c1brv = "velocity_beam4_cell_1"

df1 = df.select(en, ts, nv, ntd, c1bev, c1bnv, c1buv, c1brv)
df1.persist()

df4 = df1.withColumn('ntd', 2 * 3.1415 * ((65536+ df[ntd])%65536)/65536)\
.withColumn('c1bhv', hypot(df1[c1bev], df1[c1bnv]))\
.withColumn('c1bav', 3.1415 + atan2(df1[c1bev], df1[c1bnv]))\
.withColumn('nnv', df1[nv] * cos(2 * 3.1415 * ((65536+ df[ntd])%65536)/65536 - 3.1415))\
.withColumn('nev', df1[nv] * sin(2 * 3.1415 * ((65536+ df[ntd])%65536)/65536 - 3.1415))
#df4.select(en, nv, 'ntd', 'nev', 'nnv', c1bev, c1bnv, 'c1bhv', 'c1bav').show(20)
df4.filter(df[en]%10 == 0).select(en, nv, 'ntd', 'nev', 'nnv', c1bev, c1bnv, 'c1bhv', 'c1bav').show(140)

print df4.stat.corr(nv, 'c1bhv')
print df4.stat.corr('ntd', 'c1bav')
print df4.stat.corr('nev', c1bev)
print df4.stat.corr('nnv', c1bnv)

