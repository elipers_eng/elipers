from pyspark.sql.session import SparkSession

sparkSession = SparkSession.builder.master("local").appName("simple").enableHiveSupport().getOrCreate()

df = sparkSession.read.option("header","true").option("inferSchema","true").csv("LEG3015_000000_1_export.csv")

nan = -32768

numcells = 75
numrows = 1390

en = 'Ens'

from pyspark.sql.functions import col, avg, sqrt

x = [en]
for i in range(1, 26):
    x.append('C' + str(i*3) + 'B3I')

df.filter(df[en]%10 == 0).select(x).show(139)

x = []
for i in range(11, 76):
    x.append('C' + str(i) + 'B3I')

rs = df.filter(df[en] > 850).filter(df[en] < 951).select(x).rdd.take(100)
for i in range(0, 100):
    r1 = rs[i]
    dmin = min(r1)
    dmax = max(r1)
    imin = r1.index(dmin)
    imax = r1.index(dmax)
    print imin+11, imax+11, r1.__fields__[imin], r1.__fields__[imax]

