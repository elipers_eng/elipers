from pyspark.sql.session import SparkSession

sparkSession = SparkSession.builder.master("local").appName("simple").enableHiveSupport().getOrCreate()

df = sparkSession.read.option("header","true").option("inferSchema","true").csv("ADCP055_000001_1_export.csv")

nan = -32768

numcells = 30
numrows = 13157

from pyspark.sql.functions import col, avg, sqrt

avg_cell_beam = [[0 for x in xrange(4)] for x in xrange(numcells+1)]
avg_cell_good = [0 for x in xrange(numcells+1)]

for i in range(1,numcells+1):
    x1 = "C" + str(i) + "B1V"
    x2 = "C" + str(i) + "B2V"
    x3 = "C" + str(i) + "B3V"
    x4 = "C" + str(i) + "B4V"
    dfx = df.filter(df[x1] != nan).filter(df[x2] != nan).filter(df[x3] != nan).filter(df[x4] != nan)
    pgood = (dfx.count() * 100) / numrows
    avg_cell_good[i] = pgood
    avg_cell_beam[i][0] = dfx.agg(avg(df[x1])).rdd.take(1)[0][0]
    avg_cell_beam[i][1] = dfx.agg(avg(df[x2])).rdd.take(1)[0][0]
    avg_cell_beam[i][2] = dfx.agg(avg(df[x3])).rdd.take(1)[0][0]
    avg_cell_beam[i][3] = dfx.agg(avg(df[x4])).rdd.take(1)[0][0]


print avg_cell_beam
print avg_cell_good

dfk = df.select('C14B1V', 'C14B2V', 'C14B3V', 'C14B4V').filter(df['C14B1V'] != nan).filter(df['C14B2V'] != nan).filter(df['C14B3V'] != nan).filter(df['C14B4V'] != nan)
dfk.withColumn('rms123', sqrt(df['C14B1V'] * df['C14B1V'] + df['C14B2V'] * df['C14B2V'] + df['C14B3V'] * df['C14B3V'])).withColumn('rms234', sqrt(df['C14B2V'] * df['C14B2V'] + df['C14B3V'] * df['C14B3V'] + df['C14B4V'] * df['C14B4V'])).withColumn('rms341', sqrt(df['C14B3V'] * df['C14B3V'] + df['C14B4V'] * df['C14B4V'] + df['C14B1V'] * df['C14B1V'])).withColumn('rms412', sqrt(df['C14B4V'] * df['C14B4V'] + df['C14B1V'] * df['C14B1V'] + df['C14B2V'] * df['C14B2V'])).describe().show()
