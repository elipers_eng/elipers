from pyspark.sql.session import SparkSession

sparkSession = SparkSession.builder.master("local").appName("simple").enableHiveSupport().getOrCreate()

df = sparkSession.read.option("header","true").option("inferSchema","true").csv("LEG3015_000000_1_export.csv")

from pyspark.sql.functions import col, avg, sqrt

nan = -32768

en = 'Ens'
ts = 'TStime'

# theta = 20
#a = 1.4619; b = 0.2660; c = 1; d = 1.0337
# theta = 30
a = 1; b = 0.2887; c = 1; d = 0.7071

i = 1

cb1v = "C" + str(i) + "B1V"
cb2v = "C" + str(i) + "B2V"
cb3v = "C" + str(i) + "B3V"
cb4v = "C" + str(i) + "B4V"

xv = "C" + str(i) + "XV"
yv = "C" + str(i) + "YV"
zv = "C" + str(i) + "ZV"
ev = "C" + str(i) + "EV"
rv = "C" + str(i) + "RV"

#r1 = df.select(cb1v, cb2v, cb3v, cb4v).rdd.take(10)[0]
#x1 = c * a * (r1[0] - r1[1])
#y1 = c * a * (r1[3] - r1[2])
#z1 = b * (r1[0] + r1[1] + r1[2] + r1[3])
#e1 = d * (r1[0] + r1[1] - r1[2] - r1[3])
#print [x1, y1, z1, e1]

dfw = df.filter(df[cb1v] != nan).filter(df[cb2v] != nan).filter(df[cb3v] != nan).filter(df[cb4v] != nan)

dfx = dfw.withColumn(xv, c * a * (df[cb1v] - df[cb2v]))\
.withColumn(yv, c * a * (df[cb4v] - df[cb3v]))\
.withColumn(zv, b * (df[cb1v] + df[cb2v] + df[cb3v] + df[cb4v]))\
.withColumn(ev, d * (df[cb1v] + df[cb2v] - df[cb3v] - df[cb4v]))

dfy = dfx.withColumn(rv, sqrt(dfx[xv] * dfx[xv] + dfx[yv] * dfx[yv] + dfx[zv] * dfx[zv]))

dfy.select(en, ts, xv, yv, zv, ev, rv).show(20, False)

from pyspark.sql import Window
from pyspark.sql.functions import when, mean

min = df[en]%26
win = Window.orderBy(en).rowsBetween(0,25)
avgx= when(min == 0, mean(dfy[xv]).over(win)).otherwise(0)
avgy= when(min == 0, mean(dfy[yv]).over(win)).otherwise(0)
avgz= when(min == 0, mean(dfy[zv]).over(win)).otherwise(0)
avge= when(min == 0, mean(dfy[ev]).over(win)).otherwise(0)
avgr= when(min == 0, mean(dfy[rv]).over(win)).otherwise(0)
dfz = dfy.select(en, ts, xv, yv, zv, ev, rv).withColumn('min', min).withColumn('avgx', avgx)\
.withColumn('avgy', avgy).withColumn('avgz', avgz).withColumn('avge', avge).withColumn('avgr', avgr)
dfz.filter("min = 0").select(en, ts, 'avgx', 'avgy', 'avgz', 'avge', 'avgr').show(20, False)
