from pyspark import SparkContext
from pyspark.sql.session import SparkSession

sc = SparkContext()
spark = SparkSession.builder.master("local").appName("simple").enableHiveSupport().getOrCreate()

df = spark.read.option("header","true").option("inferSchema","true").csv("/tmp/LTA.csv")

from pyspark.sql.functions import sin, cos, when
from numpy import array
from math import sqrt
from pyspark.mllib.clustering import KMeans, KMeansModel

#
# select intersted fields
#
en = "ensemble_ID"

nv  = "nav_avg_speed"
ntd = "nav_avg_track_true"

nan = -32768
numcells = 75
numens = 1390

# east
cxbev  = ['velocity_beam1_cell_' + str(i+1) for i in list(range(numcells))]
cxbevd = ['vdiff_beam1_cell_' + str(i+1) for i in list(range(numcells))]

df1 = df.select([en, nv, ntd] + cxbev)

df2 = df1.withColumn('nev', df1[nv] * sin(2 * 3.1415 * ((65536+ df[ntd])%65536)/65536 - 3.1415))
for i in xrange(numcells):
    df2 = df2.withColumn(cxbevd[i], when(df2[cxbev[i]] != nan, df2[cxbev[i]] - df2['nev']).otherwise(nan))

# collect all east velocity of a cell
cell_x = cxbevd[10]
vlist_cell_x = df2.rdd.map(lambda x: x[cell_x]).collect()
#vlist_cell_x_avg =  sum(vlist_cell_x)/float(len(vlist_cell_x))

# break 1390 velocities into 139 arrays of 10 velocities, and create rdd
ll_cell_x = [array(vlist_cell_x[x*10:(x+1)*10]) for x in xrange(139)]
pd_cell_x = sc.parallelize(ll_cell_x)

# create a cluster based on the 139 arrays
clusters_cell_x = KMeans.train(pd_cell_x, 2, maxIterations=10,initializationMode="random")
classif_x = pd_cell_x.map(lambda point: clusters_cell_x.predict(point))
print clusters_cell_x.centers[0]
print clusters_cell_x.centers[1]
print classif_x.take(139)
print classif_x.map(lambda x: 1 if x == 1 else 0).reduce(lambda x, y: x + y)
print classif_x.map(lambda x: 0 if x == 1 else 1).reduce(lambda x, y: x + y)

# test the error
#def error(point):
#    center = clusters_cell_x.centers[clusters_cell_x.predict(point)]
#    return sqrt(sum([x**2 for x in (point - center)]))
#
#WSSSE = pd_cell_x.map(lambda point: error(point)).reduce(lambda x, y: x + y)
#print WSSSE

# collect all east velocity of a cell
cell_y = cxbevd[11]
vlist_cell_y = df2.rdd.map(lambda x: x[cell_y]).collect()

# break 1390 velocities into 139 arrays of 10 velocities, and create rdd
ll_cell_y = [array(vlist_cell_y[x*10:(x+1)*10]) for x in xrange(139)]
pd_cell_y = sc.parallelize(ll_cell_y)
classif_y = pd_cell_y.map(lambda point: clusters_cell_x.predict(point))

rd_cell_y = df2.filter(df2[en]%10 == 0).select(cell_y).rdd

a = rd_cell_y.repartition(1)
b = classif_y.repartition(1)
c = a.zip(b)
d = c.repartition(8)

from pyspark.sql import Row
def merge_row_pair(rp):
    return Row(\
        vdiff_beam1_cell_12=rp[0].vdiff_beam1_cell_12,\
        class_cell_12=rp[1])

e = d.map(merge_row_pair)
dfm = spark.createDataFrame(e)

dfm.show()
print dfm.filter(dfm.class_cell_12 == 0).count()
print dfm.filter(dfm.class_cell_12 == 1).count()

