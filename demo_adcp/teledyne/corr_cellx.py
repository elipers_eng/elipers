from pyspark.sql.session import SparkSession

import numpy as np

sparkSession = SparkSession.builder.master("local").appName("simple").enableHiveSupport().getOrCreate()

df = sparkSession.read.option("header","true").option("inferSchema","true").csv("ADCP055_000001_1_export.csv")

from datetime import datetime
from datetime import timedelta

temp1 = '%Y-%m-%d %H:%M:%S.%f'
tsstart = datetime.strptime('2004-09-20 08:27:07.58', temp1)
tsend  = datetime.strptime('2004-09-20 16:57:01.75', temp1)

t0 = datetime.strptime('2004-09-20 12:00:00.0', temp1)
dx = timedelta(minutes=30)

t = [0 for x in xrange(10)]
for i in xrange(10):
    t[i] = t0 + i * dx

enstart = 13158
enend = 26314

nan = -32768

#
# select intersted fields
#
en = "Ens"
ts = "TSTime"
tm = "Temp"
pr = "Pres"
dp = "Depth"
hd = "Heading"
pt = "Pitch"
rl = "Roll"

cell = "27"

cxb1v = "C" + cell + "B1V"; cxb2v = "C" + cell + "B2V"; cxb3v = "C" + cell + "B3V"; cxb4v = "C" + cell + "B4V"
cxb1i = "C" + cell + "B1I"; cxb2i = "C" + cell + "B2I"; cxb3i = "C" + cell + "B3I"; cxb4i = "C" + cell + "B4I"
cxb1c = "C" + cell + "B1C"; cxb2c = "C" + cell + "B2C"; cxb3c = "C" + cell + "B3C"; cxb4c = "C" + cell + "B4C"

dfk = df.select(en, ts, tm, pr, dp, hd, pt, rl, cxb1v, cxb2v, cxb3v, cxb4v, cxb1i, cxb2i, cxb3i, cxb4i, cxb1c, cxb2c, cxb3c, cxb4c)
dfk.persist()

for i in xrange(9):
    df1 = dfk.filter(df[ts] > t[i]).filter(df[ts] < t[i+1])
    df2 = df1.filter(df[cxb1v] != nan).filter(df[cxb2v] != nan).filter(df[cxb3v] != nan).filter(df[cxb4v] != nan)
    print df2.count()
    print (df2.count() * 100) / df1.count()
    print df2.stat.corr(cxb1v, cxb2v)
    print df2.stat.corr(cxb2v, cxb3v)
    print df2.stat.corr(cxb3v, cxb4v)
    print df2.stat.corr(cxb4v, cxb1v)
    
