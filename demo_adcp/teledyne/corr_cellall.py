from pyspark.sql.session import SparkSession

sparkSession = SparkSession.builder.master("local").appName("simple").enableHiveSupport().getOrCreate()

df = sparkSession.read.option("header","true").option("inferSchema","true").csv("LEG3015_000000_1_export.csv")

nan = -32768

numcells = 75
numrows = 1390

corr_cell_adjbeam = [[0 for x in xrange(4)] for x in xrange(numcells+1)]
corr_cell_good = [0 for x in xrange(numcells+1)]

for i in range(1,numcells+1):
    x1 = "C" + str(i) + "B1V"
    x2 = "C" + str(i) + "B2V"
    x3 = "C" + str(i) + "B3V"
    x4 = "C" + str(i) + "B4V"
    dfx = df.filter(df[x1] != nan).filter(df[x2] != nan).filter(df[x3] != nan).filter(df[x4] != nan)
    pgood = (dfx.count() * 100) / 1390
    corr_cell_good[i] = pgood
    corr_cell_adjbeam[i][0] = dfx.stat.corr(x1, x2)
    corr_cell_adjbeam[i][1] = dfx.stat.corr(x2, x3)
    corr_cell_adjbeam[i][2] = dfx.stat.corr(x3, x4)
    corr_cell_adjbeam[i][3] = dfx.stat.corr(x4, x1)

print corr_cell_adjbeam
print corr_cell_good

corr_adjcell_beam = [[0 for x in xrange(5)] for x in xrange(numcells+1)]
corr_adjcell_good = [[0 for x in xrange(5)] for x in xrange(numcells+1)]

for i in range(1,numcells):
    for j in range(1,5):
        xa = "C" + str(i) + "B" + str(j) + "V"
        xb = "C" + str(i+1) + "B" + str(j) + "V"
        dfx = df.filter(df[xa] != nan).filter(df[xb] != nan)
        corr_adjcell_good[i][j] = (dfx.count() * 100) / 1390
        corr_adjcell_beam[i][j] = dfx.stat.corr(xa, xb)

print corr_adjcell_beam
print corr_adjcell_good

