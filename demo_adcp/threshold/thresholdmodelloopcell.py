from pyspark import SparkContext
from pyspark.sql import SQLContext

from pyspark.sql.functions import mean, stddev
from pyspark.sql.functions import when
from pyspark.mllib.regression import LabeledPoint
from pyspark.mllib.classification import LogisticRegressionWithLBFGS

sc = SparkContext("local", "threshold")
sqlContext = SQLContext(sc)

df = sqlContext.read.format("com.databricks.spark.csv").option("header","true").option("inferSchema","true").load("/tmp/file1_2d.csv")

en = "ensemble_ID"
dy = "variable_RTC_day"
hr = "variable_RTC_hour"
mn = "variable_RTC_minute"
se = "variable_RTC_second"

# first complete: dya = 23; hra = 0; mna = 0
#dya = 23; hra = 8; mna = 0
#dya = 23; hra = 20; mna = 30
#dya = 24; hra = 0; mna = 0
dya = 24; hra = 12; mna = 30
# last complete: dya = 24; hra = 23; mna = 30

threshold = 15

print "Day " + str(dya) + " Hour " + str(hra) + " Minute " + str(mna)
print "Threshold +/- (" + str(threshold) + "/10) stddev"

for i in range(1,43):

	cell = str(i)

        print "Begin cell " + cell

	e1 = "echo_beam1_cell_" + cell
	e2 = "echo_beam2_cell_" + cell
	e3 = "echo_beam3_cell_" + cell
	e4 = "echo_beam4_cell_" + cell
	c1 = "correlation_beam1_cell_" + cell
	c2 = "correlation_beam2_cell_" + cell
	c3 = "correlation_beam3_cell_" + cell
	c4 = "correlation_beam4_cell_" + cell
	v1 = "velocity_beam1_cell_" + cell
	v2 = "velocity_beam2_cell_" + cell
	v3 = "velocity_beam3_cell_" + cell
	v4 = "velocity_beam4_cell_" + cell

        dfx = df.select(en, dy, hr, mn, se, e1, e2, e3, e4, c1, c2, c3, c4, v1, v2, v3, v4)
        dfx.persist()

        dfx_group = dfx.filter(df[dy] == dya).filter(df[hr] == hra).filter(df[mn] >= mna).filter(df[mn] <= mna+1)

        v1_mean = dfx_group.select([mean(v1).alias("xxx")]).collect()[0].xxx
        v2_mean = dfx_group.select([mean(v2).alias("xxx")]).collect()[0].xxx
        v3_mean = dfx_group.select([mean(v3).alias("xxx")]).collect()[0].xxx
        v4_mean = dfx_group.select([mean(v4).alias("xxx")]).collect()[0].xxx
        v1_stddev = dfx_group.select([stddev(v1).alias("xxx")]).collect()[0].xxx
        v2_stddev = dfx_group.select([stddev(v2).alias("xxx")]).collect()[0].xxx
        v3_stddev = dfx_group.select([stddev(v3).alias("xxx")]).collect()[0].xxx
        v4_stddev = dfx_group.select([stddev(v4).alias("xxx")]).collect()[0].xxx
        v1_upper = v1_mean + v1_stddev * threshold/10
        v2_upper = v2_mean + v2_stddev * threshold/10
        v3_upper = v3_mean + v3_stddev * threshold/10
        v4_upper = v4_mean + v4_stddev * threshold/10
        v1_lower = v1_mean - v1_stddev * threshold/10
        v2_lower = v2_mean - v2_stddev * threshold/10
        v3_lower = v3_mean - v3_stddev * threshold/10
        v4_lower = v4_mean - v4_stddev * threshold/10

        dfx_group_filtered = dfx_group.filter(df[v1] < v1_upper).filter(df[v1] > v1_lower).filter(df[v2] < v2_upper).filter(df[v2] > v2_lower).filter(df[v3] < v3_upper).filter(df[v3] > v3_lower).filter(df[v4] < v4_upper).filter(df[v4] > v4_lower)

        dfx_group.select(v1, v2, v3, v4).describe().show()
        dfx_group_filtered.select(v1, v2, v3, v4).describe().show()

        dfx_group_labeled = dfx_group.select(when((df[v1] < v1_upper) & (df[v1] > v1_lower) & (df[v2] < v2_upper) & (df[v2] > v2_lower) & (df[v3] < v3_upper) & (df[v3] > v3_lower) & (df[v4] < v4_upper) & (df[v4] > v4_lower), 1).otherwise(0).alias('vthresh'), e1, e2, e3, e4, c1, c2, c3, c4)

        corpus = dfx_group_labeled.rdd.map(lambda y: LabeledPoint(y[0], y[1:]))
	inclusive = corpus.filter(lambda p: (p.label == 1)).count() / float(corpus.count())
	print "Inclusivity (cell "+ cell + "): " + str(inclusive)

        model = LogisticRegressionWithLBFGS.train(corpus)
        print model

        eval_result = corpus.map(lambda p: (p.label, model.predict(p.features)))
        print eval_result.take(10)

        trainErr = eval_result.filter(lambda (v,p): v!= p).count() / float(eval_result.count())
        print "Training Error (cell "+ cell + "): " + str(trainErr)
        print ""

        dfx.unpersist()


