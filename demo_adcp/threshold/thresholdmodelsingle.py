from pyspark import SparkContext
from pyspark.sql import SQLContext

from pyspark.sql.functions import mean, stddev
from pyspark.sql.functions import when
from pyspark.mllib.regression import LabeledPoint
from pyspark.mllib.classification import LogisticRegressionWithLBFGS

sc = SparkContext("local", "threshold")
sqlContext = SQLContext(sc)

df = sqlContext.read.format("com.databricks.spark.csv").option("header","true").option("inferSchema","true").load("/tmp/file1_test.csv")

en = "ensemble_ID"
yr = "variable_RTC_year"
mt = "variable_RTC_month"
dy = "variable_RTC_day"
hr = "variable_RTC_hour"
mn = "variable_RTC_minute"
se = "variable_RTC_second"

cell = "25"
threshold = 15

#mta = 5; dya = 23; hra = 10; mna = 30
#mtb = 4; dyb = 23; hrb = 12; mnb = 0
mta = 5; dya = 23; hra = 10; mna = 30
mtb = 4; dyb = 23; hrb = 0; mnb = 0

e1 = "echo_beam1_cell_" + cell
e2 = "echo_beam2_cell_" + cell
e3 = "echo_beam3_cell_" + cell
e4 = "echo_beam4_cell_" + cell
c1 = "correlation_beam1_cell_" + cell
c2 = "correlation_beam2_cell_" + cell
c3 = "correlation_beam3_cell_" + cell
c4 = "correlation_beam4_cell_" + cell
v1 = "velocity_beam1_cell_" + cell
v2 = "velocity_beam2_cell_" + cell
v3 = "velocity_beam3_cell_" + cell
v4 = "velocity_beam4_cell_" + cell

dfx = df.select(en, mt, dy, hr, mn, se, e1, e2, e3, e4, c1, c2, c3, c4, v1, v2, v3, v4)
dfx.persist()

dfx_group = dfx.filter(df[mt] == mta).filter(df[dy] == dya).filter(df[hr] == hra).filter(df[mn] >= mna).filter(df[mn] <= mna+1)
dfy_group = dfx.filter(df[mt] == mtb).filter(df[dy] == dyb).filter(df[hr] == hrb).filter(df[mn] >= mnb).filter(df[mn] <= mnb+1)

v1_mean = dfx_group.select([mean(v1).alias("xxx")]).collect()[0].xxx
v2_mean = dfx_group.select([mean(v2).alias("xxx")]).collect()[0].xxx
v3_mean = dfx_group.select([mean(v3).alias("xxx")]).collect()[0].xxx
v4_mean = dfx_group.select([mean(v4).alias("xxx")]).collect()[0].xxx
v1_stddev = dfx_group.select([stddev(v1).alias("xxx")]).collect()[0].xxx
v2_stddev = dfx_group.select([stddev(v2).alias("xxx")]).collect()[0].xxx
v3_stddev = dfx_group.select([stddev(v3).alias("xxx")]).collect()[0].xxx
v4_stddev = dfx_group.select([stddev(v4).alias("xxx")]).collect()[0].xxx
v1_upper = v1_mean + v1_stddev * threshold/10
v2_upper = v2_mean + v2_stddev * threshold/10
v3_upper = v3_mean + v3_stddev * threshold/10
v4_upper = v4_mean + v4_stddev * threshold/10
v1_lower = v1_mean - v1_stddev * threshold/10
v2_lower = v2_mean - v2_stddev * threshold/10
v3_lower = v3_mean - v3_stddev * threshold/10
v4_lower = v4_mean - v4_stddev * threshold/10

dfx_group_filtered = dfx_group.filter(df[v1] < v1_upper).filter(df[v1] > v1_lower).filter(df[v2] < v2_upper).filter(df[v2] > v2_lower).filter(df[v3] < v3_upper).filter(df[v3] > v3_lower).filter(df[v4] < v4_upper).filter(df[v4] > v4_lower)

dfx_group.select(v1, v2, v3, v4).describe().show()
dfx_group_filtered.select(v1, v2, v3, v4).describe().show()

dfx_group_labeled = dfx_group.select(when((df[v1] < v1_upper) & (df[v1] > v1_lower) & (df[v2] < v2_upper) & (df[v2] > v2_lower) & (df[v3] < v3_upper) & (df[v3] > v3_lower) & (df[v4] < v4_upper) & (df[v4] > v4_lower), 1).otherwise(0).alias('vthresh'), e1, e2, e3, e4, c1, c2, c3, c4)

corpus = dfx_group_labeled.rdd.map(lambda y: LabeledPoint(y[0], y[1:]))
print "Threshold " + str(threshold)
inclusive = corpus.filter(lambda p: (p.label == 1)).count() / float(corpus.count())
print "Inclusvitiy (cell "+ cell + "): " + str(inclusive)

model = LogisticRegressionWithLBFGS.train(corpus)

dfy_group_filtered = dfy_group.filter(df[v1] < v1_upper).filter(df[v1] > v1_lower).filter(df[v2] < v2_upper).filter(df[v2] > v2_lower).filter(df[v3] < v3_upper).filter(df[v3] > v3_lower).filter(df[v4] < v4_upper).filter(df[v4] > v4_lower)

dfy_group.select(v1, v2, v3, v4).describe().show()
dfy_group_filtered.select(v1, v2, v3, v4).describe().show()

dfy_group_labeled = dfy_group.select(when((df[v1] < v1_upper) & (df[v1] > v1_lower) & (df[v2] < v2_upper) & (df[v2] > v2_lower) & (df[v3] < v3_upper) & (df[v3] > v3_lower) & (df[v4] < v4_upper) & (df[v4] > v4_lower), 1).otherwise(0).alias('vthresh'), e1, e2, e3, e4, c1, c2, c3, c4)

ecorpus = dfy_group_labeled.rdd.map(lambda y: LabeledPoint(y[0], y[1:]))
print "Threshold " + str(threshold)
inclusive = ecorpus.filter(lambda p: (p.label == 1)).count() / float(ecorpus.count())
print "Inclusvitiy (cell "+ cell + "): " + str(inclusive)

eval_result = ecorpus.map(lambda p: (p.label, model.predict(p.features)))

trainErr = eval_result.filter(lambda (v,p): v!= p).count() / float(eval_result.count())
print "Training Error (cell "+ cell + "): " + str(trainErr)
mx0 = eval_result.filter(lambda (v,p): p == 0).count() / float(eval_result.count())
print "Rejection Rate (cell "+ cell + "): " + str(mx0)

m00 = eval_result.filter(lambda (v,p): v == 0 and p == 0).count() / float(eval_result.count())
m01 = eval_result.filter(lambda (v,p): v == 0 and p == 1).count() / float(eval_result.count())
m10 = eval_result.filter(lambda (v,p): v == 1 and p == 0).count() / float(eval_result.count())
m11 = eval_result.filter(lambda (v,p): v == 1 and p == 1).count() / float(eval_result.count())
print "No good, not used  (cell "+ cell + "): " + str(m00)
print "No good, but used  (cell "+ cell + "): " + str(m01)
print "Good, but not used (cell "+ cell + "): " + str(m10)
print "Good, and used     (cell "+ cell + "): " + str(m11)
