from pyspark import SparkContext
from pyspark.sql import SQLContext

from pyspark.sql.functions import mean, stddev
from pyspark.sql.functions import when
from pyspark.mllib.regression import LabeledPoint
from pyspark.mllib.classification import LogisticRegressionWithLBFGS

sc = SparkContext("local", "threshold")
sqlContext = SQLContext(sc)

df = sqlContext.read.format("com.databricks.spark.csv").option("header","true").option("inferSchema","true").load("/tmp/file1_2d.csv")

en = "ensemble_ID"
dy = "variable_RTC_day"
hr = "variable_RTC_hour"
mn = "variable_RTC_minute"
se = "variable_RTC_second"

threshold = 15
cell = "10"

print "Cell " + cell
print "Threshold +/- (" + str(threshold) + "/10) stddev"

e1 = "echo_beam1_cell_" + cell
e2 = "echo_beam2_cell_" + cell
e3 = "echo_beam3_cell_" + cell
e4 = "echo_beam4_cell_" + cell
c1 = "correlation_beam1_cell_" + cell
c2 = "correlation_beam2_cell_" + cell
c3 = "correlation_beam3_cell_" + cell
c4 = "correlation_beam4_cell_" + cell
v1 = "velocity_beam1_cell_" + cell
v2 = "velocity_beam2_cell_" + cell
v3 = "velocity_beam3_cell_" + cell
v4 = "velocity_beam4_cell_" + cell

dfx = df.select(en, dy, hr, mn, se, e1, e2, e3, e4, c1, c2, c3, c4, v1, v2, v3, v4)
dfx.persist()

corpus_all = sc.emptyRDD()

for dya in [24]:
	for hra in range(15, 24):
		for mna in [0, 30]:

			dfx_group = dfx.filter(df[dy] == dya).filter(df[hr] == hra).filter(df[mn] >= mna).filter(df[mn] <= mna+1)

			v1_mean = dfx_group.select([mean(v1).alias("xxx")]).collect()[0].xxx
			v2_mean = dfx_group.select([mean(v2).alias("xxx")]).collect()[0].xxx
			v3_mean = dfx_group.select([mean(v3).alias("xxx")]).collect()[0].xxx
			v4_mean = dfx_group.select([mean(v4).alias("xxx")]).collect()[0].xxx
			v1_stddev = dfx_group.select([stddev(v1).alias("xxx")]).collect()[0].xxx
			v2_stddev = dfx_group.select([stddev(v2).alias("xxx")]).collect()[0].xxx
			v3_stddev = dfx_group.select([stddev(v3).alias("xxx")]).collect()[0].xxx
			v4_stddev = dfx_group.select([stddev(v4).alias("xxx")]).collect()[0].xxx
			v1_upper = v1_mean + v1_stddev * threshold/10
			v2_upper = v2_mean + v2_stddev * threshold/10
			v3_upper = v3_mean + v3_stddev * threshold/10
			v4_upper = v4_mean + v4_stddev * threshold/10
			v1_lower = v1_mean - v1_stddev * threshold/10
			v2_lower = v2_mean - v2_stddev * threshold/10
			v3_lower = v3_mean - v3_stddev * threshold/10
			v4_lower = v4_mean - v4_stddev * threshold/10

			dfx_group_labeled = dfx_group.select(when((df[v1] < v1_upper) & (df[v1] > v1_lower) & (df[v2] < v2_upper) & (df[v2] > v2_lower) & (df[v3] < v3_upper) & (df[v3] > v3_lower) & (df[v4] < v4_upper) & (df[v4] > v4_lower), 1).otherwise(0).alias('vthresh'), e1, e2, e3, e4, c1, c2, c3, c4)

			corpus = dfx_group_labeled.rdd.map(lambda y: LabeledPoint(y[0], y[1:]))
			inclusive = corpus.filter(lambda p: (p.label == 1)).count() / float(corpus.count())
			print "Inclusivity (Day " + str(dya) + " Hour " + str(hra) + " Minute " + str(mna) + "): " + str(inclusive)

			corpus_all = corpus_all.union(corpus)
			inclusive_all = corpus_all.filter(lambda p: (p.label == 1)).count() / float(corpus_all.count())
			print "Inclusivity so far: " + str(inclusive_all)

dfx.unpersist()

model = LogisticRegressionWithLBFGS.train(corpus_all)
print model

dyb = 23; hrb = 12; mnb = 0

dfy = df.select(en, dy, hr, mn, se, e1, e2, e3, e4, c1, c2, c3, c4, v1, v2, v3, v4)
dfy.persist()

dfy_group = dfy.filter(df[dy] == dyb).filter(df[hr] == hrb).filter(df[mn] >= mnb).filter(df[mn] <= mnb+1)

v1_mean = dfy_group.select([mean(v1).alias("xxx")]).collect()[0].xxx
v2_mean = dfy_group.select([mean(v2).alias("xxx")]).collect()[0].xxx
v3_mean = dfy_group.select([mean(v3).alias("xxx")]).collect()[0].xxx
v4_mean = dfy_group.select([mean(v4).alias("xxx")]).collect()[0].xxx
v1_stddev = dfy_group.select([stddev(v1).alias("xxx")]).collect()[0].xxx
v2_stddev = dfy_group.select([stddev(v2).alias("xxx")]).collect()[0].xxx
v3_stddev = dfy_group.select([stddev(v3).alias("xxx")]).collect()[0].xxx
v4_stddev = dfy_group.select([stddev(v4).alias("xxx")]).collect()[0].xxx
v1_upper = v1_mean + v1_stddev * threshold/10
v2_upper = v2_mean + v2_stddev * threshold/10
v3_upper = v3_mean + v3_stddev * threshold/10
v4_upper = v4_mean + v4_stddev * threshold/10
v1_lower = v1_mean - v1_stddev * threshold/10
v2_lower = v2_mean - v2_stddev * threshold/10
v3_lower = v3_mean - v3_stddev * threshold/10
v4_lower = v4_mean - v4_stddev * threshold/10

dfy_group_labeled = dfy_group.select(when((df[v1] < v1_upper) & (df[v1] > v1_lower) & (df[v2] < v2_upper) & (df[v2] > v2_lower) & (df[v3] < v3_upper) & (df[v3] > v3_lower) & (df[v4] < v4_upper) & (df[v4] > v4_lower), 1).otherwise(0).alias('vthresh'), e1, e2, e3, e4, c1, c2, c3, c4)

corpusy = dfy_group_labeled.rdd.map(lambda y: LabeledPoint(y[0], y[1:]))
eval_resulty = corpusy.map(lambda p: (p.label, model.predict(p.features)))
trainErry = eval_resulty.filter(lambda (v,p): v!= p).count() / float(eval_resulty.count())
print "Training Error (Day " + str(dyb) + " Hour " + str(hrb) + " Minute " + str(mnb) + "): " + str(trainErry)

dfy.unpersist()



