from pyspark import SparkContext
from pyspark.sql import HiveContext

sc = SparkContext("local", "threshold")
sqlContext = HiveContext(sc)

from pyspark.sql.functions import mean, min, max
from pyspark.sql.functions import when
from pyspark.sql import Window

#
# Step 1: load all data for 40000 pings
#
df = sqlContext.read.format("com.databricks.spark.csv").option("header","true").option("inferSchema","true").load("/tmp/y40000.csv")
df.count()

#
# Step 2: select relevant columns: ensemble, timing, pressure and echo intensity
#
en = "ensemble_ID"
yr = "variable_RTC_year"
mt = "variable_RTC_month"
dy = "variable_RTC_day"
hr = "variable_RTC_hour"
mn = "variable_RTC_minute"
se = "variable_RTC_second"
pr = "variable_pressure"
e1_29 = "echo_beam1_cell_29"
e1_30 = "echo_beam1_cell_30"
e1_31 = "echo_beam1_cell_31"
e1_32 = "echo_beam1_cell_32"

dfx = df.select(en, mt, dy, hr, mn, se, pr, e1_29, e1_30, e1_31, e1_32)
dfx.persist()

#
# Step 3: add columns for 1) average pressure and 2) range of pressure every 120 pings
#
ping120 = (df[mn]%30)*120 + (df[se]-1)*2 + 1 - df[en]%2
dfx1 = dfx.withColumn('ping', ping120)

win = Window.orderBy(en).rowsBetween(0,119)

avg120_pr = when(ping120 == 0, mean(df[pr]).over(win)).otherwise(0)
min120_pr = when(ping120 == 0, min(df[pr]).over(win)).otherwise(0)
max120_pr = when(ping120 == 0, max(df[pr]).over(win)).otherwise(0)

dfx2 = dfx1.withColumn('avg120_pr', avg120_pr).withColumn('rng120_pr', max120_pr - min120_pr)

dfx2.select(en, "ping", "avg120_pr", "rng120_pr").filter("ping = 0").show()

#
# Step 4: add columns for average echo intensity for 4 cells 
#
avg120_29= when(ping120 == 0, mean(df[e1_29]).over(win)).otherwise(0)
avg120_30= when(ping120 == 0, mean(df[e1_30]).over(win)).otherwise(0)
avg120_31= when(ping120 == 0, mean(df[e1_31]).over(win)).otherwise(0)
avg120_32= when(ping120 == 0, mean(df[e1_32]).over(win)).otherwise(0)

dfx3 = dfx2.withColumn('avg29', avg120_29).withColumn('avg30', avg120_30).withColumn('avg31', avg120_31).withColumn('avg32', avg120_32)

#
# Step 5: add columnns for 1) maximum echo intensity and 2) cell number with maximum echo intensity
#
max29_30= when(avg120_30 > avg120_29, avg120_30).otherwise(avg120_29)
max29_31= when(max29_30 > avg120_31, max29_30).otherwise(avg120_31)
max29_32= when(max29_31 > avg120_32, max29_31).otherwise(avg120_32)

match_29 = when(max29_32 == avg120_29, 29).otherwise(0)
match_30 = when(max29_32 == avg120_30, 30).otherwise(0)
match_31 = when(max29_32 == avg120_31, 31).otherwise(0)
match_32 = when(max29_32 == avg120_32, 32).otherwise(0)
idx29_32 = match_29 + match_30 + match_31 + match_32

dfx4 = dfx3.withColumn('max29_32', max29_32).withColumn('idx29_32', idx29_32)

dfx4.select(en, "ping", "avg29", "avg30", "avg31", "avg32", "max29_32", "idx29_32").filter("ping = 0").show()

#
# Step 6: check correlation between pressure and cell number with maximum echo intensity
#
pr_id_corr = dfx4.filter("ping = 0").stat.corr("avg120_pr", "idx29_32")
print "Pressure to cell correlation: "+str(pr_id_corr)


