from pyspark import SparkContext
from pyspark.sql import HiveContext

from pyspark.sql.functions import mean, stddev, min, max
from pyspark.sql.functions import when
from pyspark.sql import Window

from numpy import array
from math import sqrt
from pyspark.mllib.clustering import KMeans, KMeansModel

sc = SparkContext("local", "threshold")
sqlContext = HiveContext(sc)

df = sqlContext.read.format("com.databricks.spark.csv").option("header","true").option("inferSchema","true").load("/tmp/y40000.csv")

en = "ensemble_ID"
yr = "variable_RTC_year"
mt = "variable_RTC_month"
dy = "variable_RTC_day"
hr = "variable_RTC_hour"
mn = "variable_RTC_minute"
se = "variable_RTC_second"

cell_num = "42"

e1_10 = "echo_beam1_cell_"+cell_num
e2_10 = "echo_beam2_cell_"+cell_num
e3_10 = "echo_beam3_cell_"+cell_num
e4_10 = "echo_beam4_cell_"+cell_num

dfx = df.select(en, mt, dy, hr, mn, se, e1_10, e2_10, e3_10, e4_10)
dfx.persist()

ping120 = (df[mn]%30)*120 + (df[se]-1)*2 + 1 - df[en]%2
dfx1 = dfx.withColumn('ping', ping120)


win = Window.orderBy(en).rowsBetween(0,119)

std1_10 = when(ping120 == 0, stddev(df[e1_10]).over(win)).otherwise(0)
std2_10 = when(ping120 == 0, stddev(df[e2_10]).over(win)).otherwise(0)
std3_10 = when(ping120 == 0, stddev(df[e3_10]).over(win)).otherwise(0)
std4_10 = when(ping120 == 0, stddev(df[e4_10]).over(win)).otherwise(0)

dfx2 = dfx1.withColumn('std1_10', std1_10).withColumn('std2_10', std2_10).withColumn('std3_10', std3_10).withColumn('std4_10', std4_10)

#dfx2.select("std1_10", "std2_10", "std3_10", "std4_10").filter("ping = 0").show(500)

pdx2 = dfx2.select("std1_10", "std2_10", "std3_10", "std4_10").filter("ping = 0").rdd.map(array)

clusters = KMeans.train(pdx2, 2, maxIterations=10,initializationMode="random")
print clusters.centers[0]
print clusters.centers[1]

classif = pdx2.map(lambda point: clusters.predict(point))
print classif.take(334)

x0 = classif.map(lambda x: 1 if x == 0 else 0).reduce(lambda x, y: x + y)
x1 = classif.map(lambda x: 1 if x == 1 else 0).reduce(lambda x, y: x + y)

print str(x0)+" vs "+str(x1)
