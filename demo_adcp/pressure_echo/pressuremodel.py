from pyspark import SparkContext
from pyspark.sql import HiveContext

from pyspark.sql.functions import mean, min, max
from pyspark.sql.functions import when
from pyspark.sql import Window


sc = SparkContext("local", "threshold")
sqlContext = HiveContext(sc)

df = sqlContext.read.format("com.databricks.spark.csv").option("header","true").option("inferSchema","true").load("/tmp/y40000.csv")

en = "ensemble_ID"
yr = "variable_RTC_year"
mt = "variable_RTC_month"
dy = "variable_RTC_day"
hr = "variable_RTC_hour"
mn = "variable_RTC_minute"
se = "variable_RTC_second"

pr = "variable_pressure"

dfx = df.select(en, mt, dy, hr, mn, se, pr)
dfx.persist()

ping120 = (df[mn]%30)*120 + (df[se]-1)*2 + 1 - df[en]%2
dfx1 = dfx.withColumn('ping', ping120)


win = Window.orderBy(en).rowsBetween(0,119)
#avg120 = mean(df[pr]).over(win)
#min120 = min(df[pr]).over(win)
#max120 = max(df[pr]).over(win)

#dfx2 = dfx1.withColumn('avg120', avg120)
#dfx2 = dfx1.withColumn('avg120', avg120).withColumn('rng120', max120 - min120)
#dfx2.filter("ping = 0").show()

avg120_p0 = when(ping120 == 0, mean(df[pr]).over(win)).otherwise(0)
min120_p0 = when(ping120 == 0, min(df[pr]).over(win)).otherwise(0)
max120_p0 = when(ping120 == 0, max(df[pr]).over(win)).otherwise(0)
dfx3 = dfx1.withColumn('avg120_p0', avg120_p0).withColumn('rng120_p0', max120_p0 - min120_p0)
dfx3.filter("ping = 0").show(500)

#dfx1.select(en, ping120.alias('ping'), when(ping120 == 0, mean(df[pr]).over(win)).otherwise(0).alias('avg120_p0')).filter("ping = 0").show()
