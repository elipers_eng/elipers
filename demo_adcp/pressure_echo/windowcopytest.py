from pyspark import SparkContext
from pyspark.sql import HiveContext

from pyspark.sql.functions import avg, min, max, mean, stddev, count, first, last, lead, lag
from pyspark.sql.functions import when
from pyspark.sql import Window


sc = SparkContext("local", "threshold")
sqlContext = HiveContext(sc)

df = sqlContext.read.format("com.databricks.spark.csv").option("header","true").option("inferSchema","true").load("/tmp/y40000.csv")

en = "ensemble_ID"
yr = "variable_RTC_year"
mt = "variable_RTC_month"
dy = "variable_RTC_day"
hr = "variable_RTC_hour"
mn = "variable_RTC_minute"
se = "variable_RTC_second"

pr = "variable_pressure"

dfx = df.select(en, mt, dy, hr, mn, se, pr)
dfx.persist()

ping120 = (df[mn]%30)*120 + (df[se]-1)*2 + 1 - df[en]%2
dfx1 = dfx.withColumn('ping', ping120)

#
# Window function
#
win = Window.orderBy(en).rowsBetween(0,119)

cnt120_pr = when(ping120 == 0, count(df[pr]).over(win)).otherwise(0)
avg120_pr = when(ping120 == 0, avg(df[pr]).over(win)).otherwise(0)
std120_pr = when(ping120 == 0, stddev(df[pr]).over(win)).otherwise(0)
min120_pr = when(ping120 == 0, min(df[pr]).over(win)).otherwise(0)
max120_pr = when(ping120 == 0, max(df[pr]).over(win)).otherwise(0)

dfx2 = dfx1.withColumn('cnt120_pr', cnt120_pr).withColumn('avg120_pr', avg120_pr).withColumn('std120_pr', std120_pr).withColumn('min120_pr', min120_pr).withColumn('max120_pr', max120_pr)
dfx2.select(en, "ping", "cnt120_pr", "avg120_pr", "std120_pr", "min120_pr", "max120_pr").filter("ping = 0").show()

#
# Copy
#
dfx3 = dfx1.withColumn('avg120_pr', avg120_pr)

win1 = Window.orderBy(en).rowsBetween(-1,0) 
copy1 = when(ping120 == 1, first("avg120_pr").over(win1)).otherwise(avg120_pr)

dfx3.withColumn('copy1', copy1).select(en, 'ping', pr, 'avg120_pr', 'copy1').show()

win2 = Window.orderBy(en).rowsBetween(-2,0) 
copy2 = when(ping120 == 2, first("avg120_pr").over(win2)).otherwise(copy1)

dfx3.withColumn('copy2', copy2).select(en, 'ping', pr, 'avg120_pr', 'copy2').show()

