from pyspark import SparkContext
from pyspark.sql import HiveContext

from pyspark.sql.functions import mean
from pyspark.sql.functions import when
from pyspark.sql import Window


sc = SparkContext("local", "threshold")
sqlContext = HiveContext(sc)

df = sqlContext.read.format("com.databricks.spark.csv").option("header","true").option("inferSchema","true").load("/tmp/y40000.csv")

en = "ensemble_ID"
yr = "variable_RTC_year"
mt = "variable_RTC_month"
dy = "variable_RTC_day"
hr = "variable_RTC_hour"
mn = "variable_RTC_minute"
se = "variable_RTC_second"

e1_29 = "echo_beam1_cell_29"
e1_30 = "echo_beam1_cell_30"
e1_31 = "echo_beam1_cell_31"
e1_32 = "echo_beam1_cell_32"
e2_29 = "echo_beam2_cell_29"
e2_30 = "echo_beam2_cell_30"
e2_31 = "echo_beam2_cell_31"
e2_32 = "echo_beam2_cell_32"
e3_29 = "echo_beam3_cell_29"
e3_30 = "echo_beam3_cell_30"
e3_31 = "echo_beam3_cell_31"
e3_32 = "echo_beam3_cell_32"
e4_29 = "echo_beam4_cell_29"
e4_30 = "echo_beam4_cell_30"
e4_31 = "echo_beam4_cell_31"
e4_32 = "echo_beam4_cell_32"

dfx = df.select(en, mt, dy, hr, mn, se, e1_29, e1_30, e1_31, e1_32, e2_29, e2_30, e2_31, e2_32, e3_29, e3_30, e3_31, e3_32, e4_29, e4_30, e4_31, e4_32)
dfx.persist()

ping120 = (df[mn]%30)*120 + (df[se]-1)*2 + 1 - df[en]%2
dfx1 = dfx.withColumn('ping', ping120)


win = Window.orderBy(en).rowsBetween(0,119)

avge1_29= when(ping120 == 0, mean(df[e1_29]).over(win)).otherwise(0)
avge1_30= when(ping120 == 0, mean(df[e1_30]).over(win)).otherwise(0)
avge1_31= when(ping120 == 0, mean(df[e1_31]).over(win)).otherwise(0)
avge1_32= when(ping120 == 0, mean(df[e1_32]).over(win)).otherwise(0)

maxe1_29_30= when(avge1_30 > avge1_29, avge1_30).otherwise(avge1_29)
maxe1_29_31= when(maxe1_29_30 > avge1_31, maxe1_29_30).otherwise(avge1_31)
maxe1_29_32= when(maxe1_29_31 > avge1_32, maxe1_29_31).otherwise(avge1_32)

matche1_29 = when(maxe1_29_32 == avge1_29, 29).otherwise(0)
matche1_30 = when(maxe1_29_32 == avge1_30, 30).otherwise(0)
matche1_31 = when(maxe1_29_32 == avge1_31, 31).otherwise(0)
matche1_32 = when(maxe1_29_32 == avge1_32, 32).otherwise(0)
idxe1_29_32 = matche1_29 + matche1_30 + matche1_31 + matche1_32

avge2_29= when(ping120 == 0, mean(df[e2_29]).over(win)).otherwise(0)
avge2_30= when(ping120 == 0, mean(df[e2_30]).over(win)).otherwise(0)
avge2_31= when(ping120 == 0, mean(df[e2_31]).over(win)).otherwise(0)
avge2_32= when(ping120 == 0, mean(df[e2_32]).over(win)).otherwise(0)

maxe2_29_30= when(avge2_30 > avge2_29, avge2_30).otherwise(avge2_29)
maxe2_29_31= when(maxe2_29_30 > avge2_31, maxe2_29_30).otherwise(avge2_31)
maxe2_29_32= when(maxe2_29_31 > avge2_32, maxe2_29_31).otherwise(avge2_32)

matche2_29 = when(maxe2_29_32 == avge2_29, 29).otherwise(0)
matche2_30 = when(maxe2_29_32 == avge2_30, 30).otherwise(0)
matche2_31 = when(maxe2_29_32 == avge2_31, 31).otherwise(0)
matche2_32 = when(maxe2_29_32 == avge2_32, 32).otherwise(0)
idxe2_29_32 = matche2_29 + matche2_30 + matche2_31 + matche2_32

avge3_29= when(ping120 == 0, mean(df[e3_29]).over(win)).otherwise(0)
avge3_30= when(ping120 == 0, mean(df[e3_30]).over(win)).otherwise(0)
avge3_31= when(ping120 == 0, mean(df[e3_31]).over(win)).otherwise(0)
avge3_32= when(ping120 == 0, mean(df[e3_32]).over(win)).otherwise(0)

maxe3_29_30= when(avge3_30 > avge3_29, avge3_30).otherwise(avge3_29)
maxe3_29_31= when(maxe3_29_30 > avge3_31, maxe3_29_30).otherwise(avge3_31)
maxe3_29_32= when(maxe3_29_31 > avge3_32, maxe3_29_31).otherwise(avge3_32)

matche3_29 = when(maxe3_29_32 == avge3_29, 29).otherwise(0)
matche3_30 = when(maxe3_29_32 == avge3_30, 30).otherwise(0)
matche3_31 = when(maxe3_29_32 == avge3_31, 31).otherwise(0)
matche3_32 = when(maxe3_29_32 == avge3_32, 32).otherwise(0)
idxe3_29_32 = matche3_29 + matche3_30 + matche3_31 + matche3_32

avge4_29= when(ping120 == 0, mean(df[e4_29]).over(win)).otherwise(0)
avge4_30= when(ping120 == 0, mean(df[e4_30]).over(win)).otherwise(0)
avge4_31= when(ping120 == 0, mean(df[e4_31]).over(win)).otherwise(0)
avge4_32= when(ping120 == 0, mean(df[e4_32]).over(win)).otherwise(0)

maxe4_29_30= when(avge4_30 > avge4_29, avge4_30).otherwise(avge4_29)
maxe4_29_31= when(maxe4_29_30 > avge4_31, maxe4_29_30).otherwise(avge4_31)
maxe4_29_32= when(maxe4_29_31 > avge4_32, maxe4_29_31).otherwise(avge4_32)

matche4_29 = when(maxe4_29_32 == avge4_29, 29).otherwise(0)
matche4_30 = when(maxe4_29_32 == avge4_30, 30).otherwise(0)
matche4_31 = when(maxe4_29_32 == avge4_31, 31).otherwise(0)
matche4_32 = when(maxe4_29_32 == avge4_32, 32).otherwise(0)
idxe4_29_32 = matche4_29 + matche4_30 + matche4_31 + matche4_32

dfx3 = dfx1.withColumn('idxe1_29_32', idxe1_29_32).withColumn('idxe2_29_32', idxe2_29_32).withColumn('idxe3_29_32', idxe3_29_32).withColumn('idxe4_29_32', idxe4_29_32)
dfx5 = dfx3.select(en, "ping", "idxe1_29_32", "idxe2_29_32", "idxe3_29_32", "idxe4_29_32")

dfx5.filter("ping = 0").show(500)

