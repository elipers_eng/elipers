import sys

if len(sys.argv) != 2:
   print("Usage: modelgenu <filename>")
   exit(-1)

from pyspark import SparkContext

sc = SparkContext("local", "modelgenu")

data = sc.textFile("/tmp/uall_cooked3")
from pyspark.mllib.regression import LabeledPoint
from pyspark.mllib.tree import DecisionTree
def parsePoint(line): values = [float(x) for x in line.split(' ')]; return LabeledPoint(values[0], values[1:])

parsedData = data.map(parsePoint)
model = DecisionTree.trainClassifier(parsedData, numClasses=150, categoricalFeaturesInfo={}, impurity='gini', maxDepth=10)

with open(sys.argv[1]) as f:
  for line in f:
    l = line.split() 
    x = model.predict(l)
    print("Prediction u velocity is %i" % x)
