import tornado.ioloop
import tornado.web
import tornado.websocket
import tornado.template

import numpy as np

in_file = open("uv_header.txt")
field = []  # lookup from index to fieldname
for line in in_file:
    field = line.rstrip().split()
print field

x = 0
fieldi = {}  # lookup from fieldname to index
for f in field:
    fieldi[f] = x
    x += 1

table = {}
for f in field:
    table[f] = []

in_file = open("uv.txt")
for line in in_file:
    data = line.rstrip().split()
    for i in range(len(field)):
        table[field[i]].append(data[i])

janu = [int(x) for x in table['janu']]
janv = [int(x) for x in table['janv']]
junu = [int(x) for x in table['junu']]
junv = [int(x) for x in table['junv']]

class MainHandler(tornado.web.RequestHandler):
  def get(self):
    loader = tornado.template.Loader(".")
    self.write(loader.load("index.html").generate())

class WSHandler(tornado.websocket.WebSocketHandler):
  def check_origin(self, origin):
    return True

  def open(self):
    print 'connection opened...'

  def on_message(self, message):
    print 'received:', message
    m = message.split(' ');
    if (m[0] == 'UVEL') :
      field = m[1] + "u"
      field_raw = [int(x) for x in table[field]] 
      string = ' '.join(str(e) for e in field_raw)
    elif (m[0] == 'VVEL'):
      field = m[1] + "v"
      field_raw = [int(x) for x in table[field]] 
      string = ' '.join(str(e) for e in field_raw)
    elif (m[0] == 'STAT'):
      string = "1 2 3 4"
    elif (m[0] == 'CORR'):
      string = "5"
    else:
      string = ""
    self.write_message(string)

  def on_close(self):
    print 'connection closed...'

application = tornado.web.Application([
  (r'/', WSHandler),
])

if __name__ == "__main__":
  print 'Ready...'
  application.listen(8080)
  tornado.ioloop.IOLoop.instance().start()
 
