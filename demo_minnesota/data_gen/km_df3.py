from pyspark import SparkContext
from pyspark.sql.session import SparkSession

sc = SparkContext()
spark = SparkSession.builder.master("local").appName("simple").enableHiveSupport().getOrCreate()

from numpy import array
from pyspark.mllib.clustering import KMeans, KMeansModel

df1 = spark.read.option("header","true").option("inferSchema","true").csv("tsbridgelog_wh.csv")
data = df1.select('heart', 'resp', 'brta', 'pos', 'vmu').rdd
parsedData = data.map(array)

def center_to_state(x):
    if x[3] < -60:
        return "lying"
    if x[1] > 20:
        return "walking"
    if x[4] > 0.1:
        return "running"
    if x[3] < -20:
        return "sitting"
    return "standing"
    
def center_to_state(x):
    if x[3] < -60:
        return "R"
    if x[1] > 20:
        return "P"
    if x[4] > 0.1:
        return "T"
    if x[3] < -20:
        return "Q"
    return "S"

num_clus = 10
num_to_state = [''] * num_clus

clusters = KMeans.train(parsedData, num_clus, maxIterations=10, initializationMode="random")
cnt = parsedData.count()
pd = parsedData.take(cnt)
pd_result = [clusters.predict(x) for x in pd]

num_to_state = [center_to_state(x) for x in clusters.centers]
print num_to_state

for i in xrange(0, num_clus):
    print list(clusters.centers[i])

for i in xrange(0, num_clus):
    print pd_result.count(i)

print [num_to_state[x] for x in pd_result]
print len(pd_result)

