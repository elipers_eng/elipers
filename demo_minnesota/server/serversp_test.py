import tornado.ioloop
import tornado.web
import tornado.websocket
import tornado.template

import csv
import time
import threading
import numpy

from operator import itemgetter
from itertools import groupby
from numpy import array

# SPARK
#from pyspark import SparkContext
#from pyspark.sql.session import SparkSession
#from pyspark.mllib.clustering import KMeans, KMeansModel
#sc = SparkContext()
#spark = SparkSession.builder.master("local").appName("simple").enableHiveSupport().getOrCreate()
# SPARK

def center_to_state(x):
    if x[3] < -60:
        return "R"
    if x[1] > 20:
        return "P"
    if x[4] > 0.1:
        return "T"
    if x[3] < -20:
        return "Q"
    return "S"

# SPARK
#df1 = spark.read.option("header","true").option("inferSchema","true").csv("tsbridgelogx2_wh.csv")
#data = df1.select('heart', 'resp', 'brta', 'pos', 'vmu').rdd
#parsedData = data.map(array)
#num_clus = 5
#num_to_state = [''] * num_clus
#clusters = KMeans.train(parsedData, num_clus, maxIterations=10, initializationMode="random")
#for i in xrange(0, num_clus):
#    print clusters.centers[i]
#cnt = parsedData.count()
#pd = parsedData.take(cnt)
#pd_result = [clusters.predict(x) for x in pd]
#num_to_state = [center_to_state(x) for x in clusters.centers]
#print num_to_state
#slist = [num_to_state[x] for x in pd_result]
# SPARK

# PYTHON
with open("state.csv") as f:
    rows = csv.reader(f)
    slist = [r for r in rows][0]
# PYTHON

with open("tsbridgelogx2_wh.csv") as f:
    flist = [r for r in csv.reader(f)]

header_z = flist[0]
flist.pop(0)

model = None
lookback = 10
min_corr = 0.95
max_dev = 0.01

def calc_state(x, y):
    print x, y
    if model == None :
        return 0
    return int(model.predict([float(x), float(y)]))

def calc_ecggb2(o1):
    if o1 == 'null':
        return 'BAD:0:1'
    o15 = o1.split(":")
    o2 = [int(x) for x in o15]
    if len(mglist) > lookback:
        lsub = mglist[-lookback:]
        lsub1 = [p for p in lsub if p != 'null']
        if len(lsub1) > 0:
            lsub2 = [[int(y) for y in x.split(":")] for x in lsub1]
            corrs = [numpy.corrcoef(o2, x)[0][1] for x in lsub2]
            means = [numpy.mean(x) for x in lsub2]
            match = numpy.mean(corrs)
            dev   = abs(numpy.mean(o2) - numpy.mean(means)) / abs(numpy.mean(means))
        else:
            match = 0
            dev = 1
    else:
        match = 1
        dev = 0
    if match > min_corr and dev < max_dev:
        state = "GOOD"
    else:
        state = "BAD"
    return state + ':' + str(match) + ':' + str(dev)

def get_ecggb2(line):
    return eglist[line]

zp_vmu_offset = 4
zp_pacc_offset = 5
zp_ecgv_offset = 21

header = ['tstime']
accum = 1
fields = [x for x in header_z[1:]]
header += fields
racer_fldlen = len(fields)
racer_fldidx = accum
accum += len(fields)

fields = []
fields += [racer_fldidx-1, racer_fldlen]

calcs = []
accum = racer_fldidx -1 + racer_fldlen
calcs += [accum, 4]
accum += 4

descriptor = ':'.join([str(i) for i in fields+calcs])

mglist = []
eglist = []
ecgfix = racer_fldidx + zp_ecgv_offset

print "HAHA", racer_fldidx, racer_fldlen, calcs
        
mflist = []
rsret = []
rsidx = 0
rsskp = 0
line_num = 0
stream_th = None
stream_ev = None

lasttime = flist[-1][0]
lasttime_t = time.strptime(lasttime, "%Y-%m-%d %H:%M:%S")

mflist = flist

def ispeak(x):
    return x > 530

def period_stat(period_l):
    period_a = numpy.array(period_l)
    pmin = numpy.min(period_a)
    pmax = numpy.max(period_a)
    pmean = numpy.mean(period_a)
    pstd  = numpy.std(period_a)
    pmedian = numpy.median(period_a)
    print len(period_l), "PERIODS"
    print "(min, median, max)", pmin, pmedian, pmax
    print "(mean, std)", pmean, pstd
    return pmedian

def period_clean_stat(period_clean_l):
    period_clean_a = numpy.array(period_clean_l)
    pcmin = numpy.min(period_clean_a)
    pcmax = numpy.max(period_clean_a)
    pcmean = numpy.mean(period_clean_a)
    pcstd = numpy.std(period_clean_a)
    pcmedian = numpy.median(period_clean_a)
    print len(period_clean_l), "CLEAN PERIODS"
    print "(min, median, max)", pcmin, pcmedian, pcmax
    print "(mean, std)", pcmean, pcstd
    return pcmean

def find_peaks3(v3):
    peaks = [p for p, x in enumerate(v3) if ispeak(x)]
    peaks_split = [map(itemgetter(1), g) for k, g in groupby(enumerate(peaks), lambda (i,x):i-x)]
    peaks_idx = [x[len(x)/2] for x in peaks_split]
    return peaks_idx

def find_periods3(peaks_idx):
    diff_forward = [peaks_idx[i+1]-peaks_idx[i] for i in xrange(0,len(peaks_idx)-1)] + [999]
    diff_backward = [999] + [peaks_idx[i]-peaks_idx[i-1] for i in xrange(1,len(peaks_idx))]
    fbp3 = [x[0]/2 + x[1]/2 + 1 for x in zip(diff_forward, diff_backward)]
    return fbp3

def get_pqrst(lines_tup):
    samples = len(lines_tup[0][1])
    lines = [x[1] for x in lines_tup]
    tl = [sum(y,[]) for y in zip(lines[0:-2], lines[1:-1], lines[2:])]
    print len(tl), "SECONDS"
    peakp3 = [find_peaks3(x) for x in tl]
    periodp3 = [find_periods3(x) for x in peakp3]
    pfb3 = [zip(x[0], x[1]) for x in zip(peakp3, periodp3)]
    pfbq3 = [[x[1] for x in y if x[0] > samples and x[0] <= samples*2] for y in pfb3]
    period_l = sum(pfbq3,[])
    pmedian = period_stat(period_l)
    filter_pct = 10
    filter_lo = (100-filter_pct)*pmedian/100
    filter_hi = (100+filter_pct)*pmedian/100
    period_clean_l = [x for x in period_l if x > filter_lo and x < filter_hi]
    pcmean = period_clean_stat(period_clean_l)
    pp = [[(x-int(pcmean)/2,x+int(pcmean)/2+1) for x in y if x > samples and x <= samples*2] for y in peakp3]
    qq = zip(pp, tl)
    rr = [[y[1][x[0]:x[1]] for x in y[0]] for y in qq]
    pqrst_l = sum(rr,[])
    pp1 = zip(period_l, pqrst_l)
    pqrst_clean_l = [x[1] for x in pp1 if x[0] > filter_lo and x[0] < filter_hi]
    timel = [x[0] for x in lines_tup][1:-1]
    tpfbq3 = zip(timel, pfbq3)
    tp = [[x[0] for y in x[1]] for x in tpfbq3]
    tp_l = sum(tp,[])
    pp2 = zip(period_l, tp_l)
    tp_clean_l = [x[1] for x in pp2 if x[0] > filter_lo and x[0] < filter_hi]
    return (pqrst_clean_l, tp_clean_l)

act_n2n = ["walking","sitting","lying","standing","running"]
act_dict = {'P':0, 'Q':1, 'R':2, 'S':3, 'T':4}

sgroup = groupby(slist)
scount = [(lbl, sum(1 for _ in gp)) for lbl, gp in sgroup]
sfcount = [x for x in scount if x[1] >= 60]
sfindex = [scount.index(x) for x in sfcount]
sftoi = [scount[0:x] for x in sfindex]
act_start = [sum([x[1] for x in y]) for y in sftoi]
act_len = [x[1] for x in sfcount]
#act_num = [int(x[0]) for x in sfcount]
act_num = [act_dict[x[0]] for x in sfcount]
act_name = [act_n2n[x] for x in act_num]
act_pqrst = [[]] * len(sfcount)
act_time = [[]] * len(sfcount)
act_idx  = [0] * len(sfcount)

print scount
print sfcount

#for j in xrange(0, 5):
#    i = act_num.index(j)
#    lstart = act_start[i]
#    llen = act_len[i]
#    print act_n2n[j], act_name[i], lstart, llen
#    l1 = flist[lstart:lstart+llen]
#    l2tup = [(x[header.index('tstime')], x[header.index('ecgv')]) for x in l1]
#    l3tup = [x for x in l2tup if x[1] != 'null']
#    lines_tup = [(x[0], [int(y) for y in x[1].split(':')]) for x in l3tup]
#    l4 = get_pqrst(lines_tup)
#    l5 = l4[0]
#    act_pqrst[i] = l5
#    act_time[i] = l4[1]

acorr_val = [[]] * len(sfcount)
acorr_rank = [[]] * len(sfcount)

def corr_pqrst(pl):
    pl_corr =  numpy.corrcoef(pl)
#    pl_tp = map(list, zip(*pl))
#    pl_rdd = sc.parallelize(pl_tp)
#    pl_corr = Statistics.corr(pl_rdd)
    corr_l = [(sum(x)-1)/(len(x)-1) for x in pl_corr]
    corr_a = numpy.array(corr_l)
    corr_rank_a = numpy.argsort(corr_a)
    corr_rank_l = list(corr_rank_a)
    print "WORSE CORRELATION"
    for i in xrange(0, 5):
        print corr_rank_l[i], corr_l[corr_rank_l[i]]
    print "BEST CORRELATION"
    for i in xrange(-1, -6, -1):
        print corr_rank_l[i], corr_l[corr_rank_l[i]]
    corr_rank_a2 = numpy.argsort(corr_rank_a)
    corr_rank_l2 = list(corr_rank_a2)
    return (corr_l, corr_rank_l2)

#for j in xrange(0, 5):
#    i = act_num.index(j)
#    pl = act_pqrst[i]
#    l1 = corr_pqrst(pl)
#    acorr_val[i] = l1[0]
#    acorr_rank[i] = l1[1]

def threaded_function(arg):
    global mflist
#    for i in xrange(0, arg):
    while True:
        with open("tsbridgelogx2_wh.csv") as f:
            flist = [r for r in csv.reader(f)]
        lasttime = flist[-1][0]
        lasttime_t = time.strptime(lasttime, "%Y-%m-%d %H:%M:%S")
        endtime = mflist[-1][0]
        maxlist = flist
        maxtlist = [q[0] for q in maxlist]
        if endtime in maxtlist:
            maxidx = maxtlist.index(endtime) + 1
#            print endtime, maxidx, len(maxtlist)
            if maxidx != len(maxtlist) :
                addflist = maxlist[maxidx:]
                for i in xrange(0, len(addflist)):
                    ecgi = ecgfix
                    if ecgi != 0:
                        v0 = addflist[i][ecgi]
#                        pqrst = get_pqrst(0, v0)
#                        addflist[i][ecgi] = pqrst
#                        ecggb2 = calc_ecggb2(pqrst)
#                        eglist.append(ecggb2)
#                        mglist.append(pqrst)
                mflist += addflist
            print "FILL:", len(maxtlist) - maxidx, '[', line_num, ':', len(mflist), ']', endtime, maxtlist[0]
        else:
            print "gap", endtime, maxtlist[0]
            if True:
                addflist = maxlist
                for i in xrange(0, len(addflist)):
                    ecgi = ecgfix
                    if ecgi != 0:
                        v0 = addflist[i][ecgi]
#                        pqrst = get_pqrst(0, v0)
#                        addflist[i][ecgi] = pqrst
#                        ecggb2 = calc_ecggb2(pqrst)
#                        eglist.append(ecggb2)
#                        mglist.append(pqrst)
                mflist += addflist
            print "FILL_SKIP:", len(maxtlist), '[', line_num, ':', len(mflist), ']', endtime, maxtlist[0]
        time.sleep(5)

def line_field(i, f):
    if f[0] == 'ALL':
        return mflist[i]
    chk = [x in header for x in f]
    if all(x == True for x in chk) == False:
        return []
    idx = [header.index(x) for x in f]
    ln = mflist[i]
    val = [ln[x] for x in idx]
    return val

def maxcnt_vector(vec, skp):
    n = len(vec)
    splitvec = [vec[i:i+skp] for i in xrange(0, n, skp)]
    mcvec = [max(set(x), key=x.count) for x in splitvec]
    return mcvec

def sample_vector(vec, skp, nrm):
    if nrm == 0:
        return vec[::skp]
    else:
        skpvec = [float(x) for x in vec[::skp]]
        b = min(skpvec) 
        d = max(skpvec) - b
        nrmvec =  [(x - b)/d for x in skpvec]
        return [str(x) for x in nrmvec]

def average_vector_old(vec, skp):
    n = len(vec)
    floatvec = [float(x) for x in vec]
    splitvec = [floatvec[i:i+skp] for i in xrange(0, n, skp)]
    avgvec =  [sum(x)/len(x) for x in splitvec]
    return [str(x) for x in avgvec]

def average_vector(vec, skp, nrm):
    n = len(vec)
    floatvec = [float(x) for x in vec]
    splitvec = [floatvec[i:i+skp] for i in xrange(0, n, skp)]
    avgvec =  [sum(x)/len(x) for x in splitvec]
    if nrm == 0:
        return [str(x) for x in avgvec]
    else:
        b = min(avgvec) 
        d = max(avgvec) - b
        nrmvec =  [(x - b)/d for x in avgvec]
        return [str(x) for x in nrmvec]


class MainHandler(tornado.web.RequestHandler):
    def get(self):
        loader = tornado.template.Loader(".")
        self.write(loader.load("index.html").generate())
        
class WSHandler(tornado.websocket.WebSocketHandler):
    def tf(self, ev, msg):
        global act_idx
        while True:
            if ev.isSet():
                return
            j = int(msg[0])
            i = act_num.index(j)
            pl = act_pqrst[i]
            tl = act_time[i]
            pmx = len(pl)
            pn = act_idx[i]
            if pn < pmx:
                print "EMPTY:", 1, '[', pn, ':', pmx, ']'
                vl = acorr_val[i]
                rl = acorr_rank[i]
#                tt = time.strftime("%Y-%m-%d-%H-%M-%S")
                tt = time.strftime("%Y-%m-%d-%H-%M-%S", time.strptime(tl[pn], "%Y-%m-%d %H:%M:%S"))
                desc = str(vl[pn]) + ':' + str(rl[pn]) + ':' + tt
                stro = 'DATA ' + desc + ' ' + ' '.join([str(x) for x in pl[pn]])
                act_idx[i] = pn+1
                self.write_message(stro)
            else:
                print "EMPTY:", 0, '[', pn, ':', pmx, ']'
                return
            time.sleep(0.1)

    def ts(self, ev, msg):
        global rsidx
        global rsskp
        while True:
            if ev.isSet():
                return
            rsmax = len(rsret)
            if rsidx == rsmax:
                return

            val = rsret[rsidx]
            stro = 'STATE ' + str(rsidx) + ' ' + val
            print "EMPTY:", 1, '[', rsidx, ':', rsmax, ']', val
            self.write_message(stro)
            rsidx += 1
            time.sleep(0.2)

    def check_origin(self, origin):
        return True

    def open(self):
        print 'connection opened...'

    def on_message(self, message):
        global line_num
        global stream_th
        global stream_ev
        global rsret
        global rsidx
        global rsskp
        global act_idx
        line_max = len(mflist)
        print 'received:', message

        msg = message.split(' ')
        if len(msg) > 0:
            cmd = msg[0]
            msg.pop(0)
            if cmd == 'SET' :
                slop = 5
                if line_max - line_num < slop:
                    string = 'NACK'
                else:
                    line_num = line_max-slop
                    string = 'ACK'
            elif cmd == 'VECTOR' :
                if len(msg) > 0 :
                    fld = msg[0]
                    mlen = len(mflist)
                    cnt = 50
                    if len(msg) > 2 :
                        rangex = int(msg[1])/2
                        rangey = int(msg[2])/2
                    else :
                        rangex = 0
                        rangey = cnt
                    if len(msg) > 3 :
                        nrm = int(msg[3])
                    else :
                        nrm = 0
                    rangexx = rangex * mlen / cnt
                    rangeyy = rangey * mlen / cnt
                    rflist = mflist[rangexx:rangeyy]
                    skp = len(rflist)/cnt
                    if skp > 0:
                        if fld in header:
                            idx = header.index(fld)
                            allval = [x[idx] for x in rflist]
                            if idx == 0:
                                allval = [time.strftime("%Y-%m-%d-%H-%M-%S", time.strptime(x, "%Y-%m-%d %H:%M:%S")) for x in allval]
                            if idx < 3:
                                retval = sample_vector(allval,skp,nrm)
                            else:
                                retval = average_vector(allval,skp,nrm)
                            string = 'VECTOR' + ' ' + ' '.join(retval)
                        else:
                            string = 'NACK'
                    else:
                        string = 'NACK'
                else:
                    string = 'NACK'
            elif cmd == 'STATE' :
                if len(msg) > 0 :
                    mlen = len(slist)
                    cnt = 50
                    if len(msg) > 1 :
                        rangex = int(msg[0])/2
                        rangey = int(msg[1])/2
                    else :
                        rangex = 0
                        rangey = cnt
                    rangexx = rangex * mlen / cnt
                    rangeyy = rangey * mlen / cnt
                    rslist = slist[rangexx:rangeyy]
                    skp = len(rslist)/cnt
                    if skp > 0:
                        rsret = maxcnt_vector(rslist,skp)
                        rsidx = 0
                        rsskp = skp
                        print rsret
                        stream_ev = threading.Event()
                        stream_th = threading.Thread(target=self.ts, args=(stream_ev,msg,))
                        stream_th.start()
                        string = 'ACK'
                    else:
                        string = 'NACK'
                else:
                    string = 'NACK'
            elif cmd == 'READ' :
                if len(msg) > 0 :
                    j = int(msg[0])
                    i = act_num.index(j)
                    pl = act_pqrst[i]
                    tl = act_time[i]
                    vl = acorr_val[i]
                    rl = acorr_rank[i]
                    pmx = len(pl)
                    pn = act_idx[i]
                    print pn, pmx
                    if pn < pmx:
#                        tt = time.strftime("%Y-%m-%d-%H-%M-%S")
                        tt = time.strftime("%Y-%m-%d-%H-%M-%S", time.strptime(tl[pn], "%Y/%m/%d %H:%M:%S"))
                        desc = str(vl[pn]) + ':' + str(rl[pn]) + ':' + tt
                        string = 'DATA ' + desc + ' ' + ' '.join([str(x) for x in pl[pn]])
                        act_idx[i] = pn+1
                    else:
                        string = 'NODATA'
                else:
                    string = 'NACK'
            elif cmd == 'CALC' :
                if len(msg) > 0 :
                    j = int(msg[0])
                    i = act_num.index(j)
                    lstart = act_start[i]
                    llen = act_len[i]
                    print act_n2n[j], act_name[i], lstart, llen
                    l1 = flist[lstart:lstart+llen]
                    l2tup = [(x[header.index('tstime')], x[header.index('ecgv')]) for x in l1]
                    l3tup = [x for x in l2tup if x[1] != 'null']
                    lines_tup = [(x[0], [int(y) for y in x[1].split(':')]) for x in l3tup]
                    l4 = get_pqrst(lines_tup)
                    l5 = l4[0]
                    act_pqrst[i] = l5
                    act_time[i] = l4[1]
                    l6 = corr_pqrst(l5)
                    acorr_val[i] = l6[0]
                    acorr_rank[i] = l6[1]
#                    corr_pqrst(i)
                    act_idx[i] = 0
                    string = 'CALC ' + str(len(l5)) + ":" + str(60*252/len(l5[0]))
                else:
                    string = 'NACK'
            elif cmd == 'ARM' :
                if len(msg) > 0 :
                    j = int(msg[0])
                    i = act_num.index(j)
                    act_idx[i] = 0
                    string = 'ACK '
                else:
                    string = 'NACK'
            elif cmd == 'START' :
                stream_ev = threading.Event()
                stream_th = threading.Thread(target=self.tf, args=(stream_ev,msg,))
                stream_th.start()
                string = 'ACK'
            elif cmd == 'STOP' :
                stream_ev.set()
                stream_th.join()
                stream_ev = None
                stream_th = None
                string = 'ACK'
            else:
                string = 'NACK'

        self.write_message(string)
        print 'send: ', string

    def on_close(self):
        global stream_th
        global stream_ev
        if stream_th != None:
            print 'stopping stream'
            stream_ev.set()
            stream_th.join()
        print 'connection closed...'

application = tornado.web.Application([
    (r'/', WSHandler),
])

if __name__ == "__main__":
    print len(mflist)
    thread = threading.Thread(target=threaded_function, args=(10,))
    thread.start()
    print 'Ready...'
    application.listen(8080)
    tornado.ioloop.IOLoop.instance().start()
