#!/bin/bash
set -x
docker stop namenode01; docker rm namenode01
docker stop namenode02; docker rm namenode02
docker stop datanode01; docker rm datanode01
docker stop datanode02; docker rm datanode02
docker stop datanode03; docker rm datanode03
docker stop datanode04; docker rm datanode04
