#!/bin/bash
set -x
./pipework docker0 $(docker run --net="none" --name namenode01 --privileged -c 1024 -m 8g -v /sys/fs/cgroup:/sys/fs/cgroup -d ambari5) 172.17.226.1/16@172.17.42.1
./pipework docker0 $(docker run --net="none" --name namenode02 --privileged -c 1024 -m 8g -v /sys/fs/cgroup:/sys/fs/cgroup -d ambari5) 172.17.226.2/16@172.17.42.1
./pipework docker0 $(docker run --net="none" --name datanode01 --privileged -c 1024 -m 8g -v /sys/fs/cgroup:/sys/fs/cgroup -v /mnt/d1p1:/data1 -v /mnt/d2p1:/data2 -v /mnt/d3p1:/data3 -v /mnt/d4p1:/data4 -d ambari5) 172.17.226.3/16@172.17.42.1
./pipework docker0 $(docker run --net="none" --name datanode02 --privileged -c 1024 -m 8g -v /sys/fs/cgroup:/sys/fs/cgroup -v /mnt/d1p2:/data1 -v /mnt/d2p2:/data2 -v /mnt/d3p2:/data3 -v /mnt/d4p2:/data4 -d ambari5) 172.17.226.4/16@172.17.42.1
./pipework docker0 $(docker run --net="none" --name datanode03 --privileged -c 1024 -m 8g -v /sys/fs/cgroup:/sys/fs/cgroup -v /mnt/d1p3:/data1 -v /mnt/d2p3:/data2 -v /mnt/d3p3:/data3 -v /mnt/d4p3:/data4 -d ambari5) 172.17.226.5/16@172.17.42.1
./pipework docker0 $(docker run --net="none" --name datanode04 --privileged -c 1024 -m 8g -v /sys/fs/cgroup:/sys/fs/cgroup -v /mnt/d1p4:/data1 -v /mnt/d2p4:/data2 -v /mnt/d3p4:/data3 -v /mnt/d4p4:/data4 -d ambari5) 172.17.226.6/16@172.17.42.1
