#!/bin/bash

for i in `seq 1 12`; do
  mount /dev/sdb$i /mnt/d1p$i -o noatime,nodiratime
done

for i in `seq 1 12`; do
  mount /dev/sdc$i /mnt/d2p$i -o noatime,nodiratime
done

for i in `seq 1 12`; do
  mount /dev/sdd$i /mnt/d3p$i -o noatime,nodiratime
done

for i in `seq 1 12`; do
  mount /dev/sde$i /mnt/d4p$i -o noatime,nodiratime
done


