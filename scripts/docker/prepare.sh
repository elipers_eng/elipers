#!/bin/bash
set -x

grep -v node0 .ssh/known_hosts > ./known_hosts;
cp -f ./known_hosts .ssh/known_hosts

for i in 1 2; do ssh-copy-id -i .ssh/id_rsa.pub root@namenode0$i; done
for i in 1 2 3 4; do ssh-copy-id -i .ssh/id_rsa.pub root@datanode0$i; done

for i in 1 2; do ssh namenode0$i "hostname namenode0$i" ; done
for i in 1 2 3 4; do ssh datanode0$i "hostname datanode0$i" ; done

for i in 1 2; do ssh namenode0$i "/etc/init.d/ntpd start" ; done
for i in 1 2 3 4; do ssh datanode0$i "/etc/init.d/ntpd start" ; done

for i in 1 2; do scp hosts.cluster namenode0$i:/etc; done
for i in 1 2 3 4; do scp hosts.cluster datanode0$i:/etc; done

for i in 1 2; do
  ssh namenode0$i "cp /etc/hosts /etc/hosts.saved; cat /etc/hosts.cluster /etc/hosts.saved > /etc/hosts"
done
for i in 1 2 3 4; do
  ssh datanode0$i "cp /etc/hosts /etc/hosts.saved; cat /etc/hosts.cluster /etc/hosts.saved > /etc/hosts"
