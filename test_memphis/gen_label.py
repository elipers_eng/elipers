import csv
from datetime import datetime
from datetime import timedelta

#labelfile = '12-07 SBG dODI Sets/Dusty/label.csv'
#datafile = '12-07 SBG dODI Sets/Dusty/Dusty-12-07-2018.csv'
#labelfile = '12-06 SBG dODI Sets/Andrew/label.csv'
#datafile = '12-06 SBG dODI Sets/Andrew/Andrew-12-06-2018.csv'
#labelfile = '12-05 SBG dODI Sets/Olu/label.csv'
#datafile = '12-05 SBG dODI Sets/Olu/Olu_train_dODI_hour_1.csv'
#labelfile = '12-05 SBG dODI Sets/Arjun/labela.csv'
#datafile = '12-05 SBG dODI Sets/Arjun/arjun_dODI_full_1a.csv'
labelfile = '12-05 SBG dODI Sets/Arjun/labelb.csv'
datafile = '12-05 SBG dODI Sets/Arjun/arjun_dODI_full_1b.csv'
#labelfile = '12-06 SBG dODI Sets/Pat/label.csv'
#datafile = '12-06 SBG dODI Sets/Pat/Pat-12-06-2018.csv'
#labelfile = '12-06 SBG dODI Sets/Lindsey/label.csv'
#datafile = '12-06 SBG dODI Sets/Lindsey/Lindsey-12-06-2018.csv'
#labelfile = '12-06 SBG dODI Sets/Nick/label.csv'
#datafile = '12-06 SBG dODI Sets/Nick/Nick-12-06-2018_dODI.csv'
#labelfile = '12-07 SBG dODI Sets/Barb/label.csv'
#datafile = '12-07 SBG dODI Sets/Barb/Barb-12-07-2018.csv'
#labelfile = '12-07 SBG dODI Sets/Gary/label.csv'
#datafile = '12-07 SBG dODI Sets/Gary/Gary-12-07-2018.csv'
#labelfile = '12-07 SBG dODI Sets/Ping/label.csv'
#datafile = '12-07 SBG dODI Sets/Ping/Ping-12-07-2018.csv'

datalblfile = datafile.replace('.csv','_lbl.csv')

labels = ['Unknown','Sitting','Standing','Sleeping','Walking','Driving','Loitering']
labeld = dict(zip(labels, range(0,len(labels))))

with open(labelfile) as f:
    reader = csv.reader(f)
    nlist = list(reader)

timezero = datetime.strptime(nlist[0][0], '%H:%M')

with open(datafile) as f:
    reader = csv.reader(f)
    hdr = next(reader)
    line1 = next(reader)
    line2 = next(reader)

tidx = hdr.index('timestamp_sensor')
useczero = int(line1[tidx])
usecstep = int(line2[tidx]) - int(line1[tidx])

def dt2usec(dt):
    return int((datetime.strptime(dt, '%H:%M')-timezero).total_seconds()*1000000) + useczero

def min2usec(dt):
    return int(dt)*60*1000000

nlist2 = [[dt2usec(q[0]), dt2usec(q[1]), min2usec(q[2])] + q[3:] for q in nlist]

llist2y = filter(lambda(x): x[1] - x[0] > 0, nlist2)
llist2zzz = [[q[0], q[1]-usecstep, q[3]] for q in llist2y]

fileout =  open(datalblfile, 'wt')
writer = csv.writer(fileout)
h = hdr + ['dODI_hand_label']
writer.writerow(h)

idx = 0
jdx = 0
chunk = 100000
with open(datafile, 'rt') as filein:
    reader = csv.reader(filein)
    lin = next(reader)
    for lin in reader:
        t = int(lin[tidx])
        if t >= llist2zzz[jdx][0] and t <= llist2zzz[jdx][1]:
            lbl = llist2zzz[jdx][2]
        if t > llist2zzz[jdx][1]:
            if jdx+1 < len(llist2zzz) and t < llist2zzz[jdx+1][0]:
                lbl = 'Unknown'
        if t+usecstep > llist2zzz[jdx][1]:
            if jdx+1 < len(llist2zzz) and t+usecstep == llist2zzz[jdx+1][0]:
                jdx += 1
        lout = lin + [lbl]
#        lout = lin + [labeld[lbl]]
        writer.writerow(lout)
        if idx%chunk == 0:
            print(lout)
        idx += 1

fileout.close()

