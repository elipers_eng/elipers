import time

from py4j.java_gateway import JavaGateway, CallbackServerParameters

shm_objlen = 25

class SHM:
    def __init__(self):
        self.count = 0
        self.obj = ','.join(['null'] * shm_objlen)
        self.last_good_obj = ','.join(['null'] * shm_objlen)
        self.ecgv = []
        print "start/end shm"

    def re_init(self):
        x = self.obj.split(",")
        if x[0] != 'null':
            self.last_good_obj = self.obj
        self.count = 0
        self.obj = ','.join(['null'] * shm_objlen)
        self.ecgv = []

shmd = None #seting the global variable

class PythonListener(object):
    def __init__(self, gateway):
        global shmd #bring it in scope
        self.gateway = gateway
        shmd = SHM()

    def notify(self, obj):
        global shmd #bring it in scope
        x = obj.split(",")
        shmd.obj = obj
        if x[15] != 'null':
            shmd.ecgv.append(str(x[15]))
        shmd.count += 1
        return "A Return Value"

    class Java:
        implements = ["com.totemspark.ShimmerListener"]

shmgw = JavaGateway(callback_server_parameters=CallbackServerParameters())
shmlr = PythonListener(shmgw)
shmp = None
try:
    shmp = shmgw.entry_point.getInstance()
    shmp.setListener(shmlr)
except Exception, e:
    print "java gateway did not start: ", str(e)

while True :
    t = time.strftime("%Y-%m-%d %H:%M:%S")
    if len(shmd.ecgv) > 0:
        shm_ecgv = ':'.join(shmd.ecgv)
    else:
        shm_ecgv = 'null'
    y = shmd.obj.split(",")
    yy = y[0]
    if yy == 'null':
        z = shmd.last_good_obj.split(",")
        zz = z[0]
    else:
        zz = yy
    logstr = ("%s,%s" % (yy,zz))
    print t, ': ', logstr
    shmd.re_init()
    time.sleep(1)

#gateway.shutdown()
