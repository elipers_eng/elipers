import serial
import time

ser = serial.Serial("/dev/ttyUSB0", 115200, timeout=1)
if(ser.isOpen() == True):
    ser.close()
ser.open()
ser.write(b"\x02")
x = ser.read()
print x
x = ser.readline()
print x
ser.write(b"\x40")
x = ser.read()
print x
ser.write(b"\xf5")
ser.write(b"\x01")
x = ser.read()
print x
ser.write(b"\x23")
ser.write(b"\x03")
x = ser.read()
print x

while True:
    t = time.strftime("%Y-%m-%d %H:%M:%S") 
    l = ser.readline()
    if len(l) > 0 :
#        print l
        f = l.split()
        dst_type =  f[2] + f[3] + f[4]
        dst_id = f[5]
        src =  f[6]
        cmd =  f[9]
        print dst_type, dst_id, src, cmd
        if dst_type == "808000" and cmd == "06" and src == "B2":
            index = str(int(f[12] + f[11] + f[10], 16))
            batt  = str(int(f[14] + f[13], 16))
            temp  = str(float(int(f[16] + f[15], 16)) / 10)
            print t + ":" + index + "," + batt + "," + temp
    l = ser.readline()
