#!/usr/bin/env python

import logging
import logging.handlers
import sys
import time
import subprocess
import threading
import os
#import gps
#from gps import *
import serial
import bluetooth
import numpy

from py4j.java_gateway import JavaGateway, CallbackServerParameters

bc_port = '/dev/ttyUSB0'
#zep_baddr = 'A0:E6:F8:4A:8F:36'
zep_baddr = 'B0:B4:48:04:9C:38'
lat = 28.424715
lon = -81.469534

hist_period = 60
heart_hist = []
hrtv_hist  = []
pos_hist   = []
intp_hist  = []
batt_hist  = []
hrtc_hist  = []
ecgn_hist  = []
pacc_hist  = []
hist_ptr = 0
hist_cnt = 0

def calc_history():
    global heart_hist
    global hrtv_hist
    global pos_hist
    global intp_hist
    global batt_hist
    global hrtc_hist
    global ecgn_hist
    global pacc_hist
    global hist_ptr
    global hist_cnt
    if heart != 'null':
        heart_hist.append(float(heart))
    if hrtv != 'null':
        hrtv_hist.append(float(hrtv))
    if pos != 'null':
        pos_hist.append(float(pos))
    if intp != 'null':
        intp_hist.append(float(intp))
    if batt != 'null':
        batt_hist.append(float(batt))
    if hrtc != 'null':
        hrtc_hist.append(float(hrtc))
    if ecgn != 'null':
        ecgn_hist.append(float(ecgn))
    if pacc != 'null':
        pacc_hist.append(float(pacc))
        hist_ptr += 1
        if hist_ptr == hist_period:
            if heart_hist != []:
                heart_mean = numpy.mean(heart_hist)
                heart_min = numpy.min(heart_hist)
                heart_max = numpy.max(heart_hist)
                heart_std = numpy.std(heart_hist)
            else:
                heart_mean = 'null'
                heart_min = 'null'
                heart_max = 'null'
                heart_std = 'null' 
            if hrtv_hist != []:
                hrtv_mean = numpy.mean(hrtv_hist)
            else:
                hrtv_mean = 'null'
            if pos_hist != []:
                pos_mean = numpy.mean(pos_hist)
            else:
                pos_mean = 'null'
            if intp_hist != []:
                intp_mean = numpy.mean(intp_hist)
            else:
                intp_mean = 'null'
            if batt_hist != []:
                batt_min = numpy.min(batt_hist)
            else:
                batt_min = 'null'
            if hrtc_hist != []:
                hrtc_min = numpy.min(hrtc_hist)
            else:
                hrtc_min = 'null'
            if ecgn_hist != []:
                ecgn_max = numpy.max(ecgn_hist)
            else:
                ecgn_max = 'null'
            if pacc_hist != []:
                pacc_max = numpy.max(pacc_hist)
            else:
                pacc_max = 'null'
#            loraData = ("%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s" % (hist_cnt,lat,lon,heart_mean,heart_min,heart_max,heart_std,vmu,bt,ectp,hrtv_mean,pos_mean,intp_mean,batt_min,hrtc_min,ecgn_max,pacc_max))
#            print loraData
#            os.system('python /home/pi/lora_send.py -d '+loraData)
            heart_hist = []
            hrtv_hist  = []
            pos_hist   = []
            intp_hist  = []
            batt_hist  = []
            hrtc_hist  = []
            ecgn_hist  = []
            pacc_hist  = []
            hist_ptr = 0
            hist_cnt += 1
    else:
        hist_ptr = 0

gpsd = None #seting the global variable

class GpsPoller(threading.Thread):
    def __init__(self):
        threading.Thread.__init__(self)
        global gpsd #bring it in scope
        gpsd = gps(mode=WATCH_ENABLE) #starting the stream of info
        self.current_value = None
        self.running = True #setting the thread running to true

    def run(self):
        global gpsd
        while gpsp.running:
            gpsd.next() #this will continue to loop and grab EACH set of gpsd info to clear the buffer

class BCD:
    def __init__(self):
        self.index = 'null'
        self.batt = 'null'
        self.temp = 'null'

bcd = None #seting the global variable
ser = None #seting the global variable

class BCPoller(threading.Thread):
    def __init__(self):
        threading.Thread.__init__(self)
        global bcd #bring it in scope
        global ser #bring it in scope
        if os.path.exists(bc_port):
            ser = serial.Serial(bc_port, 115200, timeout=1)
            if(ser.isOpen() == True):
                ser.close()
                ser.open()
                ser.write(b"\x02")
                x = ser.read()
                x = ser.readline()
                ser.write(b"\x40")
                x = ser.read()
                ser.write(b"\xf5")
                ser.write(b"\x01")
                x = ser.read()
                ser.write(b"\x23")
                ser.write(b"\x03")
                x = ser.read()
                bcd = BCD()
                self.current_value = None
                self.running = True #setting the thread running to true
        else:
            print bc_port + " does not exist"
            self.running = False

    def run(self):
        global bcd
        global ser
        l = ser.readline()
        while bcp.running:
            if len(l) > 0 :
                f = l.split()
                dst_type =  f[2] + f[3] + f[4]
                dst_id = f[5]
                src =  f[6]
                cmd =  f[9]
                if dst_type == "808000" and cmd == "06" and src == "B2":
                    bcd.index = str(int(f[12] + f[11] + f[10], 16))
                    bcd.batt  = str(int(f[14] + f[13], 16))
                    bcd.temp  = str(float(int(f[16] + f[15], 16)) / 10)
            l = ser.readline()
            
class ZEP:
    def __init__(self):
        self.first_life = 0
        self.gen_packet = []
        self.brt_packet = []
        self.ecg_packet = []
        self.acc_packet = []
        self.sum_packet = []

lifesign_msg = '\x02\x23\x00\x00\x03'
setgenon_msg = '\x02\x14\x01\x01\x5e\x03'
setbrton_msg = '\x02\x15\x01\x01\x5e\x03'
setecgon_msg = '\x02\x16\x01\x01\x5e\x03'
setaccon_msg = '\x02\x1e\x01\x01\x5e\x03'
setsumon_msg = '\x02\xbd\x02\x01\x00\xc4\x03'

zep = None #seting the global variable
rfs = None #seting the global variable
gen_tmp = []
brt_tmp = []
ecg_tmp = []
acc_tmp = []
sum_tmp = []

def byte2unsigned(x):
    return x

def word2unsigned(x,y):
    return x*256 + y

def byte2signed(x):
    if x > 127 :
        return (256-x) * (-1)
    else :
        return x

def word2signed(x,y):
    if x > 127 :
        return (65536 - (x*256+y)) * (-1)
    else :
        return x*256 + y

def tsunsigned(x, s):
    if x == 65535 :
        return 'null'
    if s == 1 :
        return str(x)
    return str(float(x)/s)
        
def tsunsignedb(x, s):
    if x == 255 :
        return 'null'
    if s == 1 :
        return str(x)
    return str(float(x)/s)
        
def tssigned(x, s):
    if x == -32768 :
        return 'null'
    if s == 1 :
        return str(x)
    return str(float(x)/s)

def tssignedb(x, s):
    if x == -128 :
        return 'null'
    if s == 1 :
        return str(x)
    return str(float(x)/s)

def five_to_four(x) :
    y0 = (x[0]) + ((x[1] & 0x03) << 8)
    y1 = ((x[1] >> 2) & 0x3f) + ((x[2] & 0x0f) << 6)
    y2 = ((x[2] >> 4) & 0x0f) + ((x[3] & 0x3f) << 4)
    y3 = ((x[3] >> 6) & 0x03) + (x[4] << 2)
    return [y0, y1, y2, y3]

def four_to_three(x) :
    y0 = (x[0]) + ((x[1] & 0x03) << 8)
    y1 = ((x[1] >> 2) & 0x3f) + ((x[2] & 0x0f) << 6)
    y2 = ((x[2] >> 4) & 0x0f) + ((x[3] & 0x3f) << 4)
    return [y0, y1, y2]

class ZepPoller(threading.Thread):
    def __init__(self):
        threading.Thread.__init__(self)
        global zep #bring it in scope
        global rfs #bring it in scope
        rfs = bluetooth.BluetoothSocket(bluetooth.RFCOMM)
        print "start bt"
        try:
            rfs.connect((zep_baddr, 1))
            zep = ZEP()
            self.current_value = None
            self.running = True #setting the thread running to true
        except Exception as e:
            print zep_baddr + " does not connect: ", str(e)
            self.running = False
        print "end bt"
    def run(self):
        global zep
        global rfs
        global gen_tmp
        global brt_tmp
        global ecg_tmp
        global acc_tmp
        global sum_tmp
        while zpp.running:
            xr = rfs.recv(1024)
            yr = [hex(ord(i)) for i in xr]
            yr2 = [ord(i) for i in xr]
            if yr[1] == '0x23' :
                if zep.first_life == 0 :
                    rfs.send(setgenon_msg)
                    rfs.send(setbrton_msg)
                    rfs.send(setecgon_msg)
                    rfs.send(setaccon_msg)
                    rfs.send(setsumon_msg)
                    zep.first_life = 1
                rfs.send(lifesign_msg)
            elif yr[1] == '0x20' :
                zep.gen_packet = gen_tmp
                zep.brt_packet = brt_tmp
                zep.ecg_packet = ecg_tmp
                zep.acc_packet = acc_tmp
                zep.sum_packet = sum_tmp
                gen_tmp = [yr2]
                brt_tmp = []
                ecg_tmp = []
                acc_tmp = []
                sum_tmp = []
            elif yr[1] == '0x21' :
                brt_tmp.append(yr2)
            elif yr[1] == '0x22' :
                ecg_tmp.append(yr2)
            elif yr[1] == '0x25' :
                acc_tmp.append(yr2)
            elif yr[1] == '0x2b' :
                sum_tmp.append(yr2)

#shm_objlen = 21
shm_objlen = 25

class SHM:
    def __init__(self):
        self.count = 0
        self.obj = ','.join(['null'] * shm_objlen)
        self.last_good_obj = ','.join(['null'] * shm_objlen)
        self.ecgv = []
        print "start/end shm"

    def re_init(self):
        x = self.obj.split(",")
        if x[0] != 'null':
            self.last_good_obj = self.obj
        self.count = 0
        self.obj = ','.join(['null'] * shm_objlen)
        self.ecgv = []

shmd = None #seting the global variable

class PythonListener(object):
    def __init__(self, gateway):
        global shmd #bring it in scope
        self.gateway = gateway
        shmd = SHM()

    def notify(self, obj):
        global shmd #bring it in scope
        x = obj.split(",")
        shmd.obj = obj
        if x[15] != 'null':
            shmd.ecgv.append(str(x[15]))
        shmd.count += 1
        return "A Return Value"

    class Java:
        implements = ["com.totemspark.ShimmerListener"]

sys_logger = logging.getLogger(__name__)
sys_logger.setLevel(logging.INFO)
sys_formatter = logging.Formatter("%(asctime)s - %(name)s - %(levelname)s - %(message)s")
sys_handler = logging.FileHandler("/var/log/tsbridge/tsbridge_sys.log")
sys_handler.setFormatter(sys_formatter)
sys_logger.addHandler(sys_handler)

class MyLogger(object):
    def __init__(self, logger, level):
        self.logger = logger
        self.level = level

    def write(self, message):
        if message.rstrip() != "":
           self.logger.log(self.level, message.rstrip())

sys.stdout = MyLogger(sys_logger, logging.INFO)
sys.stderr = MyLogger(sys_logger, logging.ERROR)

tsb_logger = logging.getLogger("TSBridgeLog")
tsb_logger.setLevel(logging.INFO)
tsb_formatter = logging.Formatter("%(asctime)s,%(message)s","%Y-%m-%d %H:%M:%S")
tsb_handler = logging.handlers.RotatingFileHandler("/var/log/tsbridge/tsbridge.log", maxBytes=100000, backupCount=8)
#tsb_handler = logging.handlers.TimedRotatingFileHandler("/var/log/tsbridge/tsbridge.log", when="h", interval=6, backupCount=12)
tsb_handler.setFormatter(tsb_formatter)
tsb_logger.addHandler(tsb_handler)

#gpsp = GpsPoller()
#gpsp.start()

bcp = BCPoller()
if bcp.running:
    bcp.start()

# wait for bluetooth no time to mess with systemd service unit
time.sleep(6)

zpp = ZepPoller()
if zpp.running:
    zpp.start()

shmgw = JavaGateway(callback_server_parameters=CallbackServerParameters())
shmlr = PythonListener(shmgw)
shmp = None
try:
    shmp = shmgw.entry_point.getInstance()
    shmp.setListener(shmlr)
except Exception, e:
    print "java gateway did not start: ", str(e)

while True:
#    x = subprocess.check_output(['vcgencmd','measure_temp'])
#    temp=x[x.index('=')+1:x.index('C')-1]
#    y = subprocess.check_output(['vcgencmd','measure_volts'])
#    volt=y[y.index('=')+1:y.index('V')]
    temp = 0
    volt = 0
#    lat = gpsd.fix.latitude 
#    lon = gpsd.fix.longitude 
#    ys = [x.used for x in gpsd.satellites].count(True)
#    lat = 0
#    lon = 0
    ys = 0
    bi = 'null'
    bb = 'null'
    bt = 'null'
    if bcp.running:
        bi = bcd.index
        bb = bcd.batt
        bt = bcd.temp
    heart = 'null'
    resp  = 'null'
    sktp  = 'null'
    pos   = 'null'
    vmu   = 'null'
    pacc  = 'null'
    volt  = 'null'
    brta  = 'null'
    ecga  = 'null'
    ecgn  = 'null'
    acvm  = 'null'
    acvp  = 'null'
    aclm  = 'null'
    aclp  = 'null'
    acsm  = 'null'
    acsp  = 'null'
    hrtc = 'null'
    hrtv = 'null'
    batt = 'null'
    intp = 'null'
    ectp = 'null'
    ecgv = 'null'
    ecgc = ['', '', '', '']
    if zpp.running:
        if len(zep.gen_packet) > 0 :
            zgp = zep.gen_packet[0]
            heart = tsunsigned(word2unsigned(zgp[13],zgp[12]),1)
            resp  = tsunsigned(word2unsigned(zgp[15],zgp[14]),10)
            sktp  = tssigned(word2signed(zgp[17],zgp[16]),10)
            pos   = tssigned(word2signed(zgp[19],zgp[18]),1)
            vmu   = tsunsigned(word2unsigned(zgp[21],zgp[20]),100)
            pacc  = tsunsigned(word2unsigned(zgp[23],zgp[22]),100)
            volt  = tsunsigned(word2unsigned(zgp[25],zgp[24]),1000)
            brta  = tsunsigned(word2unsigned(zgp[27],zgp[26]),1)
            ecga  = tsunsigned(word2unsigned(zgp[29],zgp[28]),1000000)
            ecgn  = tsunsigned(word2unsigned(zgp[31],zgp[30]),1000000)
            acvm  = tssigned(word2signed(zgp[33],zgp[32]),100)
            acvp  = tssigned(word2signed(zgp[35],zgp[34]),100)
            aclm  = tssigned(word2signed(zgp[37],zgp[36]),100)
            aclp  = tssigned(word2signed(zgp[39],zgp[38]),100)
            acsm  = tssigned(word2signed(zgp[41],zgp[40]),100)
            acsp  = tssigned(word2signed(zgp[43],zgp[42]),100)
        if len(zep.sum_packet) > 0 :
            zsp = zep.sum_packet[0]
            hrtc  = tsunsignedb(byte2unsigned(zsp[37]),1)
            hrtv  = tssigned(word2signed(zsp[39],zsp[38]),10)
            batt  = tsunsignedb(byte2unsigned(zsp[27]),1)
            intp  = tssigned(word2signed(zsp[58],zsp[57]),10)
            ectp  = tssigned(word2signed(zsp[65],zsp[64]),10)
        if len(zep.ecg_packet) == 4 :
            for i in xrange(0, 4):
                y2 = zep.ecg_packet[i]
                c1 = five_to_four(y2[12:17])
                c2 = five_to_four(y2[17:22])
                c3 = five_to_four(y2[22:27])
                c4 = five_to_four(y2[27:32])
                c5 = five_to_four(y2[32:37])
                c6 = five_to_four(y2[37:42])
                c7 = five_to_four(y2[42:47])
                c8 = five_to_four(y2[47:52])
                c9 = five_to_four(y2[52:57])
                c10 = five_to_four(y2[57:62])
                c11 = five_to_four(y2[62:67])
                c12 = five_to_four(y2[67:72])
                c13 = five_to_four(y2[72:77])
                c14 = five_to_four(y2[77:82])
                c15 = five_to_four(y2[82:87])
                c16 = four_to_three(y2[87:91])
                ecgc[i] = ':'.join([str(x) for x in c1+c2+c3+c4+c5+c6+c7+c8+c9+c10+c11+c12+c13+c14+c15+c16])
            ecgv = ':'.join(ecgc)

    if (bcp.running or zpp.running or shmp != None):
        if (zpp.running):
            logstr = ("%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s" % (heart,resp,sktp,pos,vmu,pacc,volt,brta,ecga,ecgn,acvm,acvp,aclm,aclp,acsm,acsp,hrtc,hrtv,batt,intp,ectp,ecgv,bi,bb,bt))
            tsb_logger.info(logstr)
            calc_history()
        elif shmp != None:
            if len(shmd.ecgv) > 0:
                shm_ecgv = ':'.join(shmd.ecgv)
            else:
                shm_ecgv = 'null'
            logstr = ("%s,%s,%s,%s,%s" % (shmd.obj,shm_ecgv,bi,bb,bt))
            tsb_logger.info(logstr)
            shmd.re_init()
    else:
        print "tsbridge has nothing to log; break"
        break
    time.sleep(1)
    



