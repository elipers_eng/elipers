#!/usr/bin/env python

import logging
import logging.handlers
import sys
import time
import random
import paramiko
import csv
import os

sys_logger = logging.getLogger(__name__)
sys_logger.setLevel(logging.INFO)
sys_formatter = logging.Formatter("%(asctime)s - %(name)s - %(levelname)s - %(message)s")
sys_handler = logging.FileHandler("/var/log/tsharvest/tsharvest_sys.log")
sys_handler.setFormatter(sys_formatter)
sys_logger.addHandler(sys_handler)

class MyLogger(object):
    def __init__(self, logger, level):
        self.logger = logger
        self.level = level

    def write(self, message):
        if message.rstrip() != "":
           self.logger.log(self.level, message.rstrip())

sys.stdout = MyLogger(sys_logger, logging.INFO)
sys.stderr = MyLogger(sys_logger, logging.ERROR)

tsb_logger = logging.getLogger("TSHarvestLog")
tsb_logger.setLevel(logging.INFO)
tsb_formatter = logging.Formatter("%(asctime)s,%(message)s","%Y-%m-%d %H:%M:%S")
tsb_handler = logging.handlers.RotatingFileHandler("/var/log/tsharvest/tsharvest.log", maxBytes=10000, backupCount=8)
tsb_handler.setFormatter(tsb_formatter)
tsb_logger.addHandler(tsb_handler)


#tb = ['tsbridge01','tsbridge02','tsbridge03','tsbridge04']
tb = ['tsbridge02']
tblast = ['null', 'null', 'null', 'null']
nb = len(tb)
fname0 = ['/var/log/tsharvest/' + x + '/tsbridge.log' for x in tb]
fname1 = ['/var/log/tsharvest/' + x + '/tsbridge.log.1' for x in tb]

ssh = paramiko.SSHClient()
ssh.load_system_host_keys()
ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())

while True:
    for i in xrange(0,nb) :
        try: 
            ssh.connect(tb[i], username="pi")
            sftp = ssh.open_sftp()
            try:
                sftp.get('/var/log/tsbridge/tsbridge.log', fname0[i])
            except Exception, e:
                print "SFTPGET: ", str(e),  fname0[i]
            try:
                sftp.get('/var/log/tsbridge/tsbridge.log.1', fname1[i])
            except Exception, e:
                print "SFTPGET: ", str(e),  fname1[i]
            sftp.close()
            ssh.close()
        except Exception, e:
            print "SSHCONNECT: ", str(e),tb[i]
            os.remove(fname0[i])
            os.mknod(fname0[i], 0664)
            os.remove(fname1[i])
            os.mknod(fname1[i], 0664)

    ftstime = [[]] * nb
    frest = [[]] * nb

    for i in xrange(0,nb) :
        f0 = open(fname0[i])
        f1 = open(fname1[i])
        lines = [r for r in csv.reader(f1)] + [r for r in csv.reader(f0)]
        if len(lines) > 0:
            ftstime[i] = [x[0] for x in lines]
            frest[i] = [','.join(x[1:]) for x in lines]
        f0.close()
        f1.close()

    fnumlines = [len(x) for x in ftstime]
    if not all(x == 0 for x in fnumlines):
        shortest_i  = fnumlines.index(min(filter(None,fnumlines)))
        first_tstime = ftstime[shortest_i][0]

        fil_ftstime = filter(None, ftstime)
        in_list =[first_tstime in x for x in fil_ftstime]
        if all(in_list) :
            idx = [x.index(first_tstime) for x in fil_ftstime] 
            rlen = [len(x) - x.index(first_tstime) for x in fil_ftstime]
            min_rlen = min(rlen)
            for i in xrange(0,nb) :
                if len(frest[i]):
                    t = ftstime[i][idx[i]:][:min_rlen]
                    break
            j = 0
            for i in xrange(0,nb) :
                if len(frest[i]):
                    r = frest[i][idx[j]:][:min_rlen]
                    j += 1
                else:
                    r = [tblast[i]] * len(t)
                q = zip(t, r)
                t = [','.join(x) for x in q]

            for i in xrange(0,nb):
                if len(frest[i]) > 0:
                    tblast[i] = frest[i][-1]

            with open("/var/log/tsharvest/tsbridgeall/next_index", 'r') as f:
                x = f.readline().strip()
            with open("/var/log/tsharvest/tsbridgeall/all" + x + ".log", 'w') as f:
                for i in xrange(0, len(t)) :
                    f.write(t[i] + '\n')
            nxt = int(x) + 1
            if nxt == 6 :
                nxt = 0
            with open("/var/log/tsharvest/tsbridgeall/next_index", 'w') as f:
                f.write(str(nxt) + '\n')
        else:
            print "panic 1: ", first_tstime, in_list
            # remove this !!!
            for k in xrange(0,nb):
                if len(ftstime[k]) > 0:
                    print ("%s to %s [%s]" % (ftstime[k][0], ftstime[k][-1], len(ftstime[k])))
            # remove this !!!

            nxt = 999
            idx = [0] * nb
            rlen = [0] * nb
    else:
        print "panic 2"
        first_tstime = time.strftime("%Y-%m-%d %H:%M:%S",time.gmtime(0))
        nxt = 999
        idx = [0] * nb
        rlen = [0] * nb

    tsb_logger.info("%s,%s,%s,%s,%s" % (fnumlines, first_tstime, idx, rlen, nxt))
    time.sleep(5)



