import tornado.ioloop
import tornado.web
import tornado.websocket
import tornado.template

import csv
import time
import threading
import numpy

model = None
lookback = 10
min_corr = 0.95
max_dev = 0.01

def calc_state(x, y):
    print x, y
    if model == None :
        return 0
    return int(model.predict([float(x), float(y)]))

def calc_ecggb2(racer, o1):
    brg = racer_bridge[racer]
    if o1 == 'null':
        return 'BAD:0:1'
    o15 = o1.split(":")
    if brg == 'Zephyr':
        o2 = [int(x) for x in o15]
    elif brg == 'Shimmer':
        o2 = [float(x) for x in o15]
    if len(mglist[racer]) > lookback:
        lsub = mglist[racer][-lookback:]
        lsub1 = [p for p in lsub if p != 'null']
        if len(lsub1) > 0:
            if brg == 'Zephyr':
                lsub2 = [[int(y) for y in x.split(":")] for x in lsub1]
            elif brg == 'Shimmer':
                lsub2 = [[float(y) for y in x.split(":")] for x in lsub1]
            corrs = [numpy.corrcoef(o2, x)[0][1] for x in lsub2]
            means = [numpy.mean(x) for x in lsub2]
            match = numpy.mean(corrs)
            dev   = abs(numpy.mean(o2) - numpy.mean(means)) / abs(numpy.mean(means))
        else:
            match = 0
            dev = 1
    else:
        match = 1
        dev = 0
    if match > min_corr and dev < max_dev:
        state = "GOOD"
    else:
        state = "BAD"
    return state + ':' + str(match) + ':' + str(dev)

def get_ecggb2(racer, line):
    return eglist[racer][line]

def calc_ecggb(racer, o1):
    if len(o1) < 2:
        return 'BAD:0:1'
    o2 = [int(x) for x in o1]
    gl = good_list[racer]
    if len(gl) >= lookback:
        corrs = [numpy.corrcoef(o2, x)[0][1] for x in gl]
        means = [numpy.mean(x) for x in gl]
        match = numpy.mean(corrs)
        dev   = abs(numpy.mean(o2) - numpy.mean(means)) / numpy.mean(means)
    else:
        match = 1
        dev = 0
    print racer, ": ", match
    if match > min_corr and dev < max_dev:
        if len(gl) == lookback:
            gl.pop(0)
            ycount[racer] += 1
        gl.append(o2)
        return 'GOOD:' + str(match) + ':' + str(dev)
    else:
        ncount[racer] += 1
        return 'BAD:' + str(match) + ':' + str(dev)

zp_vmu_offset = 4
zp_pacc_offset = 5
zp_ecgv_offset = 21

sh_ecgv_offset = 25

num_racer = 4
good_list = [[],[],[],[]]
ycount = [0, 0, 0, 0]
ncount = [0, 0, 0, 0]

with open("randwalk.csv") as f:
    rows = csv.reader(f)
    location = [r for r in rows]
    
loc_pointer = 0
loc_max = len(location)

with open("harvest_header_zp.csv") as f:
    rows = csv.reader(f)
    header_z = [r for r in rows][0]

with open("harvest_header_sh.csv") as f:
    rows = csv.reader(f)
    header_s = [r for r in rows][0]

with open("harvest_header_mn.csv") as f:
    rows = csv.reader(f)
    header_m = [r for r in rows][0]

#racer_bridge = ['Zephyr','Zephyr','Zephyr','Zephyr']
racer_bridge = ['Unknown','Zephyr','Unknown','Unknown']
racer_fldlen = [0, 0, 0, 0]
racer_fldidx = [0, 0, 0, 0]

racer_lat = [0, 0, 0, 0]
racer_lon = [0, 0, 0, 0]
racer_state = [0, 0, 0, 0]
racer_ecggb = ['', '', '', '']

header = ['tstime']
accum = 1
for i in xrange(0, num_racer):
    if racer_bridge[i] == 'Zephyr':
        fields = [x + str(i) for x in header_z[1:]]
    elif racer_bridge[i] == 'Shimmer':
        fields = [x + str(i) for x in header_s[1:]]
    elif racer_bridge[i] == 'Minimum':
        fields = [x + str(i) for x in header_m[1:]]
    else:
        fields = []
    header += fields
    racer_fldlen[i] = len(fields)
    racer_fldidx[i] = accum
    accum += len(fields)

fields = []
for i in xrange(0, num_racer):
    fields += [racer_fldidx[i]-1, racer_fldlen[i]]

calcs = []
accum = racer_fldidx[-1] -1 + racer_fldlen[-1]
for i in xrange(0, num_racer):
    if racer_bridge[i] != 'Unknown':
        calcs += [accum, 4]
        accum += 4
    else:    
        calcs += [accum, 0]

descriptor = ':'.join([str(i) for i in fields+calcs])

ecgfix = [0, 0, 0, 0]
mglist = [[], [], [], []]
eglist = [[], [], [], []]
for i in xrange(0, num_racer):
    if racer_bridge[i] == 'Zephyr':
        ecgfix[i] = racer_fldidx[i] + zp_ecgv_offset
    elif racer_bridge[i] == 'Shimmer':
        ecgfix[i] = racer_fldidx[i] + sh_ecgv_offset

print racer_bridge, racer_fldidx, racer_fldlen, calcs
        
nf = 6
flist = [None] * nf

mflist = []
calclist = [[],[],[],[]]
line_num = 0
stream_th = None
stream_ev = None

for i in xrange(0, nf):
    with open("/var/log/tsharvest/tsbridgeall/all" + str(i) + ".log") as f:
        flist[i] = [r for r in csv.reader(f)]

lasttime = [x[-1][0] for x in flist]
lasttime_t = [time.strptime(x, "%Y-%m-%d %H:%M:%S") for x in lasttime]
min_idx = lasttime_t.index(min(lasttime_t))

oflist = flist[min_idx:] + flist[:min_idx]
otlist = [[q[0] for q in p] for p in oflist]
endtime = [x[-1] for x in otlist]

otidx = [0] * nf    
for i in xrange(0, nf-1):
    if endtime[i] in otlist[i+1]:
        otidx[i+1] = otlist[i+1].index(endtime[i]) + 1
    else:
        print "no overlap in log file"

for i in xrange(0, nf):
    mflist += oflist[i][otidx[i]:]

def peaktest_zp(v9):
    if sum(v9[0:3]) >= 12 and sum(v9[6:9]) <= -15:
        return True
    return False

def peaktest_sh(v9):
    if sum(v9[0:3]) >= 0.5 and sum(v9[6:9]) <= -0.2:
        return True
    return False

def find_pqrst(brg,d1):
    v1 = [d1[i+1]-d1[i] for i in xrange(len(d1)-1)]
    if brg == 'Zephyr':
        x1 = [peaktest_zp(v1[i:i+9]) for i in xrange(len(v1)-8)]
    elif brg == 'Shimmer':
        x1 = [peaktest_sh(v1[i:i+9]) for i in xrange(len(v1)-8)]
    y1 = [i for i, x in enumerate(x1) if x == True]
    z1 = [y1[i+1]-y1[i] > 1 for i in xrange(len(y1)-1)]
    p1 = [y1[i] for i in xrange(len(y1)-1) if y1[i+1] - y1[i] > 1 or i == len(y1)-2]
    if brg == 'Zephyr':
        p2 = [i+5 for i in p1]
    elif brg == 'Shimmer':
        p2 = [i+5 for i in p1]
    return p2

no_signal = 0
not_found = 0
on_edge = 0
good_sig = 0

def get_pqrst(racer,v0):
    global no_signal
    global not_found
    global on_edge
    global good_sig

    brg = racer_bridge[racer]
    if v0 == 'null':
        no_signal += 1
        return 'null'
    d0 = v0.split(":")
    if brg == 'Zephyr':
        d1 = [int(x) for x in d0]
    elif brg == 'Shimmer':
        d1 = [float(x) for x in d0]
    e0 = find_pqrst(brg,d1)
    if len(e0) == 0:
        not_found += 1
        return 'null'
    f0 = e0[0]
    if brg == 'Zephyr':
        bnd = 20
    elif brg == 'Shimmer':
        bnd = 20
    if f0-bnd >= 0 and f0+bnd+1 < len(d1):
        pqrst = ':'.join([str(x) for x in d1[f0-bnd:f0+bnd+1]])
        good_sig += 1
        return pqrst
    else:
        on_edge += 1
        return 'null'

for i in xrange(0, len(mflist)):
    for j in xrange(0, num_racer):
        ecgi = ecgfix[j]
        if ecgi != 0:
            v0 = mflist[i][ecgi]
            pqrst = get_pqrst(j, v0)
            mflist[i][ecgi] = pqrst
            ecggb2 = calc_ecggb2(j, pqrst)
            eglist[j].append(ecggb2)
            mglist[j].append(pqrst)
print no_signal, not_found, on_edge, good_sig
print len(mglist[0])
print len(mglist[1])
print len(mglist[2])
print len(mglist[3])
print len(eglist[0])
print len(eglist[1])
print len(eglist[2])
print len(eglist[3])

def threaded_function(arg):
    global mflist
#    for i in xrange(0, arg):
    while True:
        for i in xrange(0, nf):
            with open("/var/log/tsharvest/tsbridgeall/all" + str(i) + ".log") as f:
                flist[i] = [r for r in csv.reader(f)]
        lasttime = [x[-1][0] for x in flist]
        lasttime_t = [time.strptime(x, "%Y-%m-%d %H:%M:%S") for x in lasttime]
        min_idx = lasttime_t.index(min(lasttime_t))
        endtime = mflist[-1][0]
        max_idx = (min_idx-1)%nf
        maxlist = flist[max_idx]
        maxtlist = [q[0] for q in maxlist]
        if endtime in maxtlist:
            maxidx = maxtlist.index(endtime) + 1
#            print endtime, maxidx, len(maxtlist)
            if maxidx != len(maxtlist) :
                addflist = maxlist[maxidx:]
                for i in xrange(0, len(addflist)):
                    for j in xrange(0, num_racer):
                        ecgi = ecgfix[j]
                        if ecgi != 0:
                            v0 = addflist[i][ecgi]
                            pqrst = get_pqrst(j, v0)
                            addflist[i][ecgi] = pqrst
                            ecggb2 = calc_ecggb2(j, pqrst)
                            eglist[j].append(ecggb2)
                            mglist[j].append(pqrst)
                mflist += addflist
            print "FILL:", len(maxtlist) - maxidx, '[', line_num, ':', len(mflist), ']', endtime, maxtlist[0]
        else:
            print "gap", endtime, maxtlist[0]
            if True:
                addflist = maxlist
                for i in xrange(0, len(addflist)):
                    for j in xrange(0, num_racer):
                        ecgi = ecgfix[j]
                        if ecgi != 0:
                            v0 = addflist[i][ecgi]
                            pqrst = get_pqrst(j, v0)
                            addflist[i][ecgi] = pqrst
                            ecggb2 = calc_ecggb2(j, pqrst)
                            eglist[j].append(ecggb2)
                            mglist[j].append(pqrst)
                mflist += addflist
            print "FILL_SKIP:", len(maxtlist), '[', line_num, ':', len(mflist), ']', endtime, maxtlist[0]
        time.sleep(5)

def line_field(i, f):
    if f[0] == 'ALL':
        return mflist[i]
    chk = [x in header for x in f]
    if all(x == True for x in chk) == False:
        return []
    idx = [header.index(x) for x in f]
    ln = mflist[i]
    val = [ln[x] for x in idx]
    return val

class MainHandler(tornado.web.RequestHandler):
    def get(self):
        loader = tornado.template.Loader(".")
        self.write(loader.load("index.html").generate())
        
class WSHandler(tornado.websocket.WebSocketHandler):
    def tf(self, ev, msg):
        global line_num
        global loc_pointer
        while True:
            if ev.isSet():
                return
            line_max = len(mflist)
            if line_num < line_max:
                val = line_field(line_num, msg)
                if (msg[0] == 'ALL') :
                    calc_all = []
                    for i in xrange(0, num_racer):
                        brg = racer_bridge[i]
                        if brg != "Unknown":
                            lat = location[loc_pointer][2*i]
                            lon = location[loc_pointer][2*i+1]
                            if brg == 'Zephyr':
                                vmu  = val[racer_fldidx[i] + zp_vmu_offset]
                                pacc = val[racer_fldidx[i] + zp_pacc_offset]
                                ecgv = val[racer_fldidx[i] + zp_ecgv_offset]
                                z = calc_state(vmu, pacc)
                                p = get_ecggb2(i,line_num)
                            elif brg == 'Shimmer':
                                z = 0
                                p = get_ecggb2(i,line_num)
                            else:
                                z = 0
                                p = 'BAD:0:1'
                            calc_all += [lat, lon, str(z), p]
                    loc_pointer = (loc_pointer+1)%loc_max
                    stro = 'DATA ' + descriptor + ' ' + ' '.join(val) + ' ' + ' '.join(calc_all)
                else:
                    stro = 'DATA ' + ' '.join(val)
                if len(val) > 0:
                    line_num += 1
                    print "EMPTY:", 1, '[', line_num, ':', line_max, ']'
                    self.write_message(stro)
            else:        
                print "EMPTY:", 0, '[', line_num, ':', line_max, ']'
            time.sleep(1)

    def check_origin(self, origin):
        return True

    def open(self):
        print 'connection opened...'

    def on_message(self, message):
        global line_num
        global loc_pointer
        global stream_th
        global stream_ev
        line_max = len(mflist)
        print 'received:', message

        msg = message.split(' ')
        if len(msg) > 0:
            cmd = msg[0]
            msg.pop(0)
            if cmd == 'SET' :
                slop = 5
                if line_max - line_num < slop:
                    string = 'NACK'
                else:
                    line_num = line_max-slop
                    string = 'ACK'
            elif cmd == 'READ' :
                if line_num < line_max :
                    if len(msg) > 0 :
                        val = line_field(line_num, msg)
                        if (msg[0] == 'ALL') :
                            calc_all = []
                            for i in xrange(0, num_racer):
                                brg = racer_bridge[i]
                                if brg != "Unknown":
                                    lat = location[loc_pointer][2*i]
                                    lon = location[loc_pointer][2*i+1]
                                    if brg == 'Zephyr':
                                        vmu  = val[racer_fldidx[i] + zp_vmu_offset]
                                        pacc = val[racer_fldidx[i] + zp_pacc_offset]
                                        ecgv = val[racer_fldidx[i] + zp_ecgv_offset]
                                        z = calc_state(vmu, pacc)
                                        p = get_ecggb2(i,line_num)
                                    elif brg == 'Shimmer':
                                        z = 0
                                        p = get_ecggb2(i,line_num)
                                    else:
                                        z = 0
                                        p = 'BAD:0:1'
                                    calc_all += [lat, lon, str(z), p]
                            loc_pointer = (loc_pointer+1)%loc_max
                            string = 'DATA ' + descriptor + ' ' + ' '.join(val) + ' ' + ' '.join(calc_all)
                        else:
                            string = 'DATA ' + ' '.join(val)
                        if len(val) > 0:
                            line_num += 1 
                            print "EMPTY:", 1, '[', line_num, ':', line_max, ']'
                        else:
                            string = 'NACK'
                else:
                    print "EMPTY:", 0, '[', line_num, ':', line_max, ']'
                    string = 'NODATA'
            elif cmd == 'START' :
                stream_ev = threading.Event()
                stream_th = threading.Thread(target=self.tf, args=(stream_ev,msg,))
                stream_th.start()
                string = 'ACK'
            elif cmd == 'STOP' :
                stream_ev.set()
                stream_th.join()
                stream_ev = None
                stream_th = None
                string = 'ACK'
            else:
                string = 'NACK'

        self.write_message(string)
        print 'send: ', string

    def on_close(self):
        global stream_th
        global stream_ev
        if stream_th != None:
            print 'stopping stream'
            stream_ev.set()
            stream_th.join()
        print 'connection closed...'

application = tornado.web.Application([
    (r'/', WSHandler),
])

if __name__ == "__main__":
    print len(mflist)
    thread = threading.Thread(target=threaded_function, args=(10,))
    thread.start()
    print 'Ready...'
    application.listen(8080)
    tornado.ioloop.IOLoop.instance().start()
