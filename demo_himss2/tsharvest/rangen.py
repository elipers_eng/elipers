import random

x = [0, 0, 0, 0]
y = [0, 0, 0, 0]
dx = [0, 0, 0, 0]
dy = [0, 0, 0, 0]

foo = []
for i in xrange(0, 100):
    dx[0] = random.randint(-10, 10)
    dx[1] = random.randint(-10, 10)
    dx[2] = random.randint(-10, 10)
    dx[3] = random.randint(-10, 10)
    dy[0] = random.randint(-10, 10)
    dy[1] = random.randint(-10, 10)
    dy[2] = random.randint(-10, 10)
    dy[3] = random.randint(-10, 10)
    x = [sum(i) for i in zip(x, dx)]
    y = [sum(i) for i in zip(y, dy)]
    foo.append([x, y])

for i in range(0, 100):
    (p, q) = foo[i]
    print ("%s:%s:%s:%s:%s:%s:%s:%s" % (p[0],q[0],p[1],q[1],p[2],q[2],p[3],q[3]))

for i in range(99, -1, -1):    
    (p, q) = foo[i]
    print ("%s:%s:%s:%s:%s:%s:%s:%s" % (p[0],q[0],p[1],q[1],p[2],q[2],p[3],q[3]))
    
